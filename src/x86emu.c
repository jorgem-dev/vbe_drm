// SPDX-License-Identifier: HPND-sell-variant

/*
 * Realmode X86 Emulator Library
 *
 * Copyright (C) 1996-1999 SciTech Software, Inc.
 * Copyright (C) David Mosberger-Tang
 * Copyright (C) 1999 Egbert Eich
 * Copyright (C) 2007 Joerg Sonnenberger
 * Copyright (C) 2024 Jorge Maidana
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies and that
 * both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of the authors not be used
 * in advertising or publicity pertaining to distribution of the software
 * without specific, written prior permission. The authors make no
 * representations about the suitability of this software for any purpose.
 * It is provided "as is" without express or implied warranty.
 *
 * THE AUTHORS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL THE AUTHORS BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <linux/kernel.h>
#include <linux/math64.h>
#include <linux/string.h>
#include <linux/types.h>

#include "x86emu.h"

static void x86emu_intr_handle(struct x86emu *emu, u8 intno);
static void x86emu_intr_return(struct x86emu *emu);
static void x86emu_exec_one_byte(struct x86emu *emu);
static void x86emu_exec_two_byte(struct x86emu *emu);
static void x86emu_exec_three_byte(struct x86emu *emu, u8 op2);
static void fetch_decode_modrm(struct x86emu *emu);
static u8 fetch_byte_imm(struct x86emu *emu);
static u16 fetch_word_imm(struct x86emu *emu);
static u32 fetch_long_imm(struct x86emu *emu);
static u8 fetch_data_byte(struct x86emu *emu, u32 offset);
static u8 fetch_byte(struct x86emu *emu, u32 segment, u32 offset);
static u16 fetch_data_word(struct x86emu *emu, u32 offset);
static u16 fetch_word(struct x86emu *emu, u32 segment, u32 offset);
static u32 fetch_data_long(struct x86emu *emu, u32 offset);
static u32 fetch_long(struct x86emu *emu, u32 segment, u32 offset);
static void store_data_byte(struct x86emu *emu, u32 offset, u8 val);
static void store_byte(struct x86emu *emu, u32 segment, u32 offset, u8 val);
static void store_data_word(struct x86emu *emu, u32 offset, u16 val);
static void store_word(struct x86emu *emu, u32 segment, u32 offset, u16 val);
static void store_data_long(struct x86emu *emu, u32 offset, u32 val);
static void store_long(struct x86emu *emu, u32 segment, u32 offset, u32 val);
static u8 *decode_rl_byte_register(struct x86emu *emu);
static u16 *decode_rl_word_register(struct x86emu *emu);
static u32 *decode_rl_long_register(struct x86emu *emu);
static u8 *decode_rh_byte_register(struct x86emu *emu);
static u16 *decode_rh_word_register(struct x86emu *emu);
static u32 *decode_rh_long_register(struct x86emu *emu);
static u16 *decode_rh_seg_register(struct x86emu *emu);
static u32 decode_rl_address(struct x86emu *emu);
static u8 decode_and_fetch_byte(struct x86emu *emu);
static u16 decode_and_fetch_word(struct x86emu *emu);
static u32 decode_and_fetch_long(struct x86emu *emu);
static u8 decode_and_fetch_byte_imm8(struct x86emu *emu, u8 *imm);
static u16 decode_and_fetch_word_imm8(struct x86emu *emu, u8 *imm);
static u32 decode_and_fetch_long_imm8(struct x86emu *emu, u8 *imm);
static u16 decode_and_fetch_word_disp(struct x86emu *emu, s16 disp);
static u32 decode_and_fetch_long_disp(struct x86emu *emu, s16 disp);
static void write_back_byte(struct x86emu *emu, u8 val);
static void write_back_word(struct x86emu *emu, u16 val);
static void write_back_long(struct x86emu *emu, u32 val);

static u16 aaa_word(struct x86emu *emu, u16 op1);
static u16 aad_word(struct x86emu *emu, u16 op1, u8 op2);
static u16 aam_word(struct x86emu *emu, u8 op1, u8 op2);
static u16 aas_word(struct x86emu *emu, u16 op1);
static u8 adc_byte(struct x86emu *emu, u8 op1, u8 op2);
static u16 adc_word(struct x86emu *emu, u16 op1, u16 op2);
static u32 adc_long(struct x86emu *emu, u32 op1, u32 op2);
static u8 add_byte(struct x86emu *emu, u8 op1, u8 op2);
static u16 add_word(struct x86emu *emu, u16 op1, u16 op2);
static u32 add_long(struct x86emu *emu, u32 op1, u32 op2);
static u8 and_byte(struct x86emu *emu, u8 op1, u8 op2);
static u16 and_word(struct x86emu *emu, u16 op1, u16 op2);
static u32 and_long(struct x86emu *emu, u32 op1, u32 op2);
static u8 cmp_byte_r(struct x86emu *emu, u8 op1, u8 op2);
static void cmp_byte(struct x86emu *emu, u8 op1, u8 op2);
static u16 cmp_word_r(struct x86emu *emu, u16 op1, u16 op2);
static void cmp_word(struct x86emu *emu, u16 op1, u16 op2);
static u32 cmp_long_r(struct x86emu *emu, u32 op1, u32 op2);
static void cmp_long(struct x86emu *emu, u32 op1, u32 op2);
static u8 daa_byte(struct x86emu *emu, u8 op1);
static u8 das_byte(struct x86emu *emu, u8 op1);
static u8 dec_byte(struct x86emu *emu, u8 op1);
static u16 dec_word(struct x86emu *emu, u16 op1);
static u32 dec_long(struct x86emu *emu, u32 op1);
static void div_byte(struct x86emu *emu, u8 op1);
static void div_word(struct x86emu *emu, u16 op1);
static void div_long(struct x86emu *emu, u32 op1);
static void idiv_byte(struct x86emu *emu, u8 op1);
static void idiv_word(struct x86emu *emu, u16 op1);
static void idiv_long(struct x86emu *emu, u32 op1);
static void imul_byte(struct x86emu *emu, u8 op1);
static void imul_word(struct x86emu *emu, u16 op1);
static void imul_long(struct x86emu *emu, u32 op1);
static u8 inc_byte(struct x86emu *emu, u8 op1);
static u16 inc_word(struct x86emu *emu, u16 op1);
static u32 inc_long(struct x86emu *emu, u32 op1);
static void ins(struct x86emu *emu, s32 size);
static void mul_byte(struct x86emu *emu, u8 op1);
static void mul_word(struct x86emu *emu, u16 op1);
static void mul_long(struct x86emu *emu, u32 op1);
static u8 neg_byte(struct x86emu *emu, u8 op1);
static u16 neg_word(struct x86emu *emu, u16 op1);
static u32 neg_long(struct x86emu *emu, u32 op1);
static u8 or_byte(struct x86emu *emu, u8 op1, u8 op2);
static u16 or_word(struct x86emu *emu, u16 op1, u16 op2);
static u32 or_long(struct x86emu *emu, u32 op1, u32 op2);
static void outs(struct x86emu *emu, s32 size);
static u16 pop_word(struct x86emu *emu);
static u32 pop_long(struct x86emu *emu);
static void push_word(struct x86emu *emu, u16 val);
static void push_long(struct x86emu *emu, u32 val);
static u8 rcl_byte(struct x86emu *emu, u8 op1, u8 op2);
static u16 rcl_word(struct x86emu *emu, u16 op1, u8 op2);
static u32 rcl_long(struct x86emu *emu, u32 op1, u8 op2);
static u8 rcr_byte(struct x86emu *emu, u8 op1, u8 op2);
static u16 rcr_word(struct x86emu *emu, u16 op1, u8 op2);
static u32 rcr_long(struct x86emu *emu, u32 op1, u8 op2);
static u8 rol_byte(struct x86emu *emu, u8 op1, u8 op2);
static u16 rol_word(struct x86emu *emu, u16 op1, u8 op2);
static u32 rol_long(struct x86emu *emu, u32 op1, u8 op2);
static u8 ror_byte(struct x86emu *emu, u8 op1, u8 op2);
static u16 ror_word(struct x86emu *emu, u16 op1, u8 op2);
static u32 ror_long(struct x86emu *emu, u32 op1, u8 op2);
static u8 sar_byte(struct x86emu *emu, u8 op1, u8 op2);
static u16 sar_word(struct x86emu *emu, u16 op1, u8 op2);
static u32 sar_long(struct x86emu *emu, u32 op1, u8 op2);
static u8 sbb_byte(struct x86emu *emu, u8 op1, u8 op2);
static u16 sbb_word(struct x86emu *emu, u16 op1, u16 op2);
static u32 sbb_long(struct x86emu *emu, u32 op1, u32 op2);
static u8 shl_byte(struct x86emu *emu, u8 op1, u8 op2);
static u16 shl_word(struct x86emu *emu, u16 op1, u8 op2);
static u32 shl_long(struct x86emu *emu, u32 op1, u8 op2);
static u16 shld_word(struct x86emu *emu, u16 op1, u16 op2, u8 op3);
static u32 shld_long(struct x86emu *emu, u32 op1, u32 op2, u8 op3);
static u8 shr_byte(struct x86emu *emu, u8 op1, u8 op2);
static u16 shr_word(struct x86emu *emu, u16 op1, u8 op2);
static u32 shr_long(struct x86emu *emu, u32 op1, u8 op2);
static u16 shrd_word(struct x86emu *emu, u16 op1, u16 op2, u8 op3);
static u32 shrd_long(struct x86emu *emu, u32 op1, u32 op2, u8 op3);
static u8 sub_byte(struct x86emu *emu, u8 op1, u8 op2);
static u16 sub_word(struct x86emu *emu, u16 op1, u16 op2);
static u32 sub_long(struct x86emu *emu, u32 op1, u32 op2);
static void test_byte(struct x86emu *emu, u8 op1, u8 op2);
static void test_word(struct x86emu *emu, u16 op1, u16 op2);
static void test_long(struct x86emu *emu, u32 op1, u32 op2);
static u8 xor_byte(struct x86emu *emu, u8 op1, u8 op2);
static u16 xor_word(struct x86emu *emu, u16 op1, u16 op2);
static u32 xor_long(struct x86emu *emu, u32 op1, u32 op2);

static __always_inline void x86emu_set_flags(struct x86emu *emu, bool cf, u8 pf, bool af,
					     bool zf, bool sf, u8 of, bool of_bool);

static void x86emu_ints_return(struct x86emu *emu, u8 ret)
{
	emu->x86.R_AH = ret;
	X86EMU_SET_FLAG_COND(CF_SET(emu->x86.R_AH), F_CF);
}

static void x86emu_ints_13h_handle(struct x86emu *emu, u8 intno)
{
	switch (emu->x86.R_AH) {
	case 0x01: /* Status */
	case 0x08: /* Parameters */
	case 0x15: /* Type */
		x86emu_intr_handle(emu, intno);
		return;
	}
	x86emu_ints_return(emu, 1);
}

static void x86emu_ints_15h_handle(struct x86emu *emu, u8 intno)
{
	switch (emu->x86.R_AH) {
	case 0x87: /* Copy extended memory */
		x86emu_ints_return(emu, 0);
		return;
	}
	x86emu_intr_handle(emu, intno);
}

static void x86emu_intr_return(struct x86emu *emu)
{
	emu->x86.R_IP = pop_word(emu);
	emu->x86.R_CS = pop_word(emu);
	emu->x86.R_EFLG = (u32)pop_word(emu);
}

static void x86emu_intr_handle(struct x86emu *emu, u8 intno)
{
	push_word(emu, emu->x86.R_FLG);
	CLEAR_FLAG(F_TF | F_IF);
	push_word(emu, emu->x86.R_CS);
	emu->x86.R_CS = fetch_word(emu, 0, (u32)intno * 4 + 2);
	push_word(emu, emu->x86.R_IP);
	emu->x86.R_IP = fetch_word(emu, 0, (u32)intno * 4);
}

/* Handle any pending asynchronous interrupts */
static void x86emu_intr_dispatch(struct x86emu *emu, u8 intno)
{
	if (!emu->x86_intr[intno]) {
		switch (intno) {
		case 0x00 ... 0x05: /* Debug */
			return;
		case INT6_UD_FAULT:
		case INT7_NM_FAULT:
			x86emu_halt_sys(emu);
			return;
		case 0x13:
			x86emu_ints_13h_handle(emu, intno);
			return;
		case 0x15:
			x86emu_ints_15h_handle(emu, intno);
			return;
		default:
			x86emu_intr_handle(emu, intno);
			return;
		}
	} else {
		(*emu->x86_intr[intno])(emu, intno);
	}
}

/**
 * @intno: interrupt number to raise
 *
 * Raise the specified interrupt to be handled before the execution of the
 * next instruction.
 */
static void x86emu_intr_raise(struct x86emu *emu, u8 intno)
{
	INTR_SET_SYNCH;
	WRITE_ONCE(emu->x86.intno, intno);
}

/*
 * Main execution loop for the emulator. We return from here when the system
 * halts, which is normally caused by a stack fault when we return from the
 * original real mode call.
 */
void x86emu_exec(struct x86emu *emu)
{
	INTR_MODE_CLEAR;

	for (;;) {
		if (INTR_MODE_SET) {
			if (INTR_MODE_HALT)
				return;

			if (INTR_MODE_SYNCH) {
				u8 intno = READ_ONCE(emu->x86.intno);

				INTR_CLEAR_SYNCH;
				x86emu_intr_dispatch(emu, intno);
			}
		}
		if (emu->x86.R_CS == 0 && emu->x86.R_IP == 0)
			return;
		x86emu_exec_one_byte(emu);
		WRITE_ONCE(emu->tsc, READ_ONCE(emu->tsc) + 1);
	}
}

/* Halt the system by setting the halted system flag */
void x86emu_halt_sys(struct x86emu *emu)
{
	INTR_SET_HALT;
}

static void fetch_decode_modrm(struct x86emu *emu)
{
	u8 imm;

	imm = fetch_byte_imm(emu);
	emu->cur_mod = (imm >> 6) & 3;
	emu->cur_rh = (imm >> 3) & 7;
	emu->cur_rl = (imm >> 0) & 7;
}

/**
 * Returns:
 * Immediate byte value read from instruction queue
 *
 * This function returns the immediate byte from the instruction queue, and
 * moves the instruction pointer to the next value.
 */
static u8 fetch_byte_imm(struct x86emu *emu)
{
	u8 fetched = fetch_byte(emu, emu->x86.R_CS, emu->x86.R_IP);

	emu->x86.R_IP++;
	return fetched;
}

/**
 * Returns:
 * Immediate word value read from instruction queue
 *
 * This function returns the immediate word from the instruction queue, and
 * moves the instruction pointer to the next value.
 */
static u16 fetch_word_imm(struct x86emu *emu)
{
	u16 fetched = fetch_word(emu, emu->x86.R_CS, emu->x86.R_IP);

	emu->x86.R_IP += 2;
	return fetched;
}

/**
 * Returns:
 * Immediate long value read from instruction queue
 *
 * This function returns the immediate long from the instruction queue, and
 * moves the instruction pointer to the next value.
 */
static u32 fetch_long_imm(struct x86emu *emu)
{
	u32 fetched = fetch_long(emu, emu->x86.R_CS, emu->x86.R_IP);

	emu->x86.R_IP += 4;
	return fetched;
}

/**
 * Returns:
 * Value of the default data segment
 *
 * On the x86 processor, the default segment is not always DS if there is
 * no segment override. Address modes such as -3[BP] or 10[BP+SI] all refer to
 * addresses relative to SS (ie: on the stack). So, at the minimum, all
 * decodings of addressing modes would have to set/clear a bit describing
 * whether the access is relative to DS or SS. That is the function of the
 * cpu-state-variable emu->x86.mode. There are several potential states:
 *
 *	repe prefix seen (handled elsewhere)
 *	repne prefix seen (ditto)
 *
 *	cs segment override
 *	ds segment override
 *	es segment override
 *	fs segment override
 *	gs segment override
 *	ss segment override
 *
 *	ds/ss select (in absence of override)
 *
 * Each of the above 7 items are handled with a bit in the mode field.
 */
static u32 get_data_segment(struct x86emu *emu)
{
	switch (MODE_SEG) {
	default: /* default case: use ds register */
	case SYSMODE_SEGOVR_DS:
	case SYSMODE_SEGOVR_DS | SYSMODE_SEG_DS_SS:
		return emu->x86.R_DS;
	case SYSMODE_SEG_DS_SS: /* not-overridden, use ss register */
		return emu->x86.R_SS;
	case SYSMODE_SEGOVR_CS:
	case SYSMODE_SEGOVR_CS | SYSMODE_SEG_DS_SS:
		return emu->x86.R_CS;
	case SYSMODE_SEGOVR_ES:
	case SYSMODE_SEGOVR_ES | SYSMODE_SEG_DS_SS:
		return emu->x86.R_ES;
	case SYSMODE_SEGOVR_FS:
	case SYSMODE_SEGOVR_FS | SYSMODE_SEG_DS_SS:
		return emu->x86.R_FS;
	case SYSMODE_SEGOVR_GS:
	case SYSMODE_SEGOVR_GS | SYSMODE_SEG_DS_SS:
		return emu->x86.R_GS;
	case SYSMODE_SEGOVR_SS:
	case SYSMODE_SEGOVR_SS | SYSMODE_SEG_DS_SS:
		return emu->x86.R_SS;
	}
}

/**
 * @offset: offset to load data from
 *
 * Returns:
 * Byte value read from the absolute memory location.
 */
static u8 fetch_data_byte(struct x86emu *emu, u32 offset)
{
	return fetch_byte(emu, get_data_segment(emu), offset);
}

/**
 * @offset: offset to load data from
 *
 * Returns:
 * Word value read from the absolute memory location.
 */
static u16 fetch_data_word(struct x86emu *emu, u32 offset)
{
	return fetch_word(emu, get_data_segment(emu), offset);
}

/**
 * @offset: offset to load data from
 *
 * Returns:
 * Long value read from the absolute memory location.
 */
static u32 fetch_data_long(struct x86emu *emu, u32 offset)
{
	return fetch_long(emu, get_data_segment(emu), offset);
}

/**
 * @segment: segment to load data from
 * @offset: offset to load data from
 *
 * Returns:
 * Byte value read from the absolute memory location.
 */
static u8 fetch_byte(struct x86emu *emu, u32 segment, u32 offset)
{
	return (*emu->x86_readb)(((u32)segment << 4) + offset);
}

/**
 * @segment: segment to load data from
 * @offset: offset to load data from
 *
 * Returns:
 * Word value read from the absolute memory location.
 */
static u16 fetch_word(struct x86emu *emu, u32 segment, u32 offset)
{
	return (*emu->x86_readw)(((u32)segment << 4) + offset);
}

/**
 * @segment: segment to load data from
 * @offset: offset to load data from
 *
 * Returns:
 * Long value read from the absolute memory location.
 */
static u32 fetch_long(struct x86emu *emu, u32 segment, u32 offset)
{
	return (*emu->x86_readl)(((u32)segment << 4) + offset);
}

/**
 * @offset: offset to store data at
 * @val: value to store
 *
 * Write a byte value to an segmented memory location. The segment used is
 * the current 'default' segment, which may have been overridden.
 */
static void store_data_byte(struct x86emu *emu, u32 offset, u8 val)
{
	store_byte(emu, get_data_segment(emu), offset, val);
}

/**
 * @offset: offset to store data at
 * @val: value to store
 *
 * Write a word value to an segmented memory location. The segment used is
 * the current 'default' segment, which may have been overridden.
 */
static void store_data_word(struct x86emu *emu, u32 offset, u16 val)
{
	store_word(emu, get_data_segment(emu), offset, val);
}

/**
 * @offset: offset to store data at
 * @val: value to store
 *
 * Write a long value to an segmented memory location. The segment used is
 * the current 'default' segment, which may have been overridden.
 */
static void store_data_long(struct x86emu *emu, u32 offset, u32 val)
{
	store_long(emu, get_data_segment(emu), offset, val);
}

/**
 * @segment: segment to store data at
 * @offset: offset to store data at
 * @val: value to store
 *
 * Write a byte value to an absolute memory location.
 */
static void store_byte(struct x86emu *emu, u32 segment, u32 offset, u8 val)
{
	(*emu->x86_writeb)(((u32)segment << 4) + offset, val);
}

/**
 * @segment: segment to store data at
 * @offset: offset to store data at
 * @val: value to store
 *
 * Write a word value to an absolute memory location.
 */
static void store_word(struct x86emu *emu, u32 segment, u32 offset, u16 val)
{
	(*emu->x86_writew)(((u32)segment << 4) + offset, val);
}

/**
 * @segment: segment to store data at
 * @offset: offset to store data at
 * @val: value to store
 *
 * Write a long value to an absolute memory location.
 */
static void store_long(struct x86emu *emu, u32 segment, u32 offset, u32 val)
{
	(*emu->x86_writel)(((u32)segment << 4) + offset, val);
}

/**
 * @reg: register to decode
 *
 * Returns:
 * Pointer to the appropriate register
 *
 * Return a pointer to the register given by the R/RM field of the
 * modrm byte, for byte operands. Also enables the decoding of instructions.
 */
static u8 *decode_rm_byte_register(struct x86emu *emu, u32 reg)
{
	switch (reg) {
	case 0:
		return &emu->x86.R_AL;
	case 1:
		return &emu->x86.R_CL;
	case 2:
		return &emu->x86.R_DL;
	case 3:
		return &emu->x86.R_BL;
	case 4:
		return &emu->x86.R_AH;
	case 5:
		return &emu->x86.R_CH;
	case 6:
		return &emu->x86.R_DH;
	case 7:
		return &emu->x86.R_BH;
	default:
		x86emu_halt_sys(emu);
		return NULL;
	}
}

static u8 *decode_rl_byte_register(struct x86emu *emu)
{
	return decode_rm_byte_register(emu, emu->cur_rl);
}

static u8 *decode_rh_byte_register(struct x86emu *emu)
{
	return decode_rm_byte_register(emu, emu->cur_rh);
}

/**
 * @reg: register to decode
 *
 * Returns:
 * Pointer to the appropriate register
 *
 * Return a pointer to the register given by the R/RM field of the
 * modrm byte, for word operands. Also enables the decoding of instructions.
 */
static u16 *decode_rm_word_register(struct x86emu *emu, u32 reg)
{
	switch (reg) {
	case 0:
		return &emu->x86.R_AX;
	case 1:
		return &emu->x86.R_CX;
	case 2:
		return &emu->x86.R_DX;
	case 3:
		return &emu->x86.R_BX;
	case 4:
		return &emu->x86.R_SP;
	case 5:
		return &emu->x86.R_BP;
	case 6:
		return &emu->x86.R_SI;
	case 7:
		return &emu->x86.R_DI;
	default:
		x86emu_halt_sys(emu);
		return NULL;
	}
}

static u16 *decode_rl_word_register(struct x86emu *emu)
{
	return decode_rm_word_register(emu, emu->cur_rl);
}

static u16 *decode_rh_word_register(struct x86emu *emu)
{
	return decode_rm_word_register(emu, emu->cur_rh);
}

/**
 * @reg: register to decode
 *
 * Returns:
 * Pointer to the appropriate register
 *
 * Return a pointer to the register given by the R/RM field of the
 * modrm byte, for dword operands. Also enables the decoding of instructions.
 */
static u32 *decode_rm_long_register(struct x86emu *emu, u32 reg)
{
	switch (reg) {
	case 0:
		return &emu->x86.R_EAX;
	case 1:
		return &emu->x86.R_ECX;
	case 2:
		return &emu->x86.R_EDX;
	case 3:
		return &emu->x86.R_EBX;
	case 4:
		return &emu->x86.R_ESP;
	case 5:
		return &emu->x86.R_EBP;
	case 6:
		return &emu->x86.R_ESI;
	case 7:
		return &emu->x86.R_EDI;
	default:
		x86emu_halt_sys(emu);
		return NULL;
	}
}

static u32 *decode_rl_long_register(struct x86emu *emu)
{
	return decode_rm_long_register(emu, emu->cur_rl);
}

static u32 *decode_rh_long_register(struct x86emu *emu)
{
	return decode_rm_long_register(emu, emu->cur_rh);
}

/**
 * Returns:
 * Pointer to the appropriate register
 *
 * Return a pointer to the register given by the R/RM field of the
 * modrm byte, for word operands, modified from above for the weirdo
 * special case of segreg operands. Also enables the decoding of instructions.
 */
static u16 *decode_rh_seg_register(struct x86emu *emu)
{
	switch (emu->cur_rh) {
	case 0:
		return &emu->x86.R_ES;
	case 1:
		return &emu->x86.R_CS;
	case 2:
		return &emu->x86.R_SS;
	case 3:
		return &emu->x86.R_DS;
	case 4:
		return &emu->x86.R_FS;
	case 5:
		return &emu->x86.R_GS;
	default:
		x86emu_halt_sys(emu);
		return NULL;
	}
}

/* Return offset from the SIB Byte */
static u32 decode_sib_address(struct x86emu *emu, u8 sib)
{
	u32 base = 0, i = 0, scale = 1;

	switch (sib & 0x07) {
	case 0:
		base = emu->x86.R_EAX;
		break;
	case 1:
		base = emu->x86.R_ECX;
		break;
	case 2:
		base = emu->x86.R_EDX;
		break;
	case 3:
		base = emu->x86.R_EBX;
		break;
	case 4:
		base = READ_ONCE(emu->x86.R_ESP);
		emu->x86.mode |= SYSMODE_SEG_DS_SS;
		break;
	case 5:
		base = fetch_long_imm(emu);
		break;
	case 6:
		base = emu->x86.R_ESI;
		break;
	case 7:
		base = emu->x86.R_EDI;
		break;
	}

	switch ((sib >> 3) & 0x07) {
	case 0:
		i = emu->x86.R_EAX;
		break;
	case 1:
		i = emu->x86.R_ECX;
		break;
	case 2:
		i = emu->x86.R_EDX;
		break;
	case 3:
		i = emu->x86.R_EBX;
		break;
	case 4:
		i = 0;
		break;
	case 5:
		i = READ_ONCE(emu->x86.R_EBP);
		break;
	case 6:
		i = emu->x86.R_ESI;
		break;
	case 7:
		i = emu->x86.R_EDI;
		break;
	}
	scale = 1 << ((sib >> 6) & 0x03);
	return base + (i * scale);
}

/**
 * Returns:
 * Offset in memory for the address decoding
 *
 * Return the offset given by mod=00, mod=01 or mod=10 addressing.
 * Also enables the decoding of instructions.
 */
static u32 decode_rl_address(struct x86emu *emu)
{
	if (MODE_ADDR32) {
		u32 offset;
		u8 sib;

		/* 32-bit addressing */
		switch (emu->cur_rl) {
		case 0:
			offset = emu->x86.R_EAX;
			break;
		case 1:
			offset = emu->x86.R_ECX;
			break;
		case 2:
			offset = emu->x86.R_EDX;
			break;
		case 3:
			offset = emu->x86.R_EBX;
			break;
		case 4:
			sib = fetch_byte_imm(emu);
			offset = decode_sib_address(emu, sib);
			break;
		case 5:
			if (emu->cur_mod == 0) {
				offset = fetch_long_imm(emu);
			} else {
				emu->x86.mode |= SYSMODE_SEG_DS_SS;
				offset = READ_ONCE(emu->x86.R_EBP);
			}
			break;
		case 6:
			offset = emu->x86.R_ESI;
			break;
		case 7:
			offset = emu->x86.R_EDI;
			break;
		default:
			x86emu_halt_sys(emu);
			break;
		}
		if (emu->cur_mod == 1)
			offset += (s8)fetch_byte_imm(emu);
		else if (emu->cur_mod == 2)
			offset += fetch_long_imm(emu);
		return offset;
	} else {
		u16 offset;

		/* 16-bit addressing */
		switch (emu->cur_rl) {
		case 0:
			offset = emu->x86.R_BX + emu->x86.R_SI;
			break;
		case 1:
			offset = emu->x86.R_BX + emu->x86.R_DI;
			break;
		case 2:
			emu->x86.mode |= SYSMODE_SEG_DS_SS;
			offset = READ_ONCE(emu->x86.R_BP) + emu->x86.R_SI;
			break;
		case 3:
			emu->x86.mode |= SYSMODE_SEG_DS_SS;
			offset = READ_ONCE(emu->x86.R_BP) + emu->x86.R_DI;
			break;
		case 4:
			offset = emu->x86.R_SI;
			break;
		case 5:
			offset = emu->x86.R_DI;
			break;
		case 6:
			if (emu->cur_mod == 0) {
				offset = fetch_word_imm(emu);
			} else {
				emu->x86.mode |= SYSMODE_SEG_DS_SS;
				offset = READ_ONCE(emu->x86.R_BP);
			}
			break;
		case 7:
			offset = emu->x86.R_BX;
			break;
		default:
			x86emu_halt_sys(emu);
			break;
		}
		if (emu->cur_mod == 1)
			offset += (s8)fetch_byte_imm(emu);
		else if (emu->cur_mod == 2)
			offset += fetch_word_imm(emu);
		return offset;
	}
}

static u8 decode_and_fetch_byte(struct x86emu *emu)
{
	if (emu->cur_mod != 3) {
		emu->cur_offset = decode_rl_address(emu);
		return fetch_data_byte(emu, emu->cur_offset);
	} else {
		return *decode_rl_byte_register(emu);
	}
}

static u16 decode_and_fetch_word_disp(struct x86emu *emu, s16 disp)
{
	if (emu->cur_mod != 3) {
		emu->cur_offset = decode_rl_address(emu) + disp;
		if (!MODE_ADDR32)
			emu->cur_offset &= 0xffff;
		return fetch_data_word(emu, emu->cur_offset);
	} else {
		return *decode_rl_word_register(emu);
	}
}

static u32 decode_and_fetch_long_disp(struct x86emu *emu, s16 disp)
{
	if (emu->cur_mod != 3) {
		emu->cur_offset = decode_rl_address(emu) + disp;
		if (!MODE_ADDR32)
			emu->cur_offset &= 0xffff;
		return fetch_data_long(emu, emu->cur_offset);
	} else {
		return *decode_rl_long_register(emu);
	}
}

static u16 decode_and_fetch_word(struct x86emu *emu)
{
	return decode_and_fetch_word_disp(emu, 0);
}

static u32 decode_and_fetch_long(struct x86emu *emu)
{
	return decode_and_fetch_long_disp(emu, 0);
}

static u8 decode_and_fetch_byte_imm8(struct x86emu *emu, u8 *imm)
{
	if (emu->cur_mod != 3) {
		emu->cur_offset = decode_rl_address(emu);
		*imm = fetch_byte_imm(emu);
		return fetch_data_byte(emu, emu->cur_offset);
	} else {
		*imm = fetch_byte_imm(emu);
		return *decode_rl_byte_register(emu);
	}
}

static u16 decode_and_fetch_word_imm8(struct x86emu *emu, u8 *imm)
{
	if (emu->cur_mod != 3) {
		emu->cur_offset = decode_rl_address(emu);
		*imm = fetch_byte_imm(emu);
		return fetch_data_word(emu, emu->cur_offset);
	} else {
		*imm = fetch_byte_imm(emu);
		return *decode_rl_word_register(emu);
	}
}

static u32 decode_and_fetch_long_imm8(struct x86emu *emu, u8 *imm)
{
	if (emu->cur_mod != 3) {
		emu->cur_offset = decode_rl_address(emu);
		*imm = fetch_byte_imm(emu);
		return fetch_data_long(emu, emu->cur_offset);
	} else {
		*imm = fetch_byte_imm(emu);
		return *decode_rl_long_register(emu);
	}
}

static void write_back_byte(struct x86emu *emu, u8 val)
{
	if (emu->cur_mod != 3)
		store_data_byte(emu, emu->cur_offset, val);
	else
		*decode_rl_byte_register(emu) = val;
}

static void write_back_word(struct x86emu *emu, u16 val)
{
	if (emu->cur_mod != 3)
		store_data_word(emu, emu->cur_offset, val);
	else
		*decode_rl_word_register(emu) = val;
}

static void write_back_long(struct x86emu *emu, u32 val)
{
	if (emu->cur_mod != 3)
		store_data_long(emu, emu->cur_offset, val);
	else
		*decode_rl_long_register(emu) = val;
}

static void common_inc_word_long(struct x86emu *emu, union x86emu_register *reg)
{
	if (MODE_DATA32)
		reg->I32_reg.e_reg = inc_long(emu, reg->I32_reg.e_reg);
	else
		reg->I16_reg.x_reg = inc_word(emu, reg->I16_reg.x_reg);
}

static void common_dec_word_long(struct x86emu *emu, union x86emu_register *reg)
{
	if (MODE_DATA32)
		reg->I32_reg.e_reg = dec_long(emu, reg->I32_reg.e_reg);
	else
		reg->I16_reg.x_reg = dec_word(emu, reg->I16_reg.x_reg);
}

static void common_binop_byte_rm_r(struct x86emu *emu,
				   u8 (*binop)(struct x86emu *, u8, u8))
{
	u32 destoffset;
	u8 *destreg, srcval;
	u8 destval;

	fetch_decode_modrm(emu);
	srcval = *decode_rh_byte_register(emu);
	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		destval = fetch_data_byte(emu, destoffset);
		destval = (*binop)(emu, destval, srcval);
		store_data_byte(emu, destoffset, destval);
	} else {
		destreg = decode_rl_byte_register(emu);
		*destreg = (*binop)(emu, *destreg, srcval);
	}
}

static void common_binop_ns_byte_rm_r(struct x86emu *emu,
				      void (*binop)(struct x86emu *, u8, u8))
{
	u32 destoffset;
	u8 destval, srcval;

	fetch_decode_modrm(emu);
	srcval = *decode_rh_byte_register(emu);
	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		destval = fetch_data_byte(emu, destoffset);
	} else {
		destval = *decode_rl_byte_register(emu);
	}
	(*binop)(emu, destval, srcval);
}

static void common_binop_word_rm_r(struct x86emu *emu,
				   u16 (*binop)(struct x86emu *, u16, u16))
{
	u32 destoffset;
	u16 destval, *destreg, srcval;

	fetch_decode_modrm(emu);
	srcval = *decode_rh_word_register(emu);
	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		destval = fetch_data_word(emu, destoffset);
		destval = (*binop)(emu, destval, srcval);
		store_data_word(emu, destoffset, destval);
	} else {
		destreg = decode_rl_word_register(emu);
		*destreg = (*binop)(emu, *destreg, srcval);
	}
}

static void common_binop_byte_r_rm(struct x86emu *emu,
				   u8 (*binop)(struct x86emu *, u8, u8))
{
	u8 *destreg, srcval;
	u32 srcoffset;

	fetch_decode_modrm(emu);
	destreg = decode_rh_byte_register(emu);
	if (emu->cur_mod != 3) {
		srcoffset = decode_rl_address(emu);
		srcval = fetch_data_byte(emu, srcoffset);
	} else {
		srcval = *decode_rl_byte_register(emu);
	}
	*destreg = (*binop)(emu, *destreg, srcval);
}

static void common_binop_long_rm_r(struct x86emu *emu,
				   u32 (*binop)(struct x86emu *, u32, u32))
{
	u32 destoffset;
	u32 destval, *destreg, srcval;

	fetch_decode_modrm(emu);
	srcval = *decode_rh_long_register(emu);
	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		destval = fetch_data_long(emu, destoffset);
		destval = (*binop)(emu, destval, srcval);
		store_data_long(emu, destoffset, destval);
	} else {
		destreg = decode_rl_long_register(emu);
		*destreg = (*binop)(emu, *destreg, srcval);
	}
}

static void common_binop_word_long_rm_r(struct x86emu *emu,
					u16 (*binop16)(struct x86emu *, u16, u16),
					u32 (*binop32)(struct x86emu *, u32, u32))
{
	if (MODE_DATA32)
		common_binop_long_rm_r(emu, binop32);
	else
		common_binop_word_rm_r(emu, binop16);
}

static void common_binop_ns_word_rm_r(struct x86emu *emu,
				      void (*binop)(struct x86emu *, u16, u16))
{
	u32 destoffset;
	u16 destval, srcval;

	fetch_decode_modrm(emu);
	srcval = *decode_rh_word_register(emu);
	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		destval = fetch_data_word(emu, destoffset);
	} else {
		destval = *decode_rl_word_register(emu);
	}
	(*binop)(emu, destval, srcval);
}

static void common_binop_ns_long_rm_r(struct x86emu *emu,
				      void (*binop)(struct x86emu *, u32, u32))
{
	u32 destoffset;
	u32 destval, srcval;

	fetch_decode_modrm(emu);
	srcval = *decode_rh_long_register(emu);
	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		destval = fetch_data_long(emu, destoffset);
	} else {
		destval = *decode_rl_long_register(emu);
	}
	(*binop)(emu, destval, srcval);
}

static void common_binop_ns_word_long_rm_r(struct x86emu *emu,
					   void (*binop16)(struct x86emu *, u16, u16),
					   void (*binop32)(struct x86emu *, u32, u32))
{
	if (MODE_DATA32)
		common_binop_ns_long_rm_r(emu, binop32);
	else
		common_binop_ns_word_rm_r(emu, binop16);
}

static void common_binop_long_r_rm(struct x86emu *emu,
				   u32 (*binop)(struct x86emu *, u32, u32))
{
	u32 srcoffset;
	u32 *destreg, srcval;

	fetch_decode_modrm(emu);
	destreg = decode_rh_long_register(emu);
	if (emu->cur_mod != 3) {
		srcoffset = decode_rl_address(emu);
		srcval = fetch_data_long(emu, srcoffset);
	} else {
		srcval = *decode_rl_long_register(emu);
	}
	*destreg = (*binop)(emu, *destreg, srcval);
}

static void common_binop_word_r_rm(struct x86emu *emu,
				   u16 (*binop)(struct x86emu *, u16, u16))
{
	u32 srcoffset;
	u16 *destreg, srcval;

	fetch_decode_modrm(emu);
	destreg = decode_rh_word_register(emu);
	if (emu->cur_mod != 3) {
		srcoffset = decode_rl_address(emu);
		srcval = fetch_data_word(emu, srcoffset);
	} else {
		srcval = *decode_rl_word_register(emu);
	}
	*destreg = (*binop)(emu, *destreg, srcval);
}

static void common_binop_word_long_r_rm(struct x86emu *emu,
					u16 (*binop16)(struct x86emu *, u16, u16),
					u32 (*binop32)(struct x86emu *, u32, u32))
{
	if (MODE_DATA32)
		common_binop_long_r_rm(emu, binop32);
	else
		common_binop_word_r_rm(emu, binop16);
}

static void common_binop_byte_imm(struct x86emu *emu,
				  u8 (*binop)(struct x86emu *, u8, u8))
{
	u8 srcval = fetch_byte_imm(emu);

	emu->x86.R_AL = (*binop)(emu, emu->x86.R_AL, srcval);
}

static void common_binop_word_long_imm(struct x86emu *emu,
				       u16 (*binop16)(struct x86emu *, u16, u16),
				       u32 (*binop32)(struct x86emu *, u32, u32))
{
	if (MODE_DATA32) {
		u32 srcval = fetch_long_imm(emu);

		emu->x86.R_EAX = (*binop32)(emu, emu->x86.R_EAX, srcval);
	} else {
		u16 srcval = fetch_word_imm(emu);

		emu->x86.R_AX = (*binop16)(emu, emu->x86.R_AX, srcval);
	}
}

static void common_push_word_long(struct x86emu *emu, union x86emu_register *reg)
{
	if (MODE_DATA32)
		push_long(emu, reg->I32_reg.e_reg);
	else
		push_word(emu, reg->I16_reg.x_reg);
}

static void common_pop_word_long(struct x86emu *emu, union x86emu_register *reg)
{
	if (MODE_DATA32)
		reg->I32_reg.e_reg = pop_long(emu);
	else
		reg->I16_reg.x_reg = pop_word(emu);
}

static void common_imul_long_IMM(struct x86emu *emu, bool byte_imm)
{
	u32 srcoffset;
	u32 *destreg, srcval, res, res_hi;
	s32 imm;
	s64 res64;

	fetch_decode_modrm(emu);
	destreg = decode_rh_long_register(emu);
	if (emu->cur_mod != 3) {
		srcoffset = decode_rl_address(emu);
		srcval = fetch_data_long(emu, srcoffset);
	} else {
		srcval = *decode_rl_long_register(emu);
	}

	if (byte_imm)
		imm = (s8)fetch_byte_imm(emu);
	else
		imm = fetch_long_imm(emu);

	res64 = (s64)(s32)srcval * imm;
	res = (u32)((u64)res64 & 0xffffffff);
	res_hi = (u32)((u64)res64 >> 32);
	x86emu_set_flags(emu, CF_SET(res_hi), LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res64), MSB_LONG(res), OF_SET(res_hi), OF_BOOL);
	*destreg = res;
}

static void common_imul_word_IMM(struct x86emu *emu, bool byte_imm)
{
	u32 srcoffset;
	u16 *destreg, srcval, res, res_hi;
	s16 imm;
	s32 res32;

	fetch_decode_modrm(emu);
	destreg = decode_rh_word_register(emu);
	if (emu->cur_mod != 3) {
		srcoffset = decode_rl_address(emu);
		srcval = fetch_data_word(emu, srcoffset);
	} else {
		srcval = *decode_rl_word_register(emu);
	}

	if (byte_imm)
		imm = (s8)fetch_byte_imm(emu);
	else
		imm = fetch_word_imm(emu);

	res32 = (s32)(s16)srcval * imm;
	res = (u16)((u32)res32 & 0xffff);
	res_hi = (u16)((u32)res32 >> 16);
	x86emu_set_flags(emu, CF_SET(res_hi), LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res32), MSB_WORD(res), OF_SET(res_hi), OF_BOOL);
	*destreg = res;
}

static void common_imul_imm(struct x86emu *emu, bool byte_imm)
{
	if (MODE_DATA32)
		common_imul_long_IMM(emu, byte_imm);
	else
		common_imul_word_IMM(emu, byte_imm);
}

static void common_jmp_near(struct x86emu *emu, bool cond)
{
	s8 offset = (s8)fetch_byte_imm(emu);
	u16 target = (u16)(emu->x86.R_IP + (s16)offset);

	if (cond)
		emu->x86.R_IP = target;
}

static void common_load_far_pointer(struct x86emu *emu, u16 *seg)
{
	u16 *dstreg;
	u32 srcoffset;

	fetch_decode_modrm(emu);
	if (emu->cur_mod == 3)
		x86emu_halt_sys(emu);

	dstreg = decode_rh_word_register(emu);
	srcoffset = decode_rl_address(emu);
	*dstreg = fetch_data_word(emu, srcoffset);
	*seg = fetch_data_word(emu, srcoffset + 2);
}

/* Opcode 0x3a */
static void x86emuOp_cmp_byte_R_RM(struct x86emu *emu)
{
	u8 *destreg, srcval;

	fetch_decode_modrm(emu);
	destreg = decode_rh_byte_register(emu);
	srcval = decode_and_fetch_byte(emu);
	cmp_byte(emu, *destreg, srcval);
}

/* Opcode 0x3b */
static void x86emuOp32_cmp_word_R_RM(struct x86emu *emu)
{
	u32 srcval, *destreg;

	fetch_decode_modrm(emu);
	destreg = decode_rh_long_register(emu);
	srcval = decode_and_fetch_long(emu);
	cmp_long(emu, *destreg, srcval);
}

static void x86emuOp16_cmp_word_R_RM(struct x86emu *emu)
{
	u16 srcval, *destreg;

	fetch_decode_modrm(emu);
	destreg = decode_rh_word_register(emu);
	srcval = decode_and_fetch_word(emu);
	cmp_word(emu, *destreg, srcval);
}

static void x86emuOp_cmp_word_R_RM(struct x86emu *emu)
{
	if (MODE_DATA32)
		x86emuOp32_cmp_word_R_RM(emu);
	else
		x86emuOp16_cmp_word_R_RM(emu);
}

/* Opcode 0x3c */
static void x86emuOp_cmp_byte_AL_IMM(struct x86emu *emu)
{
	u8 srcval = fetch_byte_imm(emu);

	cmp_byte(emu, emu->x86.R_AL, srcval);
}

/* Opcode 0x3d */
static void x86emuOp32_cmp_word_AX_IMM(struct x86emu *emu)
{
	u32 srcval = fetch_long_imm(emu);

	cmp_long(emu, emu->x86.R_EAX, srcval);
}

static void x86emuOp16_cmp_word_AX_IMM(struct x86emu *emu)
{
	u16 srcval = fetch_word_imm(emu);

	cmp_word(emu, emu->x86.R_AX, srcval);
}

static void x86emuOp_cmp_word_AX_IMM(struct x86emu *emu)
{
	if (MODE_DATA32)
		x86emuOp32_cmp_word_AX_IMM(emu);
	else
		x86emuOp16_cmp_word_AX_IMM(emu);
}

/* Opcode 0x60 */
static void x86emuOp_push_all(struct x86emu *emu)
{
	if (MODE_DATA32) {
		u32 old_esp = READ_ONCE(emu->x86.R_ESP);

		push_long(emu, emu->x86.R_EAX);
		push_long(emu, emu->x86.R_ECX);
		push_long(emu, emu->x86.R_EDX);
		push_long(emu, emu->x86.R_EBX);
		push_long(emu, old_esp);
		push_long(emu, READ_ONCE(emu->x86.R_EBP));
		push_long(emu, emu->x86.R_ESI);
		push_long(emu, emu->x86.R_EDI);
	} else {
		u16 old_sp = READ_ONCE(emu->x86.R_SP);

		push_word(emu, emu->x86.R_AX);
		push_word(emu, emu->x86.R_CX);
		push_word(emu, emu->x86.R_DX);
		push_word(emu, emu->x86.R_BX);
		push_word(emu, old_sp);
		push_word(emu, READ_ONCE(emu->x86.R_BP));
		push_word(emu, emu->x86.R_SI);
		push_word(emu, emu->x86.R_DI);
	}
}

/* Opcode 0x61 */
static void x86emuOp_pop_all(struct x86emu *emu)
{
	if (MODE_DATA32) {
		emu->x86.R_EDI = pop_long(emu);
		emu->x86.R_ESI = pop_long(emu);
		WRITE_ONCE(emu->x86.R_EBP, pop_long(emu));
		WRITE_ONCE(emu->x86.R_SP, READ_ONCE(emu->x86.R_SP) + 4); /* skip ESP */
		emu->x86.R_EBX = pop_long(emu);
		emu->x86.R_EDX = pop_long(emu);
		emu->x86.R_ECX = pop_long(emu);
		emu->x86.R_EAX = pop_long(emu);
	} else {
		emu->x86.R_DI = pop_word(emu);
		emu->x86.R_SI = pop_word(emu);
		WRITE_ONCE(emu->x86.R_BP, pop_word(emu));
		WRITE_ONCE(emu->x86.R_SP, READ_ONCE(emu->x86.R_SP) + 2); /* skip SP */
		emu->x86.R_BX = pop_word(emu);
		emu->x86.R_DX = pop_word(emu);
		emu->x86.R_CX = pop_word(emu);
		emu->x86.R_AX = pop_word(emu);
	}
}

/* Opcode 0x68 */
static void x86emuOp_push_word_IMM(struct x86emu *emu)
{
	if (MODE_DATA32) {
		u32 imm = fetch_long_imm(emu);

		push_long(emu, imm);
	} else {
		u16 imm = fetch_word_imm(emu);

		push_word(emu, imm);
	}
}

/* Opcode 0x6a */
static void x86emuOp_push_byte_IMM(struct x86emu *emu)
{
	s16 imm = (s8)fetch_byte_imm(emu);

	if (MODE_DATA32)
		push_long(emu, (s32)imm);
	else
		push_word(emu, imm);
}

/* Opcode 0x6d */
static void x86emuOp_ins_word(struct x86emu *emu)
{
	ins(emu, (MODE_DATA32) ? 4 : 2);
}

/* Opcode 0x6f */
static void x86emuOp_outs_word(struct x86emu *emu)
{
	outs(emu, (MODE_DATA32) ? 4 : 2);
}

/* Opcode 0x7c */
static void x86emuOp_jump_near_L(struct x86emu *emu)
{
	bool sf = SF_STATUS;
	bool of = OF_STATUS;

	common_jmp_near(emu, sf != of);
}

/* Opcode 0x7d */
static void x86emuOp_jump_near_NL(struct x86emu *emu)
{
	bool sf = SF_STATUS;
	bool of = OF_STATUS;

	common_jmp_near(emu, sf == of);
}

/* Opcode 0x7e */
static void x86emuOp_jump_near_LE(struct x86emu *emu)
{
	bool sf = SF_STATUS;
	bool of = OF_STATUS;

	common_jmp_near(emu, (sf != of) || ACCESS_FLAG(F_ZF));
}

/* Opcode 0x7f */
static void x86emuOp_jump_near_NLE(struct x86emu *emu)
{
	bool sf = SF_STATUS;
	bool of = OF_STATUS;

	common_jmp_near(emu, (sf == of) && !ACCESS_FLAG(F_ZF));
}

static u8 (*const opc80_byte_operation[])(struct x86emu *, u8, u8) = {
	add_byte, /* 00 */
	or_byte,  /* 01 */
	adc_byte, /* 02 */
	sbb_byte, /* 03 */
	and_byte, /* 04 */
	sub_byte, /* 05 */
	xor_byte, /* 06 */
	cmp_byte_r, /* 07 */
};

/* Opcode 0x80 */
static void x86emuOp_opc80_byte_RM_IMM(struct x86emu *emu)
{
	u8 imm, destval;

	fetch_decode_modrm(emu);
	destval = decode_and_fetch_byte(emu);
	imm = fetch_byte_imm(emu);
	destval = (*opc80_byte_operation[emu->cur_rh])(emu, destval, imm);
	if (emu->cur_rh != 7)
		write_back_byte(emu, destval);
}

static u16 (*const opc81_word_operation[])(struct x86emu *, u16, u16) = {
	add_word, /* 00 */
	or_word,  /* 01 */
	adc_word, /* 02 */
	sbb_word, /* 03 */
	and_word, /* 04 */
	sub_word, /* 05 */
	xor_word, /* 06 */
	cmp_word_r, /* 07 */
};

static u32 (*const opc81_long_operation[])(struct x86emu *, u32, u32) = {
	add_long, /* 00 */
	or_long,  /* 01 */
	adc_long, /* 02 */
	sbb_long, /* 03 */
	and_long, /* 04 */
	sub_long, /* 05 */
	xor_long, /* 06 */
	cmp_long_r, /* 07 */
};

/* Opcode 0x81 */
static void x86emuOp32_opc81_word_RM_IMM(struct x86emu *emu)
{
	u32 destval, imm;

	fetch_decode_modrm(emu);
	destval = decode_and_fetch_long(emu);
	imm = fetch_long_imm(emu);
	destval = (*opc81_long_operation[emu->cur_rh])(emu, destval, imm);
	if (emu->cur_rh != 7)
		write_back_long(emu, destval);
}

static void x86emuOp16_opc81_word_RM_IMM(struct x86emu *emu)
{
	u16 destval, imm;

	fetch_decode_modrm(emu);
	destval = decode_and_fetch_word(emu);
	imm = fetch_word_imm(emu);
	destval = (*opc81_word_operation[emu->cur_rh])(emu, destval, imm);
	if (emu->cur_rh != 7)
		write_back_word(emu, destval);
}

static void x86emuOp_opc81_word_RM_IMM(struct x86emu *emu)
{
	if (MODE_DATA32)
		x86emuOp32_opc81_word_RM_IMM(emu);
	else
		x86emuOp16_opc81_word_RM_IMM(emu);
}

static u8 (*const opc82_byte_operation[])(struct x86emu *, u8, u8) = {
	add_byte, /* 00 */
	or_byte,  /* 01 */
	adc_byte, /* 02 */
	sbb_byte, /* 03 */
	and_byte, /* 04 */
	sub_byte, /* 05 */
	xor_byte, /* 06 */
	cmp_byte_r, /* 07 */
};

/* Opcode 0x82 */
static void x86emuOp_opc82_byte_RM_IMM(struct x86emu *emu)
{
	u8 imm, destval;

	/*
	 * The decoded instruction Similar to opcode 0x81, except that
	 * the immediate byte is sign extended to a word length.
	 */
	fetch_decode_modrm(emu);
	destval = decode_and_fetch_byte(emu);
	imm = fetch_byte_imm(emu);
	destval = (*opc82_byte_operation[emu->cur_rh])(emu, destval, imm);
	if (emu->cur_rh != 7)
		write_back_byte(emu, destval);
}

static u16 (*const opc83_word_operation[])(struct x86emu *, u16, u16) = {
	add_word, /* 00 */
	or_word,  /* 01 - 386+ */
	adc_word, /* 02 */
	sbb_word, /* 03 */
	and_word, /* 04 - 386+ */
	sub_word, /* 05 */
	xor_word, /* 06 - 386+ */
	cmp_word_r, /* 07 */
};

static u32 (*const opc83_long_operation[])(struct x86emu *, u32, u32) = {
	add_long, /* 00 */
	or_long,  /* 01 - 386+ */
	adc_long, /* 02 */
	sbb_long, /* 03 */
	and_long, /* 04 - 386+ */
	sub_long, /* 05 */
	xor_long, /* 06 - 386+ */
	cmp_long_r, /* 07 */
};

/* Opcode 0x83 */
static void x86emuOp32_opc83_word_RM_IMM(struct x86emu *emu)
{
	u32 destval, imm;

	fetch_decode_modrm(emu);
	destval = decode_and_fetch_long(emu);
	imm = (s8)fetch_byte_imm(emu);
	destval = (*opc83_long_operation[emu->cur_rh])(emu, destval, imm);
	if (emu->cur_rh != 7)
		write_back_long(emu, destval);
}

static void x86emuOp16_opc83_word_RM_IMM(struct x86emu *emu)
{
	u16 destval, imm;

	fetch_decode_modrm(emu);
	destval = decode_and_fetch_word(emu);
	imm = (s8)fetch_byte_imm(emu);
	destval = (*opc83_word_operation[emu->cur_rh])(emu, destval, imm);
	if (emu->cur_rh != 7)
		write_back_word(emu, destval);
}

static void x86emuOp_opc83_word_RM_IMM(struct x86emu *emu)
{
	if (MODE_DATA32)
		x86emuOp32_opc83_word_RM_IMM(emu);
	else
		x86emuOp16_opc83_word_RM_IMM(emu);
}

/* Opcode 0x86 */
static void x86emuOp_xchg_byte_RM_R(struct x86emu *emu)
{
	u8 *srcreg, destval, new_val8;

	fetch_decode_modrm(emu);
	destval = decode_and_fetch_byte(emu);
	srcreg = decode_rh_byte_register(emu);
	new_val8 = destval;
	destval = *srcreg;
	*srcreg = new_val8;
	write_back_byte(emu, destval);
}

/* Opcode 0x87 */
static void x86emuOp32_xchg_word_RM_R(struct x86emu *emu)
{
	u32 *srcreg, destval, new_val32;

	fetch_decode_modrm(emu);
	destval = decode_and_fetch_long(emu);
	srcreg = decode_rh_long_register(emu);
	new_val32 = destval;
	destval = *srcreg;
	*srcreg = new_val32;
	write_back_long(emu, destval);
}

static void x86emuOp16_xchg_word_RM_R(struct x86emu *emu)
{
	u16 *srcreg, destval, new_val16;

	fetch_decode_modrm(emu);
	destval = decode_and_fetch_word(emu);
	srcreg = decode_rh_word_register(emu);
	new_val16 = destval;
	destval = *srcreg;
	*srcreg = new_val16;
	write_back_word(emu, destval);
}

static void x86emuOp_xchg_word_RM_R(struct x86emu *emu)
{
	if (MODE_DATA32)
		x86emuOp32_xchg_word_RM_R(emu);
	else
		x86emuOp16_xchg_word_RM_R(emu);
}

/* Opcode 0x88 */
static void x86emuOp_mov_byte_RM_R(struct x86emu *emu)
{
	u8 *destreg, *srcreg;
	u32 destoffset;

	fetch_decode_modrm(emu);
	srcreg = decode_rh_byte_register(emu);
	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		store_data_byte(emu, destoffset, *srcreg);
	} else {
		destreg = decode_rl_byte_register(emu);
		*destreg = *srcreg;
	}
}

/* Opcode 0x89 */
static void x86emuOp32_mov_word_RM_R(struct x86emu *emu)
{
	u32 destoffset;
	u32 *destreg, srcval;

	fetch_decode_modrm(emu);
	srcval = *decode_rh_long_register(emu);
	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		store_data_long(emu, destoffset, srcval);
	} else {
		destreg = decode_rl_long_register(emu);
		*destreg = srcval;
	}
}

static void x86emuOp16_mov_word_RM_R(struct x86emu *emu)
{
	u32 destoffset;
	u16 *destreg, srcval;

	fetch_decode_modrm(emu);
	srcval = *decode_rh_word_register(emu);
	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		store_data_word(emu, destoffset, srcval);
	} else {
		destreg = decode_rl_word_register(emu);
		*destreg = srcval;
	}
}

static void x86emuOp_mov_word_RM_R(struct x86emu *emu)
{
	if (MODE_DATA32)
		x86emuOp32_mov_word_RM_R(emu);
	else
		x86emuOp16_mov_word_RM_R(emu);
}

/* Opcode 0x8a */
static void x86emuOp_mov_byte_R_RM(struct x86emu *emu)
{
	u8 *destreg;

	fetch_decode_modrm(emu);
	destreg = decode_rh_byte_register(emu);
	*destreg = decode_and_fetch_byte(emu);
}

/* Opcode 0x8b */
static void x86emuOp_mov_word_R_RM(struct x86emu *emu)
{
	if (MODE_DATA32) {
		u32 *destreg;

		fetch_decode_modrm(emu);
		destreg = decode_rh_long_register(emu);
		*destreg = decode_and_fetch_long(emu);
	} else {
		u16 *destreg;

		fetch_decode_modrm(emu);
		destreg = decode_rh_word_register(emu);
		*destreg = decode_and_fetch_word(emu);
	}
}

/* Opcode 0x8c */
static void x86emuOp_mov_word_RM_SR(struct x86emu *emu)
{
	u16 *destreg, srcval;
	u32 destoffset;

	fetch_decode_modrm(emu);
	srcval = *decode_rh_seg_register(emu);
	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		store_data_word(emu, destoffset, srcval);
	} else {
		destreg = decode_rl_word_register(emu);
		*destreg = srcval;
	}
}

/* Opcode 0x8d */
static void x86emuOp_lea_word_R_M(struct x86emu *emu)
{
	u32 destoffset;

	fetch_decode_modrm(emu);
	if (emu->cur_mod == 3)
		x86emu_halt_sys(emu);

	destoffset = decode_rl_address(emu);
	if (MODE_ADDR32) {
		u32 *srcreg = decode_rh_long_register(emu);

		*srcreg = destoffset;
	} else {
		u16 *srcreg = decode_rh_word_register(emu);

		*srcreg = (u16)destoffset;
	}
}

/* Opcode 0x8e */
static void x86emuOp_mov_word_SR_RM(struct x86emu *emu)
{
	u16 *destreg;

	fetch_decode_modrm(emu);
	destreg = decode_rh_seg_register(emu);
	*destreg = decode_and_fetch_word(emu);
	/*
	 * Clean up, and reset all the R_xSP pointers to the correct
	 * locations. This is about 3x too much overhead (doing all the
	 * segreg ptrs when only one is needed, but this instruction
	 * *cannot* be that common, and this isn't too much work anyway.
	 */
}

/* Opcode 0x8f */
static void x86emuOp32_pop_RM(struct x86emu *emu)
{
	u32 destoffset;
	u32 destval, *destreg;

	fetch_decode_modrm(emu);
	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		destval = pop_long(emu);
		store_data_long(emu, destoffset, destval);
	} else {
		destreg = decode_rl_long_register(emu);
		*destreg = pop_long(emu);
	}
}

static void x86emuOp16_pop_RM(struct x86emu *emu)
{
	u32 destoffset;
	u16 destval, *destreg;

	fetch_decode_modrm(emu);
	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		destval = pop_word(emu);
		store_data_word(emu, destoffset, destval);
	} else {
		destreg = decode_rl_word_register(emu);
		*destreg = pop_word(emu);
	}
}

static void x86emuOp_pop_RM(struct x86emu *emu)
{
	if (MODE_DATA32)
		x86emuOp32_pop_RM(emu);
	else
		x86emuOp16_pop_RM(emu);
}

/* Opcode 0x91 */
static void x86emuOp_xchg_word_AX_CX(struct x86emu *emu)
{
	if (MODE_DATA32) {
		u32 new_ecx = emu->x86.R_EAX;

		emu->x86.R_EAX = emu->x86.R_ECX;
		emu->x86.R_ECX = new_ecx;
	} else {
		u16 new_cx = emu->x86.R_AX;

		emu->x86.R_AX = emu->x86.R_CX;
		emu->x86.R_CX = new_cx;
	}
}

/* Opcode 0x92 */
static void x86emuOp_xchg_word_AX_DX(struct x86emu *emu)
{
	if (MODE_DATA32) {
		u32 new_edx = emu->x86.R_EAX;

		emu->x86.R_EAX = emu->x86.R_EDX;
		emu->x86.R_EDX = new_edx;
	} else {
		u16 new_dx = emu->x86.R_AX;

		emu->x86.R_AX = emu->x86.R_DX;
		emu->x86.R_DX = new_dx;
	}
}

/* Opcode 0x93 */
static void x86emuOp_xchg_word_AX_BX(struct x86emu *emu)
{
	if (MODE_DATA32) {
		u32 new_ebx = emu->x86.R_EAX;

		emu->x86.R_EAX = emu->x86.R_EBX;
		emu->x86.R_EBX = new_ebx;
	} else {
		u16 new_bx = emu->x86.R_AX;

		emu->x86.R_AX = emu->x86.R_BX;
		emu->x86.R_BX = new_bx;
	}
}

/* Opcode 0x94 */
static void x86emuOp_xchg_word_AX_SP(struct x86emu *emu)
{
	if (MODE_DATA32) {
		u32 new_esp = emu->x86.R_EAX;

		emu->x86.R_EAX = READ_ONCE(emu->x86.R_ESP);
		WRITE_ONCE(emu->x86.R_ESP, new_esp);
	} else {
		u16 new_sp = emu->x86.R_AX;

		emu->x86.R_AX = READ_ONCE(emu->x86.R_SP);
		WRITE_ONCE(emu->x86.R_SP, new_sp);
	}
}

/* Opcode 0x95 */
static void x86emuOp_xchg_word_AX_BP(struct x86emu *emu)
{
	if (MODE_DATA32) {
		u32 new_ebp = emu->x86.R_EAX;

		emu->x86.R_EAX = READ_ONCE(emu->x86.R_EBP);
		WRITE_ONCE(emu->x86.R_EBP, new_ebp);
	} else {
		u16 new_bp = emu->x86.R_AX;

		emu->x86.R_AX = READ_ONCE(emu->x86.R_BP);
		WRITE_ONCE(emu->x86.R_BP, new_bp);
	}
}

/* Opcode 0x96 */
static void x86emuOp_xchg_word_AX_SI(struct x86emu *emu)
{
	if (MODE_DATA32) {
		u32 new_esi = emu->x86.R_EAX;

		emu->x86.R_EAX = emu->x86.R_ESI;
		emu->x86.R_ESI = new_esi;
	} else {
		u16 new_si = emu->x86.R_AX;

		emu->x86.R_AX = emu->x86.R_SI;
		emu->x86.R_SI = new_si;
	}
}

/* Opcode 0x97 */
static void x86emuOp_xchg_word_AX_DI(struct x86emu *emu)
{
	if (MODE_DATA32) {
		u32 new_edi = emu->x86.R_EAX;

		emu->x86.R_EAX = emu->x86.R_EDI;
		emu->x86.R_EDI = new_edi;
	} else {
		u16 new_di = emu->x86.R_AX;

		emu->x86.R_AX = emu->x86.R_DI;
		emu->x86.R_DI = new_di;
	}
}

/* Opcode 0x98 */
static void x86emuOp_cbw(struct x86emu *emu)
{
	if (MODE_DATA32) {
		if (MSB_WORD(emu->x86.R_AX))
			emu->x86.R_EAX |= 0xffff0000;
		else
			emu->x86.R_EAX &= 0x0000ffff;
	} else {
		if (MSB_BYTE(emu->x86.R_AL))
			emu->x86.R_AH = 0xff;
		else
			emu->x86.R_AH = 0x00;
	}
}

/* Opcode 0x99 */
static void x86emuOp_cwd(struct x86emu *emu)
{
	if (MODE_DATA32) {
		if (MSB_LONG(emu->x86.R_EAX))
			emu->x86.R_EDX = 0xffffffff;
		else
			emu->x86.R_EDX = 0x00000000;
	} else {
		if (MSB_WORD(emu->x86.R_AX))
			emu->x86.R_DX = 0xffff;
		else
			emu->x86.R_DX = 0x0000;
	}
}

/* Opcode 0x9a */
static void x86emuOp_call_far_IMM(struct x86emu *emu)
{
	u16 faroff = fetch_word_imm(emu);
	u16 farseg = fetch_word_imm(emu);

	/*
	 * Hooked interrupt vectors calling into our "BIOS" will cause problems
	 * unless all intersegment stuff is checked for BIOS access. Check
	 * needed here.
	 */
	push_word(emu, emu->x86.R_CS);
	emu->x86.R_CS = farseg;
	push_word(emu, emu->x86.R_IP);
	emu->x86.R_IP = faroff;
}

/* Opcode 0x9c */
static void x86emuOp_pushf_word(struct x86emu *emu)
{
	if (MODE_DATA32)
		push_long(emu, GET_PPF_FLAGS(emu->x86.R_EFLG));
	else
		push_word(emu, GET_PPF_FLAGS(emu->x86.R_FLG));
}

/* Opcode 0x9d */
static void x86emuOp_popf_word(struct x86emu *emu)
{
	if (MODE_DATA32)
		emu->x86.R_EFLG = GET_PPF_FLAGS(pop_long(emu));
	else
		emu->x86.R_EFLG = (u32)GET_PPF_FLAGS(pop_word(emu));
}

/* Opcode 0x9e */
static void x86emuOp_sahf(struct x86emu *emu)
{
	emu->x86.R_PSBL = GET_LSF_FLAGS(emu->x86.R_AH);
}

/* Opcode 0x9f */
static void x86emuOp_lahf(struct x86emu *emu)
{
	emu->x86.R_AH = GET_LSF_FLAGS(emu->x86.R_PSBL);
}

/* Opcode 0xa0 */
static void x86emuOp_mov_AL_M_IMM(struct x86emu *emu)
{
	u16 offset = fetch_word_imm(emu);

	emu->x86.R_AL = fetch_data_byte(emu, offset);
}

/* Opcode 0xa1 */
static void x86emuOp_mov_AX_M_IMM(struct x86emu *emu)
{
	u16 offset = fetch_word_imm(emu);

	if (MODE_DATA32)
		emu->x86.R_EAX = fetch_data_long(emu, offset);
	else
		emu->x86.R_AX = fetch_data_word(emu, offset);
}

/* Opcode 0xa2 */
static void x86emuOp_mov_M_AL_IMM(struct x86emu *emu)
{
	u16 offset = fetch_word_imm(emu);

	store_data_byte(emu, offset, emu->x86.R_AL);
}

/* Opcode 0xa3 */
static void x86emuOp_mov_M_AX_IMM(struct x86emu *emu)
{
	u16 offset = fetch_word_imm(emu);

	if (MODE_DATA32)
		store_data_long(emu, offset, emu->x86.R_EAX);
	else
		store_data_word(emu, offset, emu->x86.R_AX);
}

/* Opcode 0xa4 */
static void x86emuOp_movs_byte(struct x86emu *emu)
{
	u32 count = 1;
	s32 inc = (ACCESS_FLAG(F_DF)) ? -1 : 1;

	if (MODE_REP) {
		/* Don't care whether REPE or REPNE, move them until CX is ZERO */
		count = emu->x86.R_CX;
		emu->x86.R_CX = 0;
		CLEAR_MODE_REP;
	}
	while (count--) {
		u8 val = fetch_data_byte(emu, emu->x86.R_SI);

		store_byte(emu, emu->x86.R_ES, emu->x86.R_DI, val);
		emu->x86.R_SI += inc;
		emu->x86.R_DI += inc;
	}
}

/* Opcode 0xa5 */
static void x86emuOp_movs_word(struct x86emu *emu)
{
	u32 val, count = 1;
	s32 inc = (MODE_DATA32) ? 4 : 2;

	if (ACCESS_FLAG(F_DF))
		inc = -inc;

	if (MODE_REP) {
		/* Don't care whether REPE or REPNE, move them until CX is ZERO */
		count = emu->x86.R_CX;
		emu->x86.R_CX = 0;
		CLEAR_MODE_REP;
	}
	while (count--) {
		if (MODE_DATA32) {
			val = fetch_data_long(emu, emu->x86.R_SI);
			store_long(emu, emu->x86.R_ES, emu->x86.R_DI, val);
		} else {
			val = fetch_data_word(emu, emu->x86.R_SI);
			store_word(emu, emu->x86.R_ES, emu->x86.R_DI, (u16)val);
		}
		emu->x86.R_SI += inc;
		emu->x86.R_DI += inc;
	}
}

/* Opcode 0xa6 */
static void x86emuOp_cmps_byte(struct x86emu *emu)
{
	s8 val1, val2;
	s32 inc = (ACCESS_FLAG(F_DF)) ? -1 : 1;

	if (MODE_REPE) {
		/* REPE, move them until CX is ZERO */
		while (emu->x86.R_CX != 0) {
			val1 = fetch_data_byte(emu, emu->x86.R_SI);
			val2 = fetch_byte(emu, emu->x86.R_ES, emu->x86.R_DI);
			cmp_byte(emu, val1, val2);
			emu->x86.R_CX -= 1;
			emu->x86.R_SI += inc;
			emu->x86.R_DI += inc;
			if (!ACCESS_FLAG(F_ZF))
				break;
		}
		CLEAR_MODE_REPE;
	} else if (MODE_REPNE) {
		/* REPNE, move them until CX is ZERO */
		while (emu->x86.R_CX != 0) {
			val1 = fetch_data_byte(emu, emu->x86.R_SI);
			val2 = fetch_byte(emu, emu->x86.R_ES, emu->x86.R_DI);
			cmp_byte(emu, val1, val2);
			emu->x86.R_CX -= 1;
			emu->x86.R_SI += inc;
			emu->x86.R_DI += inc;
			if (ACCESS_FLAG(F_ZF))
				break; /* zero flag set means equal */
		}
		CLEAR_MODE_REPNE;
	} else {
		val1 = fetch_data_byte(emu, emu->x86.R_SI);
		val2 = fetch_byte(emu, emu->x86.R_ES, emu->x86.R_DI);
		cmp_byte(emu, val1, val2);
		emu->x86.R_SI += inc;
		emu->x86.R_DI += inc;
	}
}

/* Opcode 0xa7 */
static void x86emuOp_cmps_word(struct x86emu *emu)
{
	u32 val1, val2;
	s32 inc = (MODE_DATA32) ? 4 : 2;

	if (ACCESS_FLAG(F_DF))
		inc = -inc;

	if (MODE_REPE) {
		/* REPE, move them until CX is ZERO */
		while (emu->x86.R_CX != 0) {
			if (MODE_DATA32) {
				val1 = fetch_data_long(emu, emu->x86.R_SI);
				val2 = fetch_long(emu, emu->x86.R_ES, emu->x86.R_DI);
				cmp_long(emu, val1, val2);
			} else {
				val1 = fetch_data_word(emu, emu->x86.R_SI);
				val2 = fetch_word(emu, emu->x86.R_ES, emu->x86.R_DI);
				cmp_word(emu, (u16)val1, (u16)val2);
			}
			emu->x86.R_CX -= 1;
			emu->x86.R_SI += inc;
			emu->x86.R_DI += inc;
			if (!ACCESS_FLAG(F_ZF))
				break;
		}
		CLEAR_MODE_REPE;
	} else if (MODE_REPNE) {
		/* REPNE, move them until CX is ZERO */
		while (emu->x86.R_CX != 0) {
			if (MODE_DATA32) {
				val1 = fetch_data_long(emu, emu->x86.R_SI);
				val2 = fetch_long(emu, emu->x86.R_ES, emu->x86.R_DI);
				cmp_long(emu, val1, val2);
			} else {
				val1 = fetch_data_word(emu, emu->x86.R_SI);
				val2 = fetch_word(emu, emu->x86.R_ES, emu->x86.R_DI);
				cmp_word(emu, (u16)val1, (u16)val2);
			}
			emu->x86.R_CX -= 1;
			emu->x86.R_SI += inc;
			emu->x86.R_DI += inc;
			if (ACCESS_FLAG(F_ZF))
				break; /* zero flag set means equal */
		}
		CLEAR_MODE_REPNE;
	} else {
		if (MODE_DATA32) {
			val1 = fetch_data_long(emu, emu->x86.R_SI);
			val2 = fetch_long(emu, emu->x86.R_ES, emu->x86.R_DI);
			cmp_long(emu, val1, val2);
		} else {
			val1 = fetch_data_word(emu, emu->x86.R_SI);
			val2 = fetch_word(emu, emu->x86.R_ES, emu->x86.R_DI);
			cmp_word(emu, (u16)val1, (u16)val2);
		}
		emu->x86.R_SI += inc;
		emu->x86.R_DI += inc;
	}
}

/* Opcode 0xa9 */
static void x86emuOp_test_AX_IMM(struct x86emu *emu)
{
	if (MODE_DATA32)
		test_long(emu, emu->x86.R_EAX, fetch_long_imm(emu));
	else
		test_word(emu, emu->x86.R_AX, fetch_word_imm(emu));
}

/* Opcode 0xaa */
static void x86emuOp_stos_byte(struct x86emu *emu)
{
	s32 inc = (ACCESS_FLAG(F_DF)) ? -1 : 1;

	if (MODE_REP) {
		/* Don't care whether REPE or REPNE, move them until CX is ZERO */
		while (emu->x86.R_CX != 0) {
			store_byte(emu, emu->x86.R_ES, emu->x86.R_DI, emu->x86.R_AL);
			emu->x86.R_CX -= 1;
			emu->x86.R_DI += inc;
		}
		CLEAR_MODE_REP;
	} else {
		store_byte(emu, emu->x86.R_ES, emu->x86.R_DI, emu->x86.R_AL);
		emu->x86.R_DI += inc;
	}
}

/* Opcode 0xab */
static void x86emuOp_stos_word(struct x86emu *emu)
{
	u32 count = 1;
	s32 inc = (MODE_DATA32) ? 4 : 2;

	if (ACCESS_FLAG(F_DF))
		inc = -inc;

	if (MODE_REP) {
		/* Don't care whether REPE or REPNE, move them until CX is ZERO */
		count = emu->x86.R_CX;
		emu->x86.R_CX = 0;
		CLEAR_MODE_REP;
	}
	while (count--) {
		if (MODE_DATA32)
			store_long(emu, emu->x86.R_ES, emu->x86.R_DI, emu->x86.R_EAX);
		else
			store_word(emu, emu->x86.R_ES, emu->x86.R_DI, emu->x86.R_AX);
		emu->x86.R_DI += inc;
	}
}

/* Opcode 0xac */
static void x86emuOp_lods_byte(struct x86emu *emu)
{
	s32 inc = (ACCESS_FLAG(F_DF)) ? -1 : 1;

	if (MODE_REP) {
		/* Don't care whether REPE or REPNE, move them until CX is ZERO */
		while (emu->x86.R_CX != 0) {
			emu->x86.R_AL = fetch_data_byte(emu, emu->x86.R_SI);
			emu->x86.R_CX -= 1;
			emu->x86.R_SI += inc;
		}
		CLEAR_MODE_REP;
	} else {
		emu->x86.R_AL = fetch_data_byte(emu, emu->x86.R_SI);
		emu->x86.R_SI += inc;
	}
}

/* Opcode 0xad */
static void x86emuOp_lods_word(struct x86emu *emu)
{
	u32 count = 1;
	s32 inc = (MODE_DATA32) ? 4 : 2;

	if (ACCESS_FLAG(F_DF))
		inc = -inc;

	if (MODE_REP) {
		/* Don't care whether REPE or REPNE, move them until CX is ZERO */
		count = emu->x86.R_CX;
		emu->x86.R_CX = 0;
		CLEAR_MODE_REP;
	}
	while (count--) {
		if (MODE_DATA32)
			emu->x86.R_EAX = fetch_data_long(emu, emu->x86.R_SI);
		else
			emu->x86.R_AX = fetch_data_word(emu, emu->x86.R_SI);
		emu->x86.R_SI += inc;
	}
}

/* Opcode 0xae */
static void x86emuOp_scas_byte(struct x86emu *emu)
{
	s8 val2;
	s32 inc = (ACCESS_FLAG(F_DF)) ? -1 : 1;

	if (MODE_REPE) {
		/* REPE, move them until CX is ZERO */
		while (emu->x86.R_CX != 0) {
			val2 = fetch_byte(emu, emu->x86.R_ES, emu->x86.R_DI);
			cmp_byte(emu, emu->x86.R_AL, val2);
			emu->x86.R_CX -= 1;
			emu->x86.R_DI += inc;
			if (!ACCESS_FLAG(F_ZF))
				break;
		}
		CLEAR_MODE_REPE;
	} else if (MODE_REPNE) {
		/* REPNE, move them until CX is ZERO */
		while (emu->x86.R_CX != 0) {
			val2 = fetch_byte(emu, emu->x86.R_ES, emu->x86.R_DI);
			cmp_byte(emu, emu->x86.R_AL, val2);
			emu->x86.R_CX -= 1;
			emu->x86.R_DI += inc;
			if (ACCESS_FLAG(F_ZF))
				break; /* zero flag set means equal */
		}
		CLEAR_MODE_REPNE;
	} else {
		val2 = fetch_byte(emu, emu->x86.R_ES, emu->x86.R_DI);
		cmp_byte(emu, emu->x86.R_AL, val2);
		emu->x86.R_DI += inc;
	}
}

/* Opcode 0xaf */
static void x86emuOp_scas_word(struct x86emu *emu)
{
	u32 val;
	s32 inc = (MODE_DATA32) ? 4 : 2;

	if (ACCESS_FLAG(F_DF))
		inc = -inc;

	if (MODE_REPE) {
		/* REPE, move them until CX is ZERO */
		while (emu->x86.R_CX != 0) {
			if (MODE_DATA32) {
				val = fetch_long(emu, emu->x86.R_ES, emu->x86.R_DI);
				cmp_long(emu, emu->x86.R_EAX, val);
			} else {
				val = fetch_word(emu, emu->x86.R_ES, emu->x86.R_DI);
				cmp_word(emu, emu->x86.R_AX, (u16)val);
			}
			emu->x86.R_CX -= 1;
			emu->x86.R_DI += inc;
			if (!ACCESS_FLAG(F_ZF))
				break;
		}
		CLEAR_MODE_REPE;
	} else if (MODE_REPNE) {
		/* REPNE, move them until CX is ZERO */
		while (emu->x86.R_CX != 0) {
			if (MODE_DATA32) {
				val = fetch_long(emu, emu->x86.R_ES, emu->x86.R_DI);
				cmp_long(emu, emu->x86.R_EAX, val);
			} else {
				val = fetch_word(emu, emu->x86.R_ES, emu->x86.R_DI);
				cmp_word(emu, emu->x86.R_AX, (u16)val);
			}
			emu->x86.R_CX -= 1;
			emu->x86.R_DI += inc;
			if (ACCESS_FLAG(F_ZF))
				break; /* zero flag set means equal */
		}
		CLEAR_MODE_REPNE;
	} else {
		if (MODE_DATA32) {
			val = fetch_long(emu, emu->x86.R_ES, emu->x86.R_DI);
			cmp_long(emu, emu->x86.R_EAX, val);
		} else {
			val = fetch_word(emu, emu->x86.R_ES, emu->x86.R_DI);
			cmp_word(emu, emu->x86.R_AX, (u16)val);
		}
		emu->x86.R_DI += inc;
	}
}

/* Opcode 0xb8 */
static void x86emuOp_mov_word_AX_IMM(struct x86emu *emu)
{
	if (MODE_DATA32)
		emu->x86.R_EAX = fetch_long_imm(emu);
	else
		emu->x86.R_AX = fetch_word_imm(emu);
}

/* Opcode 0xb9 */
static void x86emuOp_mov_word_CX_IMM(struct x86emu *emu)
{
	if (MODE_DATA32)
		emu->x86.R_ECX = fetch_long_imm(emu);
	else
		emu->x86.R_CX = fetch_word_imm(emu);
}

/* Opcode 0xba */
static void x86emuOp_mov_word_DX_IMM(struct x86emu *emu)
{
	if (MODE_DATA32)
		emu->x86.R_EDX = fetch_long_imm(emu);
	else
		emu->x86.R_DX = fetch_word_imm(emu);
}

/* Opcode 0xbb */
static void x86emuOp_mov_word_BX_IMM(struct x86emu *emu)
{
	if (MODE_DATA32)
		emu->x86.R_EBX = fetch_long_imm(emu);
	else
		emu->x86.R_BX = fetch_word_imm(emu);
}

/* Opcode 0xbc */
static void x86emuOp_mov_word_SP_IMM(struct x86emu *emu)
{
	if (MODE_DATA32)
		WRITE_ONCE(emu->x86.R_ESP, fetch_long_imm(emu));
	else
		WRITE_ONCE(emu->x86.R_SP, fetch_word_imm(emu));
}

/* Opcode 0xbd */
static void x86emuOp_mov_word_BP_IMM(struct x86emu *emu)
{
	if (MODE_DATA32)
		WRITE_ONCE(emu->x86.R_EBP, fetch_long_imm(emu));
	else
		WRITE_ONCE(emu->x86.R_BP, fetch_word_imm(emu));
}

/* Opcode 0xbe */
static void x86emuOp_mov_word_SI_IMM(struct x86emu *emu)
{
	if (MODE_DATA32)
		emu->x86.R_ESI = fetch_long_imm(emu);
	else
		emu->x86.R_SI = fetch_word_imm(emu);
}

/* Opcode 0xbf */
static void x86emuOp_mov_word_DI_IMM(struct x86emu *emu)
{
	if (MODE_DATA32)
		emu->x86.R_EDI = fetch_long_imm(emu);
	else
		emu->x86.R_DI = fetch_word_imm(emu);
}

/* Used by opcodes 0xc0, 0xd0 and 0xd2 */
static u8 (*const opcD0_byte_operation[])(struct x86emu *, u8, u8) = {
	rol_byte,
	ror_byte,
	rcl_byte,
	rcr_byte,
	shl_byte,
	shr_byte,
	shl_byte,
	sar_byte,
};

/* Opcode 0xc0 */
static void x86emuOp_opcC0_byte_RM_MEM(struct x86emu *emu)
{
	u8 destval, amt;

	fetch_decode_modrm(emu);
	/* Decode the mod byte to find the addressing mode */
	destval = decode_and_fetch_byte_imm8(emu, &amt);
	destval = (*opcD0_byte_operation[emu->cur_rh])(emu, destval, amt);
	write_back_byte(emu, destval);
}

/* Used by opcodes 0xc1, 0xd1 and 0xd3 */
static u16 (*const opcD1_word_operation[])(struct x86emu *, u16, u8) = {
	rol_word,
	ror_word,
	rcl_word,
	rcr_word,
	shl_word,
	shr_word,
	shl_word,
	sar_word,
};

/* Used by opcodes 0xc1, 0xd1 and 0xd3 */
static u32 (*const opcD1_long_operation[])(struct x86emu *, u32, u8) = {
	rol_long,
	ror_long,
	rcl_long,
	rcr_long,
	shl_long,
	shr_long,
	shl_long,
	sar_long,
};

/* Opcode 0xc1 */
static void x86emuOp_opcC1_word_RM_MEM(struct x86emu *emu)
{
	u8 amt;

	fetch_decode_modrm(emu);
	if (MODE_DATA32) {
		u32 destval;

		destval = decode_and_fetch_long_imm8(emu, &amt);
		destval = (*opcD1_long_operation[emu->cur_rh])(emu, destval, amt);
		write_back_long(emu, destval);
	} else {
		u16 destval;

		destval = decode_and_fetch_word_imm8(emu, &amt);
		destval = (*opcD1_word_operation[emu->cur_rh])(emu, destval, amt);
		write_back_word(emu, destval);
	}
}

/* Opcode 0xc2 */
static void x86emuOp_ret_near_IMM(struct x86emu *emu)
{
	u16 imm = fetch_word_imm(emu);

	emu->x86.R_IP = pop_word(emu);
	WRITE_ONCE(emu->x86.R_SP, READ_ONCE(emu->x86.R_SP) + imm);
}

/* Opcode 0xc6 */
static void x86emuOp_mov_byte_RM_IMM(struct x86emu *emu)
{
	u8 *destreg;
	u32 destoffset;
	u8 imm;

	fetch_decode_modrm(emu);
	if (emu->cur_rh != 0)
		x86emu_halt_sys(emu);

	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		imm = fetch_byte_imm(emu);
		store_data_byte(emu, destoffset, imm);
	} else {
		destreg = decode_rl_byte_register(emu);
		imm = fetch_byte_imm(emu);
		*destreg = imm;
	}
}

/* Opcode 0xc7 */
static void x86emuOp32_mov_word_RM_IMM(struct x86emu *emu)
{
	u32 destoffset;
	u32 imm, *destreg;

	fetch_decode_modrm(emu);
	if (emu->cur_rh != 0)
		x86emu_halt_sys(emu);

	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		imm = fetch_long_imm(emu);
		store_data_long(emu, destoffset, imm);
	} else {
		destreg = decode_rl_long_register(emu);
		imm = fetch_long_imm(emu);
		*destreg = imm;
	}
}

static void x86emuOp16_mov_word_RM_IMM(struct x86emu *emu)
{
	u32 destoffset;
	u16 imm, *destreg;

	fetch_decode_modrm(emu);
	if (emu->cur_rh != 0)
		x86emu_halt_sys(emu);

	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		imm = fetch_word_imm(emu);
		store_data_word(emu, destoffset, imm);
	} else {
		destreg = decode_rl_word_register(emu);
		imm = fetch_word_imm(emu);
		*destreg = imm;
	}
}

static void x86emuOp_mov_word_RM_IMM(struct x86emu *emu)
{
	if (MODE_DATA32)
		x86emuOp32_mov_word_RM_IMM(emu);
	else
		x86emuOp16_mov_word_RM_IMM(emu);
}

/* Opcode 0xc8 */
static void x86emuOp_enter(struct x86emu *emu)
{
	u16 local, frame_pointer;
	u8 nesting;
	int i;

	local = fetch_word_imm(emu);
	nesting = fetch_byte_imm(emu) & 0x1f;
	push_word(emu, READ_ONCE(emu->x86.R_BP));
	frame_pointer = READ_ONCE(emu->x86.R_SP);
	if (nesting > 0) {
		for (i = 1; i < nesting; i++) {
			WRITE_ONCE(emu->x86.R_BP, READ_ONCE(emu->x86.R_BP) - 2);
			push_word(emu, fetch_word(emu, emu->x86.R_SS, READ_ONCE(emu->x86.R_BP)));
		}
		push_word(emu, frame_pointer);
	}
	WRITE_ONCE(emu->x86.R_BP, frame_pointer);
	WRITE_ONCE(emu->x86.R_SP, READ_ONCE(emu->x86.R_SP) - local);
}

/* Opcode 0xc9 */
static void x86emuOp_leave(struct x86emu *emu)
{
	WRITE_ONCE(emu->x86.R_SP, emu->x86.R_BP);
	WRITE_ONCE(emu->x86.R_BP, pop_word(emu));
}

/* Opcode 0xca */
static void x86emuOp_ret_far_IMM(struct x86emu *emu)
{
	u16 imm = fetch_word_imm(emu);

	emu->x86.R_IP = pop_word(emu);
	emu->x86.R_CS = pop_word(emu);
	WRITE_ONCE(emu->x86.R_SP, READ_ONCE(emu->x86.R_SP) + imm);
}

/* Opcode 0xcb */
static void x86emuOp_ret_far(struct x86emu *emu)
{
	emu->x86.R_IP = pop_word(emu);
	emu->x86.R_CS = pop_word(emu);
}

/* Opcode 0xcc */
static void x86emuOp_int3(struct x86emu *emu)
{
	x86emu_intr_dispatch(emu, INT3_BP_TRAP);
}

/* Opcode 0xcd */
static void x86emuOp_int_IMM(struct x86emu *emu)
{
	u8 intno = fetch_byte_imm(emu);

	x86emu_intr_dispatch(emu, intno);
}

/* Opcode 0xce */
static void x86emuOp_into(struct x86emu *emu)
{
	if (ACCESS_FLAG(F_OF))
		x86emu_intr_dispatch(emu, INT4_OF_TRAP);
}

/* Opcode 0xcf */
static void x86emuOp_iret(struct x86emu *emu)
{
	x86emu_intr_return(emu);
}

/* Opcode 0xd0 */
static void x86emuOp_opcD0_byte_RM_1(struct x86emu *emu)
{
	u8 destval;

	fetch_decode_modrm(emu);
	destval = decode_and_fetch_byte(emu);
	destval = (*opcD0_byte_operation[emu->cur_rh])(emu, destval, 1);
	write_back_byte(emu, destval);
}

/* Opcode 0xd1 */
static void x86emuOp_opcD1_word_RM_1(struct x86emu *emu)
{
	if (MODE_DATA32) {
		u32 destval;

		fetch_decode_modrm(emu);
		destval = decode_and_fetch_long(emu);
		destval = (*opcD1_long_operation[emu->cur_rh])(emu, destval, 1);
		write_back_long(emu, destval);
	} else {
		u16 destval;

		fetch_decode_modrm(emu);
		destval = decode_and_fetch_word(emu);
		destval = (*opcD1_word_operation[emu->cur_rh])(emu, destval, 1);
		write_back_word(emu, destval);
	}
}

/* Opcode 0xd2 */
static void x86emuOp_opcD2_byte_RM_CL(struct x86emu *emu)
{
	u8 destval;

	fetch_decode_modrm(emu);
	destval = decode_and_fetch_byte(emu);
	destval = (*opcD0_byte_operation[emu->cur_rh])(emu, destval, emu->x86.R_CL);
	write_back_byte(emu, destval);
}

/* Opcode 0xd3 */
static void x86emuOp_opcD3_word_RM_CL(struct x86emu *emu)
{
	if (MODE_DATA32) {
		u32 destval;

		fetch_decode_modrm(emu);
		destval = decode_and_fetch_long(emu);
		destval = (*opcD1_long_operation[emu->cur_rh])(emu, destval, emu->x86.R_CL);
		write_back_long(emu, destval);
	} else {
		u16 destval;

		fetch_decode_modrm(emu);
		destval = decode_and_fetch_word(emu);
		destval = (*opcD1_word_operation[emu->cur_rh])(emu, destval, emu->x86.R_CL);
		write_back_word(emu, destval);
	}
}

/* Opcode 0xd4 */
static void x86emuOp_aam(struct x86emu *emu)
{
	u8 imm = fetch_byte_imm(emu);

	emu->x86.R_AX = aam_word(emu, emu->x86.R_AL, imm);
}

/* Opcode 0xd5 */
static void x86emuOp_aad(struct x86emu *emu)
{
	u8 imm = fetch_byte_imm(emu);

	emu->x86.R_AX = aad_word(emu, emu->x86.R_AX, imm);
}

/* Opcode 0xd6 */
static void x86emuOp_salc(struct x86emu *emu)
{
	emu->x86.R_AL = CF_STATUS ? 0xff : 0x00;
}

/* Opcode 0xd7 */
static void x86emuOp_xlat(struct x86emu *emu)
{
	u16 addr = emu->x86.R_BX + (u16)emu->x86.R_AL;

	emu->x86.R_AL = fetch_data_byte(emu, addr);
}

/* Opcodes 0xd8 - 0xdf */
static void x86emuOp_x87_fpu(struct x86emu *emu, u8 op1)
{
	pr_err("NM: Unsupported x87 FPU Opcode %x\n", op1);
	x86emu_intr_dispatch(emu, INT7_NM_FAULT);
}

/* Opcode 0xe0 */
static void x86emuOp_loopne(struct x86emu *emu)
{
	s16 ip = (s8)fetch_byte_imm(emu);

	ip += (s16)emu->x86.R_IP;
	emu->x86.R_CX -= 1;
	if (emu->x86.R_CX != 0 && !ACCESS_FLAG(F_ZF)) /* CX != 0 and !ZF */
		emu->x86.R_IP = ip;
}

/* Opcode 0xe1 */
static void x86emuOp_loope(struct x86emu *emu)
{
	s16 ip = (s8)fetch_byte_imm(emu);

	ip += (s16)emu->x86.R_IP;
	emu->x86.R_CX -= 1;
	if (emu->x86.R_CX != 0 && ACCESS_FLAG(F_ZF)) /* CX != 0 and ZF */
		emu->x86.R_IP = ip;
}

/* Opcode 0xe2 */
static void x86emuOp_loop(struct x86emu *emu)
{
	s16 ip = (s8)fetch_byte_imm(emu);

	ip += (s16)emu->x86.R_IP;
	emu->x86.R_CX -= 1;
	if (emu->x86.R_CX != 0)
		emu->x86.R_IP = ip;
}

/* Opcode 0xe3 */
static void x86emuOp_jcxz(struct x86emu *emu)
{
	s8 offset = (s8)fetch_byte_imm(emu);
	u16 target = (u16)(emu->x86.R_IP + offset);

	if (emu->x86.R_CX == 0)
		emu->x86.R_IP = target;
}

/* Opcode 0xe4 */
static void x86emuOp_in_byte_AL_IMM(struct x86emu *emu)
{
	u8 port = (u8)fetch_byte_imm(emu);

	emu->x86.R_AL = (*emu->x86_inb)(port);
}

/* Opcode 0xe5 */
static void x86emuOp_in_word_AX_IMM(struct x86emu *emu)
{
	u8 port = (u8)fetch_byte_imm(emu);

	if (MODE_DATA32)
		emu->x86.R_EAX = (*emu->x86_inl)(port);
	else
		emu->x86.R_AX = (*emu->x86_inw)(port);
}

/* Opcode 0xe6 */
static void x86emuOp_out_byte_IMM_AL(struct x86emu *emu)
{
	u8 port = (u8)fetch_byte_imm(emu);

	(*emu->x86_outb)(port, emu->x86.R_AL);
}

/* Opcode 0xe7 */
static void x86emuOp_out_word_IMM_AX(struct x86emu *emu)
{
	u8 port = (u8)fetch_byte_imm(emu);

	if (MODE_DATA32)
		(*emu->x86_outl)(port, emu->x86.R_EAX);
	else
		(*emu->x86_outw)(port, emu->x86.R_AX);
}

/* Opcode 0xe8 */
static void x86emuOp_call_near_IMM(struct x86emu *emu)
{
	if (MODE_DATA32) {
		s32 new_eip;

		new_eip = (s32)fetch_long_imm(emu);
		new_eip += (s32)emu->x86.R_EIP;
		push_long(emu, emu->x86.R_EIP);
		emu->x86.R_EIP = new_eip;
	} else {
		s16 new_ip;

		new_ip = (s16)fetch_word_imm(emu);
		new_ip += (s16)emu->x86.R_IP;
		push_word(emu, emu->x86.R_IP);
		emu->x86.R_IP = new_ip;
	}
}

/* Opcode 0xe9 */
static void x86emuOp_jump_near_IMM(struct x86emu *emu)
{
	s32 ip = (s16)fetch_word_imm(emu);

	ip += (s16)emu->x86.R_IP;
	emu->x86.R_IP = (u16)ip;
}

/* Opcode 0xea */
static void x86emuOp_jump_far_IMM(struct x86emu *emu)
{
	u16 ip = fetch_word_imm(emu);
	u16 cs = fetch_word_imm(emu);

	emu->x86.R_IP = ip;
	emu->x86.R_CS = cs;
}

/* Opcode 0xeb */
static void x86emuOp_jump_byte_IMM(struct x86emu *emu)
{
	s8 offset = (s8)fetch_byte_imm(emu);
	u16 target = (u16)(emu->x86.R_IP + offset);

	emu->x86.R_IP = target;
}

/* Opcode 0xec */
static void x86emuOp_in_byte_AL_DX(struct x86emu *emu)
{
	emu->x86.R_AL = (*emu->x86_inb)(emu->x86.R_DX);
}

/* Opcode 0xed */
static void x86emuOp_in_word_AX_DX(struct x86emu *emu)
{
	if (MODE_DATA32)
		emu->x86.R_EAX = (*emu->x86_inl)(emu->x86.R_DX);
	else
		emu->x86.R_AX = (*emu->x86_inw)(emu->x86.R_DX);
}

/* Opcode 0xee */
static void x86emuOp_out_byte_DX_AL(struct x86emu *emu)
{
	(*emu->x86_outb)(emu->x86.R_DX, emu->x86.R_AL);
}

/* Opcode 0xef */
static void x86emuOp_out_word_DX_AX(struct x86emu *emu)
{
	if (MODE_DATA32)
		(*emu->x86_outl)(emu->x86.R_DX, emu->x86.R_EAX);
	else
		(*emu->x86_outw)(emu->x86.R_DX, emu->x86.R_AX);
}

/* Opcode 0xf1 - INT1 - 386+ */
static void x86emuOp_int1(struct x86emu *emu)
{
	x86emu_intr_dispatch(emu, INT1_DB_TRAP);
}

/* Opcode 0xf5 */
static void x86emuOp_cmc(struct x86emu *emu)
{
	X86EMU_SET_FLAG_COND(!CF_STATUS, F_CF);
}

/* Opcode 0xf6 */
static void x86emuOp_opcF6_byte_RM(struct x86emu *emu)
{
	u8 destval, srcval;

	fetch_decode_modrm(emu);
	if (emu->cur_rh == 1)
		x86emu_halt_sys(emu);

	if (emu->cur_rh == 0) {
		destval = decode_and_fetch_byte_imm8(emu, &srcval);
		test_byte(emu, destval, srcval);
		return;
	}
	destval = decode_and_fetch_byte(emu);

	switch (emu->cur_rh) {
	case 2:
		destval = ~destval;
		write_back_byte(emu, destval);
		break;
	case 3:
		destval = neg_byte(emu, destval);
		write_back_byte(emu, destval);
		break;
	case 4:
		mul_byte(emu, destval);
		break;
	case 5:
		imul_byte(emu, destval);
		break;
	case 6:
		div_byte(emu, destval);
		break;
	case 7:
		idiv_byte(emu, destval);
		break;
	}
}

/* Opcode 0xf7 */
static void x86emuOp32_opcF7_word_RM(struct x86emu *emu)
{
	u32 destval, srcval;

	fetch_decode_modrm(emu);
	if (emu->cur_rh == 1)
		x86emu_halt_sys(emu);

	if (emu->cur_rh == 0) {
		if (emu->cur_mod != 3) {
			u32 destoffset;

			destoffset = decode_rl_address(emu);
			srcval = fetch_long_imm(emu);
			destval = fetch_data_long(emu, destoffset);
		} else {
			srcval = fetch_long_imm(emu);
			destval = *decode_rl_long_register(emu);
		}
		test_long(emu, destval, srcval);
		return;
	}
	destval = decode_and_fetch_long(emu);

	switch (emu->cur_rh) {
	case 2:
		destval = ~destval;
		write_back_long(emu, destval);
		break;
	case 3:
		destval = neg_long(emu, destval);
		write_back_long(emu, destval);
		break;
	case 4:
		mul_long(emu, destval);
		break;
	case 5:
		imul_long(emu, destval);
		break;
	case 6:
		div_long(emu, destval);
		break;
	case 7:
		idiv_long(emu, destval);
		break;
	}
}

static void x86emuOp16_opcF7_word_RM(struct x86emu *emu)
{
	u16 destval, srcval;

	fetch_decode_modrm(emu);
	if (emu->cur_rh == 1)
		x86emu_halt_sys(emu);

	if (emu->cur_rh == 0) {
		if (emu->cur_mod != 3) {
			u32 destoffset;

			destoffset = decode_rl_address(emu);
			srcval = fetch_word_imm(emu);
			destval = fetch_data_word(emu, destoffset);
		} else {
			srcval = fetch_word_imm(emu);
			destval = *decode_rl_word_register(emu);
		}
		test_word(emu, destval, srcval);
		return;
	}
	destval = decode_and_fetch_word(emu);

	switch (emu->cur_rh) {
	case 2:
		destval = ~destval;
		write_back_word(emu, destval);
		break;
	case 3:
		destval = neg_word(emu, destval);
		write_back_word(emu, destval);
		break;
	case 4:
		mul_word(emu, destval);
		break;
	case 5:
		imul_word(emu, destval);
		break;
	case 6:
		div_word(emu, destval);
		break;
	case 7:
		idiv_word(emu, destval);
		break;
	}
}

static void x86emuOp_opcF7_word_RM(struct x86emu *emu)
{
	if (MODE_DATA32)
		x86emuOp32_opcF7_word_RM(emu);
	else
		x86emuOp16_opcF7_word_RM(emu);
}

/* Opcode 0xfe */
static void x86emuOp_opcFE_byte_RM(struct x86emu *emu)
{
	u32 destoffset;
	u8 destval, *destreg;

	/* Yet another special case instruction */
	fetch_decode_modrm(emu);
	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);

		switch (emu->cur_rh) {
		case 0: /* inc word ptr ... */
			destval = fetch_data_byte(emu, destoffset);
			destval = inc_byte(emu, destval);
			store_data_byte(emu, destoffset, destval);
			break;
		case 1: /* dec word ptr ... */
			destval = fetch_data_byte(emu, destoffset);
			destval = dec_byte(emu, destval);
			store_data_byte(emu, destoffset, destval);
			break;
		}
	} else {
		destreg = decode_rl_byte_register(emu);

		switch (emu->cur_rh) {
		case 0:
			*destreg = inc_byte(emu, *destreg);
			break;
		case 1:
			*destreg = dec_byte(emu, *destreg);
			break;
		}
	}
}

/* Opcode 0xff */
static void x86emuOp32_opcFF_word_RM(struct x86emu *emu)
{
	u32 destoffset = 0;
	u32 destval, *destreg;

	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		destval = fetch_data_long(emu, destoffset);

		switch (emu->cur_rh) {
		case 0: /* inc word ptr ... */
			destval = inc_long(emu, destval);
			store_data_long(emu, destoffset, destval);
			break;
		case 1: /* dec word ptr ... */
			destval = dec_long(emu, destval);
			store_data_long(emu, destoffset, destval);
			break;
		case 6: /* push word ptr ... */
			push_long(emu, destval);
			break;
		}
	} else {
		destreg = decode_rl_long_register(emu);

		switch (emu->cur_rh) {
		case 0:
			*destreg = inc_long(emu, *destreg);
			break;
		case 1:
			*destreg = dec_long(emu, *destreg);
			break;
		case 6:
			push_long(emu, *destreg);
			break;
		}
	}
}

static void x86emuOp16_opcFF_word_RM(struct x86emu *emu)
{
	u32 destoffset = 0;
	u16 destval, *destreg;

	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		destval = fetch_data_word(emu, destoffset);

		switch (emu->cur_rh) {
		case 0:
			destval = inc_word(emu, destval);
			store_data_word(emu, destoffset, destval);
			break;
		case 1: /* dec word ptr ... */
			destval = dec_word(emu, destval);
			store_data_word(emu, destoffset, destval);
			break;
		case 6: /* push word ptr ... */
			push_word(emu, destval);
			break;
		}
	} else {
		destreg = decode_rl_word_register(emu);

		switch (emu->cur_rh) {
		case 0:
			*destreg = inc_word(emu, *destreg);
			break;
		case 1:
			*destreg = dec_word(emu, *destreg);
			break;
		case 6:
			push_word(emu, *destreg);
			break;
		}
	}
}

static void x86emuOp_opcFF_word_RM(struct x86emu *emu)
{
	u32 destoffset = 0;
	u16 destval, destval2;

	/* Yet another special case instruction */
	fetch_decode_modrm(emu);
	if ((emu->cur_mod == 3 &&
	    (emu->cur_rh == 3 || emu->cur_rh == 5)) || emu->cur_rh == 7)
		x86emu_halt_sys(emu);

	if (emu->cur_rh == 0 || emu->cur_rh == 1 || emu->cur_rh == 6) {
		if (MODE_DATA32)
			x86emuOp32_opcFF_word_RM(emu);
		else
			x86emuOp16_opcFF_word_RM(emu);
		return;
	}

	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		destval = fetch_data_word(emu, destoffset);

		switch (emu->cur_rh) {
		case 3: /* call far ptr ... */
			destval2 = fetch_data_word(emu, destoffset + 2);
			push_word(emu, emu->x86.R_CS);
			emu->x86.R_CS = destval2;
			push_word(emu, emu->x86.R_IP);
			emu->x86.R_IP = destval;
			break;
		case 5: /* jmp far ptr ... */
			destval2 = fetch_data_word(emu, destoffset + 2);
			emu->x86.R_IP = destval;
			emu->x86.R_CS = destval2;
			break;
		}
	} else {
		destval = *decode_rl_word_register(emu);
	}

	switch (emu->cur_rh) {
	case 2: /* call word ptr */
		push_word(emu, emu->x86.R_IP);
		emu->x86.R_IP = destval;
		break;
	case 4: /* jmp */
		emu->x86.R_IP = destval;
		break;
	}
}

/* Single-Byte Opcode Instructions */
static void x86emu_exec_one_byte(struct x86emu *emu)
{
	u8 op1 = fetch_byte_imm(emu);

	switch (op1) {
	case 0x00:
		common_binop_byte_rm_r(emu, add_byte);
		break;
	case 0x01:
		common_binop_word_long_rm_r(emu, add_word, add_long);
		break;
	case 0x02:
		common_binop_byte_r_rm(emu, add_byte);
		break;
	case 0x03:
		common_binop_word_long_r_rm(emu, add_word, add_long);
		break;
	case 0x04:
		common_binop_byte_imm(emu, add_byte);
		break;
	case 0x05:
		common_binop_word_long_imm(emu, add_word, add_long);
		break;
	case 0x06:
		push_word(emu, emu->x86.R_ES);
		break;
	case 0x07:
		emu->x86.R_ES = pop_word(emu);
		break;
	case 0x08:
		common_binop_byte_rm_r(emu, or_byte);
		break;
	case 0x09:
		common_binop_word_long_rm_r(emu, or_word, or_long);
		break;
	case 0x0a:
		common_binop_byte_r_rm(emu, or_byte);
		break;
	case 0x0b:
		common_binop_word_long_r_rm(emu, or_word, or_long);
		break;
	case 0x0c:
		common_binop_byte_imm(emu, or_byte);
		break;
	case 0x0d:
		common_binop_word_long_imm(emu, or_word, or_long);
		break;
	case 0x0e:
		push_word(emu, emu->x86.R_CS);
		break;
	case 0x0f:
#if X86EMU_CPU == X86EMU_086
		emu->x86.R_CS = pop_word(emu);
#elif X86EMU_CPU > X86EMU_186
		x86emu_exec_two_byte(emu); /* Two-Byte Opcode Instructions */
#endif
		break;
	case 0x10:
		common_binop_byte_rm_r(emu, adc_byte);
		break;
	case 0x11:
		common_binop_word_long_rm_r(emu, adc_word, adc_long);
		break;
	case 0x12:
		common_binop_byte_r_rm(emu, adc_byte);
		break;
	case 0x13:
		common_binop_word_long_r_rm(emu, adc_word, adc_long);
		break;
	case 0x14:
		common_binop_byte_imm(emu, adc_byte);
		break;
	case 0x15:
		common_binop_word_long_imm(emu, adc_word, adc_long);
		break;
	case 0x16:
		push_word(emu, emu->x86.R_SS);
		break;
	case 0x17:
		emu->x86.R_SS = pop_word(emu);
		break;
	case 0x18:
		common_binop_byte_rm_r(emu, sbb_byte);
		break;
	case 0x19:
		common_binop_word_long_rm_r(emu, sbb_word, sbb_long);
		break;
	case 0x1a:
		common_binop_byte_r_rm(emu, sbb_byte);
		break;
	case 0x1b:
		common_binop_word_long_r_rm(emu, sbb_word, sbb_long);
		break;
	case 0x1c:
		common_binop_byte_imm(emu, sbb_byte);
		break;
	case 0x1d:
		common_binop_word_long_imm(emu, sbb_word, sbb_long);
		break;
	case 0x1e:
		push_word(emu, emu->x86.R_DS);
		break;
	case 0x1f:
		emu->x86.R_DS = pop_word(emu);
		break;
	case 0x20:
		common_binop_byte_rm_r(emu, and_byte);
		break;
	case 0x21:
		common_binop_word_long_rm_r(emu, and_word, and_long);
		break;
	case 0x22:
		common_binop_byte_r_rm(emu, and_byte);
		break;
	case 0x23:
		common_binop_word_long_r_rm(emu, and_word, and_long);
		break;
	case 0x24:
		common_binop_byte_imm(emu, and_byte);
		break;
	case 0x25:
		common_binop_word_long_imm(emu, and_word, and_long);
		break;
	case 0x26:
		emu->x86.mode |= SYSMODE_SEGOVR_ES;
		break;
	case 0x27:
		emu->x86.R_AL = daa_byte(emu, emu->x86.R_AL);
		break;
	case 0x28:
		common_binop_byte_rm_r(emu, sub_byte);
		break;
	case 0x29:
		common_binop_word_long_rm_r(emu, sub_word, sub_long);
		break;
	case 0x2a:
		common_binop_byte_r_rm(emu, sub_byte);
		break;
	case 0x2b:
		common_binop_word_long_r_rm(emu, sub_word, sub_long);
		break;
	case 0x2c:
		common_binop_byte_imm(emu, sub_byte);
		break;
	case 0x2d:
		common_binop_word_long_imm(emu, sub_word, sub_long);
		break;
	case 0x2e:
		emu->x86.mode |= SYSMODE_SEGOVR_CS;
		break;
	case 0x2f:
		emu->x86.R_AL = das_byte(emu, emu->x86.R_AL);
		break;
	case 0x30:
		common_binop_byte_rm_r(emu, xor_byte);
		break;
	case 0x31:
		common_binop_word_long_rm_r(emu, xor_word, xor_long);
		break;
	case 0x32:
		common_binop_byte_r_rm(emu, xor_byte);
		break;
	case 0x33:
		common_binop_word_long_r_rm(emu, xor_word, xor_long);
		break;
	case 0x34:
		common_binop_byte_imm(emu, xor_byte);
		break;
	case 0x35:
		common_binop_word_long_imm(emu, xor_word, xor_long);
		break;
	case 0x36:
		emu->x86.mode |= SYSMODE_SEGOVR_SS;
		break;
	case 0x37:
		emu->x86.R_AX = aaa_word(emu, emu->x86.R_AX);
		break;
	case 0x38:
		common_binop_ns_byte_rm_r(emu, cmp_byte);
		break;
	case 0x39:
		common_binop_ns_word_long_rm_r(emu, cmp_word, cmp_long);
		break;
	case 0x3a:
		x86emuOp_cmp_byte_R_RM(emu);
		break;
	case 0x3b:
		x86emuOp_cmp_word_R_RM(emu);
		break;
	case 0x3c:
		x86emuOp_cmp_byte_AL_IMM(emu);
		break;
	case 0x3d:
		x86emuOp_cmp_word_AX_IMM(emu);
		break;
	case 0x3e:
		emu->x86.mode |= SYSMODE_SEGOVR_DS;
		break;
	case 0x3f:
		emu->x86.R_AX = aas_word(emu, emu->x86.R_AX);
		break;
	case 0x40:
		common_inc_word_long(emu, &emu->x86.register_a);
		break;
	case 0x41:
		common_inc_word_long(emu, &emu->x86.register_c);
		break;
	case 0x42:
		common_inc_word_long(emu, &emu->x86.register_d);
		break;
	case 0x43:
		common_inc_word_long(emu, &emu->x86.register_b);
		break;
	case 0x44:
		common_inc_word_long(emu, &emu->x86.register_sp);
		break;
	case 0x45:
		common_inc_word_long(emu, &emu->x86.register_bp);
		break;
	case 0x46:
		common_inc_word_long(emu, &emu->x86.register_si);
		break;
	case 0x47:
		common_inc_word_long(emu, &emu->x86.register_di);
		break;
	case 0x48:
		common_dec_word_long(emu, &emu->x86.register_a);
		break;
	case 0x49:
		common_dec_word_long(emu, &emu->x86.register_c);
		break;
	case 0x4a:
		common_dec_word_long(emu, &emu->x86.register_d);
		break;
	case 0x4b:
		common_dec_word_long(emu, &emu->x86.register_b);
		break;
	case 0x4c:
		common_dec_word_long(emu, &emu->x86.register_sp);
		break;
	case 0x4d:
		common_dec_word_long(emu, &emu->x86.register_bp);
		break;
	case 0x4e:
		common_dec_word_long(emu, &emu->x86.register_si);
		break;
	case 0x4f:
		common_dec_word_long(emu, &emu->x86.register_di);
		break;
	case 0x50:
		common_push_word_long(emu, &emu->x86.register_a);
		break;
	case 0x51:
		common_push_word_long(emu, &emu->x86.register_c);
		break;
	case 0x52:
		common_push_word_long(emu, &emu->x86.register_d);
		break;
	case 0x53:
		common_push_word_long(emu, &emu->x86.register_b);
		break;
	case 0x54:
		common_push_word_long(emu, &emu->x86.register_sp);
		break;
	case 0x55:
		common_push_word_long(emu, &emu->x86.register_bp);
		break;
	case 0x56:
		common_push_word_long(emu, &emu->x86.register_si);
		break;
	case 0x57:
		common_push_word_long(emu, &emu->x86.register_di);
		break;
	case 0x58:
		common_pop_word_long(emu, &emu->x86.register_a);
		break;
	case 0x59:
		common_pop_word_long(emu, &emu->x86.register_c);
		break;
	case 0x5a:
		common_pop_word_long(emu, &emu->x86.register_d);
		break;
	case 0x5b:
		common_pop_word_long(emu, &emu->x86.register_b);
		break;
	case 0x5c:
		common_pop_word_long(emu, &emu->x86.register_sp);
		break;
	case 0x5d:
		common_pop_word_long(emu, &emu->x86.register_bp);
		break;
	case 0x5e:
		common_pop_word_long(emu, &emu->x86.register_si);
		break;
	case 0x5f:
		common_pop_word_long(emu, &emu->x86.register_di);
		break;
#if X86EMU_CPU > X86EMU_086
	case 0x60:
		x86emuOp_push_all(emu);
		break;
	case 0x61:
		x86emuOp_pop_all(emu);
		break;
	case 0x62: /* BOUND */
		break;
#if X86EMU_CPU > X86EMU_286
	case 0x64:
		emu->x86.mode |= SYSMODE_SEGOVR_FS;
		break;
	case 0x65:
		emu->x86.mode |= SYSMODE_SEGOVR_GS;
		break;
	case 0x66:
		emu->x86.mode |= SYSMODE_PREFIX_DATA32;
		break;
	case 0x67:
		emu->x86.mode |= SYSMODE_PREFIX_ADDR32;
		break;
#endif
	case 0x68:
		x86emuOp_push_word_IMM(emu);
		break;
	case 0x69:
		common_imul_imm(emu, false);
		break;
	case 0x6a:
		x86emuOp_push_byte_IMM(emu);
		break;
	case 0x6b:
		common_imul_imm(emu, true);
		break;
	case 0x6c:
		ins(emu, 1);
		break;
	case 0x6d:
		x86emuOp_ins_word(emu);
		break;
	case 0x6e:
		outs(emu, 1);
		break;
	case 0x6f:
		x86emuOp_outs_word(emu);
		break;
#endif
	case 0x70:
		common_jmp_near(emu, ACCESS_FLAG(F_OF));
		break;
	case 0x71:
		common_jmp_near(emu, !ACCESS_FLAG(F_OF));
		break;
	case 0x72:
		common_jmp_near(emu, ACCESS_FLAG(F_CF));
		break;
	case 0x73:
		common_jmp_near(emu, !ACCESS_FLAG(F_CF));
		break;
	case 0x74:
		common_jmp_near(emu, ACCESS_FLAG(F_ZF));
		break;
	case 0x75:
		common_jmp_near(emu, !ACCESS_FLAG(F_ZF));
		break;
	case 0x76:
		common_jmp_near(emu, ACCESS_FLAG(F_CF) || ACCESS_FLAG(F_ZF));
		break;
	case 0x77:
		common_jmp_near(emu, !ACCESS_FLAG(F_CF) && !ACCESS_FLAG(F_ZF));
		break;
	case 0x78:
		common_jmp_near(emu, ACCESS_FLAG(F_SF));
		break;
	case 0x79:
		common_jmp_near(emu, !ACCESS_FLAG(F_SF));
		break;
	case 0x7a:
		common_jmp_near(emu, ACCESS_FLAG(F_PF));
		break;
	case 0x7b:
		common_jmp_near(emu, !ACCESS_FLAG(F_PF));
		break;
	case 0x7c:
		x86emuOp_jump_near_L(emu);
		break;
	case 0x7d:
		x86emuOp_jump_near_NL(emu);
		break;
	case 0x7e:
		x86emuOp_jump_near_LE(emu);
		break;
	case 0x7f:
		x86emuOp_jump_near_NLE(emu);
		break;
	case 0x80:
		x86emuOp_opc80_byte_RM_IMM(emu);
		break;
	case 0x81:
		x86emuOp_opc81_word_RM_IMM(emu);
		break;
	case 0x82:
		x86emuOp_opc82_byte_RM_IMM(emu);
		break;
	case 0x83:
		x86emuOp_opc83_word_RM_IMM(emu);
		break;
	case 0x84:
		common_binop_ns_byte_rm_r(emu, test_byte);
		break;
	case 0x85:
		common_binop_ns_word_long_rm_r(emu, test_word, test_long);
		break;
	case 0x86:
		x86emuOp_xchg_byte_RM_R(emu);
		break;
	case 0x87:
		x86emuOp_xchg_word_RM_R(emu);
		break;
	case 0x88:
		x86emuOp_mov_byte_RM_R(emu);
		break;
	case 0x89:
		x86emuOp_mov_word_RM_R(emu);
		break;
	case 0x8a:
		x86emuOp_mov_byte_R_RM(emu);
		break;
	case 0x8b:
		x86emuOp_mov_word_R_RM(emu);
		break;
	case 0x8c:
		x86emuOp_mov_word_RM_SR(emu);
		break;
	case 0x8d:
		x86emuOp_lea_word_R_M(emu);
		break;
	case 0x8e:
		x86emuOp_mov_word_SR_RM(emu);
		break;
	case 0x8f:
		x86emuOp_pop_RM(emu);
		break;
	case 0x90: /* NOP */
		break;
	case 0x91:
		x86emuOp_xchg_word_AX_CX(emu);
		break;
	case 0x92:
		x86emuOp_xchg_word_AX_DX(emu);
		break;
	case 0x93:
		x86emuOp_xchg_word_AX_BX(emu);
		break;
	case 0x94:
		x86emuOp_xchg_word_AX_SP(emu);
		break;
	case 0x95:
		x86emuOp_xchg_word_AX_BP(emu);
		break;
	case 0x96:
		x86emuOp_xchg_word_AX_SI(emu);
		break;
	case 0x97:
		x86emuOp_xchg_word_AX_DI(emu);
		break;
	case 0x98:
		x86emuOp_cbw(emu);
		break;
	case 0x99:
		x86emuOp_cwd(emu);
		break;
	case 0x9a:
		x86emuOp_call_far_IMM(emu);
		break;
	case 0x9b: /* WAIT */
		break;
	case 0x9c:
		x86emuOp_pushf_word(emu);
		break;
	case 0x9d:
		x86emuOp_popf_word(emu);
		break;
	case 0x9e:
		x86emuOp_sahf(emu);
		break;
	case 0x9f:
		x86emuOp_lahf(emu);
		break;
	case 0xa0:
		x86emuOp_mov_AL_M_IMM(emu);
		break;
	case 0xa1:
		x86emuOp_mov_AX_M_IMM(emu);
		break;
	case 0xa2:
		x86emuOp_mov_M_AL_IMM(emu);
		break;
	case 0xa3:
		x86emuOp_mov_M_AX_IMM(emu);
		break;
	case 0xa4:
		x86emuOp_movs_byte(emu);
		break;
	case 0xa5:
		x86emuOp_movs_word(emu);
		break;
	case 0xa6:
		x86emuOp_cmps_byte(emu);
		break;
	case 0xa7:
		x86emuOp_cmps_word(emu);
		break;
	case 0xa8:
		test_byte(emu, emu->x86.R_AL, fetch_byte_imm(emu));
		break;
	case 0xa9:
		x86emuOp_test_AX_IMM(emu);
		break;
	case 0xaa:
		x86emuOp_stos_byte(emu);
		break;
	case 0xab:
		x86emuOp_stos_word(emu);
		break;
	case 0xac:
		x86emuOp_lods_byte(emu);
		break;
	case 0xad:
		x86emuOp_lods_word(emu);
		break;
	case 0xae:
		x86emuOp_scas_byte(emu);
		break;
	case 0xaf:
		x86emuOp_scas_word(emu);
		break;
	case 0xb0:
		emu->x86.R_AL = fetch_byte_imm(emu);
		break;
	case 0xb1:
		emu->x86.R_CL = fetch_byte_imm(emu);
		break;
	case 0xb2:
		emu->x86.R_DL = fetch_byte_imm(emu);
		break;
	case 0xb3:
		emu->x86.R_BL = fetch_byte_imm(emu);
		break;
	case 0xb4:
		emu->x86.R_AH = fetch_byte_imm(emu);
		break;
	case 0xb5:
		emu->x86.R_CH = fetch_byte_imm(emu);
		break;
	case 0xb6:
		emu->x86.R_DH = fetch_byte_imm(emu);
		break;
	case 0xb7:
		emu->x86.R_BH = fetch_byte_imm(emu);
		break;
	case 0xb8:
		x86emuOp_mov_word_AX_IMM(emu);
		break;
	case 0xb9:
		x86emuOp_mov_word_CX_IMM(emu);
		break;
	case 0xba:
		x86emuOp_mov_word_DX_IMM(emu);
		break;
	case 0xbb:
		x86emuOp_mov_word_BX_IMM(emu);
		break;
	case 0xbc:
		x86emuOp_mov_word_SP_IMM(emu);
		break;
	case 0xbd:
		x86emuOp_mov_word_BP_IMM(emu);
		break;
	case 0xbe:
		x86emuOp_mov_word_SI_IMM(emu);
		break;
	case 0xbf:
		x86emuOp_mov_word_DI_IMM(emu);
		break;
#if X86EMU_CPU > X86EMU_086
	case 0xc0:
		x86emuOp_opcC0_byte_RM_MEM(emu);
		break;
	case 0xc1:
		x86emuOp_opcC1_word_RM_MEM(emu);
		break;
#endif
	case 0xc2:
		x86emuOp_ret_near_IMM(emu);
		break;
	case 0xc3:
		emu->x86.R_IP = pop_word(emu);
		break;
	case 0xc4:
		common_load_far_pointer(emu, &emu->x86.R_ES);
		break;
	case 0xc5:
		common_load_far_pointer(emu, &emu->x86.R_DS);
		break;
	case 0xc6:
		x86emuOp_mov_byte_RM_IMM(emu);
		break;
	case 0xc7:
		x86emuOp_mov_word_RM_IMM(emu);
		break;
#if X86EMU_CPU > X86EMU_086
	case 0xc8:
		x86emuOp_enter(emu);
		break;
	case 0xc9:
		x86emuOp_leave(emu);
		break;
#endif
	case 0xca:
		x86emuOp_ret_far_IMM(emu);
		break;
	case 0xcb:
		x86emuOp_ret_far(emu);
		break;
	case 0xcc:
		x86emuOp_int3(emu);
		break;
	case 0xcd:
		x86emuOp_int_IMM(emu);
		break;
	case 0xce:
		x86emuOp_into(emu);
		break;
	case 0xcf:
		x86emuOp_iret(emu);
		break;
	case 0xd0:
		x86emuOp_opcD0_byte_RM_1(emu);
		break;
	case 0xd1:
		x86emuOp_opcD1_word_RM_1(emu);
		break;
	case 0xd2:
		x86emuOp_opcD2_byte_RM_CL(emu);
		break;
	case 0xd3:
		x86emuOp_opcD3_word_RM_CL(emu);
		break;
	case 0xd4:
		x86emuOp_aam(emu);
		break;
	case 0xd5:
		x86emuOp_aad(emu);
		break;
	case 0xd6:
		x86emuOp_salc(emu);
		break;
	case 0xd7:
		x86emuOp_xlat(emu);
		break;
	case 0xd8 ... 0xdf:
		x86emuOp_x87_fpu(emu, op1);
		break;
	case 0xe0:
		x86emuOp_loopne(emu);
		break;
	case 0xe1:
		x86emuOp_loope(emu);
		break;
	case 0xe2:
		x86emuOp_loop(emu);
		break;
	case 0xe3:
		x86emuOp_jcxz(emu);
		break;
	case 0xe4:
		x86emuOp_in_byte_AL_IMM(emu);
		break;
	case 0xe5:
		x86emuOp_in_word_AX_IMM(emu);
		break;
	case 0xe6:
		x86emuOp_out_byte_IMM_AL(emu);
		break;
	case 0xe7:
		x86emuOp_out_word_IMM_AX(emu);
		break;
	case 0xe8:
		x86emuOp_call_near_IMM(emu);
		break;
	case 0xe9:
		x86emuOp_jump_near_IMM(emu);
		break;
	case 0xea:
		x86emuOp_jump_far_IMM(emu);
		break;
	case 0xeb:
		x86emuOp_jump_byte_IMM(emu);
		break;
	case 0xec:
		x86emuOp_in_byte_AL_DX(emu);
		break;
	case 0xed:
		x86emuOp_in_word_AX_DX(emu);
		break;
	case 0xee:
		x86emuOp_out_byte_DX_AL(emu);
		break;
	case 0xef:
		x86emuOp_out_word_DX_AX(emu);
		break;
	case 0xf0: /* LOCK */
		break;
#if X86EMU_CPU == X86EMU_286
	case 0xf1:
		break;
#elif X86EMU_CPU > X86EMU_286
	case 0xf1:
		x86emuOp_int1(emu);
		break;
#endif
	case 0xf2:
		SET_MODE_REPNE;
		break;
	case 0xf3:
		SET_MODE_REPE;
		break;
	case 0xf4:
		x86emu_halt_sys(emu);
		break;
	case 0xf5:
		x86emuOp_cmc(emu);
		break;
	case 0xf6:
		x86emuOp_opcF6_byte_RM(emu);
		break;
	case 0xf7:
		x86emuOp_opcF7_word_RM(emu);
		break;
	case 0xf8:
		CLEAR_FLAG(F_CF);
		break;
	case 0xf9:
		SET_FLAG(F_CF);
		break;
	case 0xfa:
		CLEAR_FLAG(F_IF);
		break;
	case 0xfb:
		SET_FLAG(F_IF);
		break;
	case 0xfc:
		CLEAR_FLAG(F_DF);
		break;
	case 0xfd:
		SET_FLAG(F_DF);
		break;
	case 0xfe:
		x86emuOp_opcFE_byte_RM(emu);
		break;
	case 0xff:
		x86emuOp_opcFF_word_RM(emu);
		break;
	default:
		pr_err("UD: Undefined Opcode %x\n", op1);
#if X86EMU_CPU > X86EMU_086
		x86emu_intr_dispatch(emu, INT6_UD_FAULT);
#endif
		break;
	}
#if X86EMU_CPU > X86EMU_286
	if (op1 != 0x26 && op1 != 0x2e && op1 != 0x36 && op1 != 0x3e && (op1 | 3) != 0x67)
#else
	if (op1 != 0x26 && op1 != 0x2e && op1 != 0x36 && op1 != 0x3e)
#endif
		CLEAR_MODE;
}

static void common_jmp_long(struct x86emu *emu, bool cond)
{
	s16 target = (s16)fetch_word_imm(emu);

	target += (s16)emu->x86.R_IP;
	if (cond)
		emu->x86.R_IP = (u16)target;
}

static void common_set_byte(struct x86emu *emu, bool cond)
{
	u32 destoffset;
	u8 destval, *destreg;

	fetch_decode_modrm(emu);
	destval = cond ? 1 : 0;
	if (emu->cur_mod != 3) {
		destoffset = decode_rl_address(emu);
		store_data_byte(emu, destoffset, destval);
	} else {
		destreg = decode_rl_byte_register(emu);
		*destreg = destval;
	}
}

static void common_bitstring32(struct x86emu *emu, u8 op)
{
	u32 srcval, *shiftreg, bit, mask;

	fetch_decode_modrm(emu);
	shiftreg = decode_rh_long_register(emu);
	srcval = decode_and_fetch_long_disp(emu, (s16)*shiftreg >> 5);
	bit = *shiftreg & 0x1f;
	mask = (1 << bit);
	X86EMU_SET_FLAG_COND(CF_SET(srcval & mask), F_CF);

	switch (op) {
	case 0:
		break;
	case 1:
		write_back_long(emu, srcval | mask);
		break;
	case 2:
		write_back_long(emu, srcval & ~mask);
		break;
	case 3:
		write_back_long(emu, srcval ^ mask);
		break;
	}
}

static void common_bitstring16(struct x86emu *emu, u8 op)
{
	u16 srcval, *shiftreg, bit, mask;

	fetch_decode_modrm(emu);
	shiftreg = decode_rh_word_register(emu);
	srcval = decode_and_fetch_word_disp(emu, (s16)*shiftreg >> 4);
	bit = *shiftreg & 0xf;
	mask = (1 << bit);
	X86EMU_SET_FLAG_COND(CF_SET(srcval & mask), F_CF);

	switch (op) {
	case 0:
		break;
	case 1:
		write_back_word(emu, srcval | mask);
		break;
	case 2:
		write_back_word(emu, srcval & ~mask);
		break;
	case 3:
		write_back_word(emu, srcval ^ mask);
		break;
	}
}

static void common_bitstring(struct x86emu *emu, u8 op)
{
	if (MODE_DATA32)
		common_bitstring32(emu, op);
	else
		common_bitstring16(emu, op);
}

static void common_bitsearch32(struct x86emu *emu, s32 diff)
{
	u32 srcval, *dstreg;

	fetch_decode_modrm(emu);
	dstreg = decode_rh_long_register(emu);
	srcval = decode_and_fetch_long(emu);
	X86EMU_SET_FLAG_COND(ZF_SET(srcval), F_ZF);
	for (*dstreg = 0; *dstreg < 32; *dstreg += diff) {
		if ((srcval >> *dstreg) & 1)
			break;
	}
}

static void common_bitsearch16(struct x86emu *emu, s32 diff)
{
	u16 srcval, *dstreg;

	fetch_decode_modrm(emu);
	dstreg = decode_rh_word_register(emu);
	srcval = decode_and_fetch_word(emu);
	X86EMU_SET_FLAG_COND(ZF_SET(srcval), F_ZF);
	for (*dstreg = 0; *dstreg < 16; *dstreg += diff) {
		if ((srcval >> *dstreg) & 1)
			break;
	}
}

static void common_bitsearch(struct x86emu *emu, s32 diff)
{
	if (MODE_DATA32)
		common_bitsearch32(emu, diff);
	else
		common_bitsearch16(emu, diff);
}

static void common_shift32(struct x86emu *emu, bool shift_left, bool use_cl)
{
	u32 destval, *shiftreg;
	u8 shift;

	fetch_decode_modrm(emu);
	shiftreg = decode_rh_long_register(emu);
	if (use_cl) {
		destval = decode_and_fetch_long(emu);
		shift = emu->x86.R_CL;
	} else {
		destval = decode_and_fetch_long_imm8(emu, &shift);
	}
	if (shift_left)
		destval = shld_long(emu, destval, *shiftreg, shift);
	else
		destval = shrd_long(emu, destval, *shiftreg, shift);
	write_back_long(emu, destval);
}

static void common_shift16(struct x86emu *emu, bool shift_left, bool use_cl)
{
	u8 shift;
	u16 destval, *shiftreg;

	fetch_decode_modrm(emu);
	shiftreg = decode_rh_word_register(emu);
	if (use_cl) {
		destval = decode_and_fetch_word(emu);
		shift = emu->x86.R_CL;
	} else {
		destval = decode_and_fetch_word_imm8(emu, &shift);
	}
	if (shift_left)
		destval = shld_word(emu, destval, *shiftreg, shift);
	else
		destval = shrd_word(emu, destval, *shiftreg, shift);
	write_back_word(emu, destval);
}

static void common_shift(struct x86emu *emu, bool shift_left, bool use_cl)
{
	if (MODE_DATA32)
		common_shift32(emu, shift_left, use_cl);
	else
		common_shift16(emu, shift_left, use_cl);
}

/*
 * Documentation:
 *
 * Programming the 80386 - John H. Crawford, Patrick P. Gelsinger
 *
 * Intel 64 and IA-32 Architectures Software Developer's Manual,
 * Combined volumes: 1, 2A, 2B, 2C, 2D, 3A, 3B, 3C, 3D and 4 - March 2024
 */

#define X86EMU_XORL(a, b)			(((a) && !(b)) || (!(a) && (b)))

/* Opcode 0x0f31 - RDTSC - 586+ */
static void x86emuOp2_rdtsc(struct x86emu *emu)
{
	u64 tsc = READ_ONCE(emu->tsc);

	emu->x86.R_EAX = (u32)(tsc & 0xffffffff);
	emu->x86.R_EDX = (u32)(tsc >> 32);
}

/* Opcode 0x0fa0 */
static void x86emuOp2_push_FS(struct x86emu *emu)
{
	push_word(emu, emu->x86.R_FS);
}

/* Opcode 0x0fa1 */
static void x86emuOp2_pop_FS(struct x86emu *emu)
{
	emu->x86.R_FS = pop_word(emu);
}

#if defined(__i386__) || defined(__amd64__)
static void x86emu_native_cpuid(u32 *eax, u32 *ebx, u32 *ecx, u32 *edx)
{
	asm volatile("cpuid"
	    : "=a" (*eax), "=b" (*ebx), "=c" (*ecx), "=d" (*edx)
	    : "0" (*eax), "2" (*ecx)
	    : "memory");
}
#endif

/* Opcode 0x0fa2 - CPUID - 486+ */
static void x86emuOp2_cpuid(struct x86emu *emu)
{
	u32 op = emu->x86.R_EAX;
	u32 level = 1;

	if (op > level) {
		emu->x86.R_EAX = 0x00000000;
		emu->x86.R_EBX = 0x00000000;
		emu->x86.R_ECX = 0x00000000;
		emu->x86.R_EDX = 0x00000000;
		return;
	}

#if defined(__i386__) || defined(__amd64__)
	emu->x86.R_ECX = 0x00000000;
	x86emu_native_cpuid(&emu->x86.R_EAX, &emu->x86.R_EBX, &emu->x86.R_ECX, &emu->x86.R_EDX);
#endif

	switch (op) {
	case 0:
		emu->x86.R_EAX = level;
#if !defined(__i386__) && !defined(__amd64__)
		/* "GenuineIntel" */
		emu->x86.R_EBX = 0x756e6547;
		emu->x86.R_ECX = 0x6c65746e;
		emu->x86.R_EDX = 0x49656e69;
#endif
		break;
	case 1:
#if !defined(__i386__) && !defined(__amd64__)
		emu->x86.R_EAX = 0x00000480;
		emu->x86.R_EBX = 0x00000000;
		emu->x86.R_ECX = 0x00000000;
#endif
		emu->x86.R_EDX = 0x00000010;
		break;
	}
}

/* Opcode 0x0fa3 */
static void x86emuOp2_bt_R(struct x86emu *emu)
{
	common_bitstring(emu, 0);
}

/* Opcode 0x0fa4 */
static void x86emuOp2_shld_IMM(struct x86emu *emu)
{
	common_shift(emu, true, false);
}

/* Opcode 0x0fa5 */
static void x86emuOp2_shld_CL(struct x86emu *emu)
{
	common_shift(emu, true, true);
}

/* Opcode 0x0fa8 */
static void x86emuOp2_push_GS(struct x86emu *emu)
{
	push_word(emu, emu->x86.R_GS);
}

/* Opcode 0x0fa9 */
static void x86emuOp2_pop_GS(struct x86emu *emu)
{
	emu->x86.R_GS = pop_word(emu);
}

/* Opcode 0x0fab */
static void x86emuOp2_bts_R(struct x86emu *emu)
{
	common_bitstring(emu, 1);
}

/* Opcode 0x0fac */
static void x86emuOp2_shrd_IMM(struct x86emu *emu)
{
	common_shift(emu, false, false);
}

/* Opcode 0x0fad */
static void x86emuOp2_shrd_CL(struct x86emu *emu)
{
	common_shift(emu, false, true);
}

/* Opcode 0x0faf */
static void x86emuOp2_32_imul_R_RM(struct x86emu *emu)
{
	u32 *destreg, srcval, res, res_hi;
	s64 res64;

	fetch_decode_modrm(emu);
	destreg = decode_rh_long_register(emu);
	srcval = decode_and_fetch_long(emu);
	res64 = (s64)(s32)*destreg * (s32)srcval;
	res = (u32)((u64)res64 & 0xffffffff);
	res_hi = (u32)((u64)res64 >> 32);
	x86emu_set_flags(emu, CF_SET(res_hi), LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res64), MSB_LONG(res), OF_SET(res_hi), OF_BOOL);
	*destreg = res;
}

static void x86emuOp2_16_imul_R_RM(struct x86emu *emu)
{
	u16 *destreg, srcval, res, res_hi;
	s32 res32;

	fetch_decode_modrm(emu);
	destreg = decode_rh_word_register(emu);
	srcval = decode_and_fetch_word(emu);
	res32 = (s32)(s16)*destreg * (s16)srcval;
	res = (u16)((u32)res32 & 0xffff);
	res_hi = (u16)((u32)res32 >> 16);
	x86emu_set_flags(emu, CF_SET(res_hi), LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res32), MSB_WORD(res), OF_SET(res_hi), OF_BOOL);
	*destreg = res;
}

static void x86emuOp2_imul_R_RM(struct x86emu *emu)
{
	if (MODE_DATA32)
		x86emuOp2_32_imul_R_RM(emu);
	else
		x86emuOp2_16_imul_R_RM(emu);
}

/* Opcode 0x0fb2 */
static void x86emuOp2_lss_R_IMM(struct x86emu *emu)
{
	common_load_far_pointer(emu, &emu->x86.R_SS);
}

/* Opcode 0x0fb3 */
static void x86emuOp2_btr_R(struct x86emu *emu)
{
	common_bitstring(emu, 2);
}

/* Opcode 0x0fb4 */
static void x86emuOp2_lfs_R_IMM(struct x86emu *emu)
{
	common_load_far_pointer(emu, &emu->x86.R_FS);
}

/* Opcode 0x0fb5 */
static void x86emuOp2_lgs_R_IMM(struct x86emu *emu)
{
	common_load_far_pointer(emu, &emu->x86.R_GS);
}

/* Opcode 0x0fb6 */
static void x86emuOp2_32_movzx_byte_R_RM(struct x86emu *emu)
{
	u32 *destreg;

	fetch_decode_modrm(emu);
	destreg = decode_rh_long_register(emu);
	*destreg = decode_and_fetch_byte(emu);
}

static void x86emuOp2_16_movzx_byte_R_RM(struct x86emu *emu)
{
	u16 *destreg;

	fetch_decode_modrm(emu);
	destreg = decode_rh_word_register(emu);
	*destreg = decode_and_fetch_byte(emu);
}

static void x86emuOp2_movzx_byte_R_RM(struct x86emu *emu)
{
	if (MODE_DATA32)
		x86emuOp2_32_movzx_byte_R_RM(emu);
	else
		x86emuOp2_16_movzx_byte_R_RM(emu);
}

/* Opcode 0x0fb7 */
static void x86emuOp2_movzx_word_R_RM(struct x86emu *emu)
{
	u32 *destreg;

	fetch_decode_modrm(emu);
	destreg = decode_rh_long_register(emu);
	*destreg = decode_and_fetch_word(emu);
}

/* Opcode 0x0fba */
static void x86emuOp2_32_btX_I(struct x86emu *emu)
{
	u32 srcval, bit, mask;
	u8 shift;

	fetch_decode_modrm(emu);
	if (emu->cur_rh < 4)
		x86emu_halt_sys(emu);

	srcval = decode_and_fetch_long_imm8(emu, &shift);
	bit = shift & 0x1f;
	mask = (1 << bit);

	switch (emu->cur_rh) {
	case 5:
		write_back_long(emu, srcval | mask);
		break;
	case 6:
		write_back_long(emu, srcval & ~mask);
		break;
	case 7:
		write_back_long(emu, srcval ^ mask);
		break;
	}
	X86EMU_SET_FLAG_COND(CF_SET(srcval & mask), F_CF);
}

static void x86emuOp2_16_btX_I(struct x86emu *emu)
{
	u16 srcval, bit, mask;
	u8 shift;

	fetch_decode_modrm(emu);
	if (emu->cur_rh < 4)
		x86emu_halt_sys(emu);

	srcval = decode_and_fetch_word_imm8(emu, &shift);
	bit = shift & 0xf;
	mask = (1 << bit);

	switch (emu->cur_rh) {
	case 5:
		write_back_word(emu, srcval | mask);
		break;
	case 6:
		write_back_word(emu, srcval & ~mask);
		break;
	case 7:
		write_back_word(emu, srcval ^ mask);
		break;
	}
	X86EMU_SET_FLAG_COND(CF_SET(srcval & mask), F_CF);
}

static void x86emuOp2_btX_I(struct x86emu *emu)
{
	if (MODE_DATA32)
		x86emuOp2_32_btX_I(emu);
	else
		x86emuOp2_16_btX_I(emu);
}

/* Opcode 0x0fbb */
static void x86emuOp2_btc_R(struct x86emu *emu)
{
	common_bitstring(emu, 3);
}

/* BSF - Bit Scan Forward */
static void x86emuOp2_bsf(struct x86emu *emu)
{
	common_bitsearch(emu, 1);
}

/* BSR - Bit Scan Reverse */
static void x86emuOp2_bsr(struct x86emu *emu)
{
	common_bitsearch(emu, -1);
}

/* Opcode 0x0fbe */
static void x86emuOp2_32_movsx_byte_R_RM(struct x86emu *emu)
{
	u32 *destreg;

	fetch_decode_modrm(emu);
	destreg = decode_rh_long_register(emu);
	*destreg = (s32)(s8)decode_and_fetch_byte(emu);
}

static void x86emuOp2_16_movsx_byte_R_RM(struct x86emu *emu)
{
	u16 *destreg;

	fetch_decode_modrm(emu);
	destreg = decode_rh_word_register(emu);
	*destreg = (s16)(s8)decode_and_fetch_byte(emu);
}

static void x86emuOp2_movsx_byte_R_RM(struct x86emu *emu)
{
	if (MODE_DATA32)
		x86emuOp2_32_movsx_byte_R_RM(emu);
	else
		x86emuOp2_16_movsx_byte_R_RM(emu);
}

/* Opcode 0x0fbf */
static void x86emuOp2_movsx_word_R_RM(struct x86emu *emu)
{
	u32 *destreg;

	fetch_decode_modrm(emu);
	destreg = decode_rh_long_register(emu);
	*destreg = (s32)(s16)decode_and_fetch_word(emu);
}

/* Opcodes 0x0fc8 - 0x0fcf - BSWAP - 486+ */
static void x86emuOp2_32_bswap(u32 *reg)
{
	u32 old_reg = *reg;
	u32 new_reg = ((old_reg & 0xff000000) >> 24) | ((old_reg & 0x00ff0000) >> 8) |
		      ((old_reg & 0x0000ff00) << 8) | ((old_reg & 0x000000ff) << 24);

	*reg = new_reg;
}

/* Two-Byte Opcode Instructions */
static void x86emu_exec_two_byte(struct x86emu *emu)
{
	u8 op2 = fetch_byte_imm(emu);

	switch (op2) {
	case 0x0b:
		x86emu_intr_dispatch(emu, INT6_UD_FAULT); /* #UD2 */
		break;
	/* 0x0f30 - WRMSR */
	case 0x31:
		x86emuOp2_rdtsc(emu);
		break;
	/* 0x0f32 - RDMSR */
	/* 0x0f33 - RDPMC */
	case 0x38:
	case 0x3a:
		x86emu_exec_three_byte(emu, op2); /* Three-Byte Opcode Instructions */
		break;
	/* 0x0f4x - CMOV */
	case 0x80:
		common_jmp_long(emu, ACCESS_FLAG(F_OF));
		break;
	case 0x81:
		common_jmp_long(emu, !ACCESS_FLAG(F_OF));
		break;
	case 0x82:
		common_jmp_long(emu, ACCESS_FLAG(F_CF));
		break;
	case 0x83:
		common_jmp_long(emu, !ACCESS_FLAG(F_CF));
		break;
	case 0x84:
		common_jmp_long(emu, ACCESS_FLAG(F_ZF));
		break;
	case 0x85:
		common_jmp_long(emu, !ACCESS_FLAG(F_ZF));
		break;
	case 0x86:
		common_jmp_long(emu, ACCESS_FLAG(F_CF) || ACCESS_FLAG(F_ZF));
		break;
	case 0x87:
		common_jmp_long(emu, !(ACCESS_FLAG(F_CF) || ACCESS_FLAG(F_ZF)));
		break;
	case 0x88:
		common_jmp_long(emu, ACCESS_FLAG(F_SF));
		break;
	case 0x89:
		common_jmp_long(emu, !ACCESS_FLAG(F_SF));
		break;
	case 0x8a:
		common_jmp_long(emu, ACCESS_FLAG(F_PF));
		break;
	case 0x8b:
		common_jmp_long(emu, !ACCESS_FLAG(F_PF));
		break;
	case 0x8c:
		common_jmp_long(emu, (X86EMU_XORL(ACCESS_FLAG(F_SF), ACCESS_FLAG(F_OF))));
		break;
	case 0x8d:
		common_jmp_long(emu, !(X86EMU_XORL(ACCESS_FLAG(F_SF), ACCESS_FLAG(F_OF))));
		break;
	case 0x8e:
		common_jmp_long(emu, (X86EMU_XORL(ACCESS_FLAG(F_SF), ACCESS_FLAG(F_OF)) ||
				      ACCESS_FLAG(F_ZF)));
		break;
	case 0x8f:
		common_jmp_long(emu, !(X86EMU_XORL(ACCESS_FLAG(F_SF), ACCESS_FLAG(F_OF)) ||
				       ACCESS_FLAG(F_ZF)));
		break;
	case 0x90:
		common_set_byte(emu, ACCESS_FLAG(F_OF));
		break;
	case 0x91:
		common_set_byte(emu, !ACCESS_FLAG(F_OF));
		break;
	case 0x92:
		common_set_byte(emu, ACCESS_FLAG(F_CF));
		break;
	case 0x93:
		common_set_byte(emu, !ACCESS_FLAG(F_CF));
		break;
	case 0x94:
		common_set_byte(emu, ACCESS_FLAG(F_ZF));
		break;
	case 0x95:
		common_set_byte(emu, !ACCESS_FLAG(F_ZF));
		break;
	case 0x96:
		common_set_byte(emu, ACCESS_FLAG(F_CF) || ACCESS_FLAG(F_ZF));
		break;
	case 0x97:
		common_set_byte(emu, !(ACCESS_FLAG(F_CF) || ACCESS_FLAG(F_ZF)));
		break;
	case 0x98:
		common_set_byte(emu, ACCESS_FLAG(F_SF));
		break;
	case 0x99:
		common_set_byte(emu, !ACCESS_FLAG(F_SF));
		break;
	case 0x9a:
		common_set_byte(emu, ACCESS_FLAG(F_PF));
		break;
	case 0x9b:
		common_set_byte(emu, !ACCESS_FLAG(F_PF));
		break;
	case 0x9c:
		common_set_byte(emu, (X86EMU_XORL(ACCESS_FLAG(F_SF), ACCESS_FLAG(F_OF))));
		break;
	case 0x9d:
		common_set_byte(emu, !(X86EMU_XORL(ACCESS_FLAG(F_SF), ACCESS_FLAG(F_OF))));
		break;
	case 0x9e:
		common_set_byte(emu, (X86EMU_XORL(ACCESS_FLAG(F_SF), ACCESS_FLAG(F_OF)) ||
				      ACCESS_FLAG(F_ZF)));
		break;
	case 0x9f:
		common_set_byte(emu, !(X86EMU_XORL(ACCESS_FLAG(F_SF), ACCESS_FLAG(F_OF)) ||
				       ACCESS_FLAG(F_ZF)));
		break;
	case 0xa0:
		x86emuOp2_push_FS(emu);
		break;
	case 0xa1:
		x86emuOp2_pop_FS(emu);
		break;
	case 0xa2:
		x86emuOp2_cpuid(emu);
		break;
	case 0xa3:
		x86emuOp2_bt_R(emu);
		break;
	case 0xa4:
		x86emuOp2_shld_IMM(emu);
		break;
	case 0xa5:
		x86emuOp2_shld_CL(emu);
		break;
	case 0xa8:
		x86emuOp2_push_GS(emu);
		break;
	case 0xa9:
		x86emuOp2_pop_GS(emu);
		break;
	case 0xab:
		x86emuOp2_bts_R(emu);
		break;
	case 0xac:
		x86emuOp2_shrd_IMM(emu);
		break;
	case 0xad:
		x86emuOp2_shrd_CL(emu);
		break;
	case 0xaf:
		x86emuOp2_imul_R_RM(emu);
		break;
	/* 0x0fb0, 0x0fb1 - CMPXCHG */
	case 0xb2:
		x86emuOp2_lss_R_IMM(emu);
		break;
	case 0xb3:
		x86emuOp2_btr_R(emu);
		break;
	case 0xb4:
		x86emuOp2_lfs_R_IMM(emu);
		break;
	case 0xb5:
		x86emuOp2_lgs_R_IMM(emu);
		break;
	case 0xb6:
		x86emuOp2_movzx_byte_R_RM(emu);
		break;
	case 0xb7:
		x86emuOp2_movzx_word_R_RM(emu);
		break;
	case 0xb9:
		x86emu_intr_dispatch(emu, INT6_UD_FAULT); /* #UD1 */
		break;
	case 0xba:
		x86emuOp2_btX_I(emu);
		break;
	case 0xbb:
		x86emuOp2_btc_R(emu);
		break;
	case 0xbc:
		x86emuOp2_bsf(emu);
		break;
	case 0xbd:
		x86emuOp2_bsr(emu);
		break;
	case 0xbe:
		x86emuOp2_movsx_byte_R_RM(emu);
		break;
	case 0xbf:
		x86emuOp2_movsx_word_R_RM(emu);
		break;
	/* 0x0fc0, 0x0fc1 - XADD */
	case 0xc8:
		x86emuOp2_32_bswap(&emu->x86.R_EAX);
		break;
	case 0xc9:
		x86emuOp2_32_bswap(&emu->x86.R_ECX);
		break;
	case 0xca:
		x86emuOp2_32_bswap(&emu->x86.R_EDX);
		break;
	case 0xcb:
		x86emuOp2_32_bswap(&emu->x86.R_EBX);
		break;
	case 0xcc:
		x86emuOp2_32_bswap(&emu->x86.R_ESP);
		break;
	case 0xcd:
		x86emuOp2_32_bswap(&emu->x86.R_EBP);
		break;
	case 0xce:
		x86emuOp2_32_bswap(&emu->x86.R_ESI);
		break;
	case 0xcf:
		x86emuOp2_32_bswap(&emu->x86.R_EDI);
		break;
	case 0xff:
		x86emu_intr_dispatch(emu, INT6_UD_FAULT); /* #UD0 */
		break;
	default:
		pr_err("UD: Undefined Opcode 0f%02x\n", op2);
		x86emu_intr_dispatch(emu, INT6_UD_FAULT);
		break;
	}
}

/*
 * Carry Chain Calculation
 *
 * This represents a somewhat expensive calculation which is
 * apparently required to emulate the setting of the OF and AF flag.
 * The latter is not so important, but the former is. The overflow
 * flag is the XOR of the top two bits of the carry chain for an
 * addition (similar for subtraction). Since we do not want to
 * simulate the addition in a bitwise manner, we try to calculate the
 * carry chain given the two operands and the result.
 *
 * So, given the following table, which represents the addition of two
 * bits, we can derive a formula for the carry chain.
 *
 * a   b   cin   r     cout
 * 0   0   0     0     0
 * 0   0   1     1     0
 * 0   1   0     1     0
 * 0   1   1     0     1
 * 1   0   0     1     0
 * 1   0   1     0     1
 * 1   1   0     0     1
 * 1   1   1     1     1
 *
 * Construction of table for cout:
 *
 * ab
 * r  \  00   01   11  10
 * |------------------
 * 0  |   0    1    1   1
 * 1  |   0    0    1   0
 *
 * By inspection, one gets: cc = ab + r'(a + b)
 *
 * That represents a lot of operations, but NO CHOICE....
 *
 * Borrow Chain Calculation.
 *
 * The following table represents the subtraction of two bits, from
 * which we can derive a formula for the borrow chain.
 *
 * a   b   bin   r     bout
 * 0   0   0     0     0
 * 0   0   1     1     1
 * 0   1   0     1     1
 * 0   1   1     0     1
 * 1   0   0     1     0
 * 1   0   1     0     0
 * 1   1   0     0     0
 * 1   1   1     1     1
 *
 * Construction of table for cout:
 *
 * ab
 * r  \  00   01   11  10
 * |------------------
 * 0  |   0    1    0   0
 * 1  |   1    1    1   0
 *
 * By inspection, one gets: bc = a'b + r(a' + b)
 *
 */

static const u32 x86emu_parity_tab[8] = {
	0x96696996,
	0x69969669,
	0x69969669,
	0x96696996,
	0x69969669,
	0x96696996,
	0x96696996,
	0x69969669,
};

#define X86EMU_PARITY(x)		(((x86emu_parity_tab[(x) / 32] >> ((x) % 32)) & 1) == 0)
#define X86EMU_XOR2(x)			(((x) ^ ((x) >> 1)) & 1)

#if X86EMU_CPU < X86EMU_186
#define X86EMU_SHIFT_MASK(count)	((count) & 0xff) /* 8086 */
#else
#define X86EMU_SHIFT_MASK(count)	((count) & 0x1f) /* 80186+ */
#endif

static __always_inline void x86emu_set_flags(struct x86emu *emu, bool cf, u8 pf, bool af,
					     bool zf, bool sf, u8 of, bool of_bool)
{
	X86EMU_SET_FLAG_COND(cf, F_CF);
	X86EMU_SET_FLAG_COND(X86EMU_PARITY(pf), F_PF);
	X86EMU_SET_FLAG_COND(af, F_AF);
	X86EMU_SET_FLAG_COND(zf, F_ZF);
	X86EMU_SET_FLAG_COND(sf, F_SF);
	if (of_bool)
		X86EMU_SET_FLAG_COND(OF_SET(of), F_OF);
	else
		X86EMU_SET_FLAG_COND(X86EMU_XOR2(of), F_OF);
}

/* AAA - ASCII Adjust after Addition */
static u16 aaa_word(struct x86emu *emu, u16 op1)
{
	u16 ax = op1;
	u8 al;
	bool af, cf;

	if ((op1 & 0xf) > 0x9 || ACCESS_FLAG(F_AF)) {
		ax += 0x106;
		af = cf = true;
	} else {
		af = cf = false;
	}
	ax = ax & 0xff0f;
	al = LOW_BYTE(ax);
	x86emu_set_flags(emu, cf, al, af,
			 ZF_SET(al), MSB_BYTE(al), OF_CLEAR, OF_BOOL);
	return ax;
}

/* AAD - ASCII Adjust before Division */
static u16 aad_word(struct x86emu *emu, u16 op1, u8 op2)
{
	u8 al = LOW_BYTE(op1);
	u8 ah = HIGH_BYTE(op1);
	u16 ax = (u16)al + ((u16)ah * op2);

	al = LOW_BYTE(ax);
	ax = al;
	x86emu_set_flags(emu, CF_CLEAR, al, AF_CLEAR,
			 ZF_SET(al), MSB_BYTE(al), OF_CLEAR, OF_BOOL);
	return ax;
}

/* AAM - ASCII Adjust after Multiplication */
static u16 aam_word(struct x86emu *emu, u8 op1, u8 op2)
{
	u8 al = op1;
	u8 ah = ~op2;
	u16 ax;

	if (op2 == 0) {
		x86emu_intr_raise(emu, INT0_DE_FAULT);
	} else {
		al = op1 % op2;
		ah = op1 / op2;
	}
	ax = ((u16)ah << 8) | al;
	x86emu_set_flags(emu, CF_CLEAR, al, AF_CLEAR,
			 ZF_SET(al), MSB_BYTE(al), OF_CLEAR, OF_BOOL);
	return ax;
}

/* AAS - ASCII Adjust after Subtraction */
static u16 aas_word(struct x86emu *emu, u16 op1)
{
	u16 ax = op1;
	u8 al;
	bool af, cf;

	if ((op1 & 0xf) > 0x9 || ACCESS_FLAG(F_AF)) {
		ax -= 0x106;
		af = cf = true;
	} else {
		af = cf = false;
	}
	ax = ax & 0xff0f;
	al = LOW_BYTE(ax);
	x86emu_set_flags(emu, cf, al, af,
			 ZF_SET(al), MSB_BYTE(al), OF_CLEAR, OF_BOOL);
	return ax;
}

/* ADC - Add with Carry - 8-bit */
static u8 adc_byte(struct x86emu *emu, u8 op1, u8 op2)
{
	u16 res16 = (u16)op1 + (u16)op2 + CF_STATUS;
	u8 cc, res = LOW_BYTE(res16);

	cc = (op2 & op1) | ((~res16) & (op2 | op1)); /* Carry chain */
	x86emu_set_flags(emu, CFA_BYTE(res16), res, MSB_NIBBLE(cc),
			 ZF_SET(res), MSB_BYTE(res), MSB2_BYTE(cc), OF_MSB2);
	return res;
}

/* ADC - Add with Carry - 16-bit */
static u16 adc_word(struct x86emu *emu, u16 op1, u16 op2)
{
	u32 res32 = (u32)op1 + (u32)op2 + CF_STATUS;
	u16 cc, res = LOW_WORD(res32);

	cc = (op2 & op1) | ((~res32) & (op2 | op1));
	x86emu_set_flags(emu, CFA_WORD(res32), LOW_BYTE(res), MSB_NIBBLE(cc),
			 ZF_SET(res), MSB_WORD(res), MSB2_WORD(cc), OF_MSB2);
	return res;
}

/* ADC - Add with Carry - 32-bit */
static u32 adc_long(struct x86emu *emu, u32 op1, u32 op2)
{
	u64 res64 = (u64)op1 + (u64)op2 + CF_STATUS;
	u32 cc, res = LOW_LONG(res64);

	cc = (op2 & op1) | ((~res64) & (op2 | op1));
	x86emu_set_flags(emu, CFA_LONG(res64), LOW_BYTE(res), MSB_NIBBLE(cc),
			 ZF_SET(res), MSB_LONG(res), MSB2_LONG(cc), OF_MSB2);
	return res;
}

/* ADD - Integer Addition - 8-bit */
static u8 add_byte(struct x86emu *emu, u8 op1, u8 op2)
{
	u16 res16 = (u16)op1 + (u16)op2;
	u8 cc, res = LOW_BYTE(res16);

	cc = (op2 & op1) | ((~res16) & (op2 | op1)); /* Carry chain */
	x86emu_set_flags(emu, CFA_BYTE(res16), res, MSB_NIBBLE(cc),
			 ZF_SET(res), MSB_BYTE(res), MSB2_BYTE(cc), OF_MSB2);
	return res;
}

/* ADD - Integer Addition - 16-bit */
static u16 add_word(struct x86emu *emu, u16 op1, u16 op2)
{
	u32 res32 = (u32)op1 + (u32)op2;
	u16 cc, res = LOW_WORD(res32);

	cc = (op2 & op1) | ((~res32) & (op2 | op1));
	x86emu_set_flags(emu, CFA_WORD(res32), LOW_BYTE(res), MSB_NIBBLE(cc),
			 ZF_SET(res), MSB_WORD(res), MSB2_WORD(cc), OF_MSB2);
	return res;
}

/* ADD - Integer Addition - 32-bit */
static u32 add_long(struct x86emu *emu, u32 op1, u32 op2)
{
	u64 res64 = (u64)op1 + (u64)op2;
	u32 cc, res = LOW_LONG(res64);

	cc = (op2 & op1) | ((~res64) & (op2 | op1));
	x86emu_set_flags(emu, CFA_LONG(res64), LOW_BYTE(res), MSB_NIBBLE(cc),
			 ZF_SET(res), MSB_LONG(res), MSB2_LONG(cc), OF_MSB2);
	return res;
}

/* AND - 8-bit */
static u8 and_byte(struct x86emu *emu, u8 op1, u8 op2)
{
	u8 res = op1 & op2;

	x86emu_set_flags(emu, CF_CLEAR, res, AF_CLEAR,
			 ZF_SET(res), MSB_BYTE(res), OF_CLEAR, OF_BOOL);
	return res;
}

/* AND - 16-bit */
static u16 and_word(struct x86emu *emu, u16 op1, u16 op2)
{
	u16 res = op1 & op2;

	x86emu_set_flags(emu, CF_CLEAR, LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_WORD(res), OF_CLEAR, OF_BOOL);
	return res;
}

/* AND - 32-bit */
static u32 and_long(struct x86emu *emu, u32 op1, u32 op2)
{
	u32 res = op1 & op2;

	x86emu_set_flags(emu, CF_CLEAR, LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_LONG(res), OF_CLEAR, OF_BOOL);
	return res;
}

/* CMP - Compare - 8-bit */
static u8 cmp_byte_r(struct x86emu *emu, u8 op1, u8 op2)
{
	u8 bc, res = op1 - op2;

	bc = (res & (~op1 | op2)) | (~op1 & op2); /* Borrow chain */
	x86emu_set_flags(emu, MSB_BYTE(bc), LOW_BYTE(res), MSB_NIBBLE(bc),
			 ZF_SET(res), MSB_BYTE(res), MSB2_BYTE(bc), OF_MSB2);
	return op1;
}

static void cmp_byte(struct x86emu *emu, u8 op1, u8 op2)
{
	(void)cmp_byte_r(emu, op1, op2);
}

/* CMP - Compare - 16-bit */
static u16 cmp_word_r(struct x86emu *emu, u16 op1, u16 op2)
{
	u16 bc, res = op1 - op2;

	bc = (res & (~op1 | op2)) | (~op1 & op2);
	x86emu_set_flags(emu, MSB_WORD(bc), LOW_BYTE(res), MSB_NIBBLE(bc),
			 ZF_SET(res), MSB_WORD(res), MSB2_WORD(bc), OF_MSB2);
	return op1;
}

static void cmp_word(struct x86emu *emu, u16 op1, u16 op2)
{
	(void)cmp_word_r(emu, op1, op2);
}

/* CMP - Compare - 32-bit */
static u32 cmp_long_r(struct x86emu *emu, u32 op1, u32 op2)
{
	u32 bc, res = op1 - op2;

	bc = (res & (~op1 | op2)) | (~op1 & op2);
	x86emu_set_flags(emu, MSB_LONG(bc), LOW_BYTE(res), MSB_NIBBLE(bc),
			 ZF_SET(res), MSB_LONG(res), MSB2_LONG(bc), OF_MSB2);
	return op1;
}

static void cmp_long(struct x86emu *emu, u32 op1, u32 op2)
{
	(void)cmp_long_r(emu, op1, op2);
}

/* DAA - Decimal Adjust after Addition */
static u8 daa_byte(struct x86emu *emu, u8 op1)
{
	u8 al = op1;
	bool af, cf;

	if ((op1 & 0xf) > 0x9 || ACCESS_FLAG(F_AF)) {
		al += 0x6;
		af = true;
	} else {
		af = false;
	}
	if (op1 > 0x99 || ACCESS_FLAG(F_CF)) {
		al += 0x60;
		cf = true;
	} else {
		cf = false;
	}
	x86emu_set_flags(emu, cf, al, af,
			 ZF_SET(al), MSB_BYTE(al), OF_CLEAR, OF_BOOL);
	return al;
}

/* DAS - Decimal Adjust after Subtraction */
static u8 das_byte(struct x86emu *emu, u8 op1)
{
	u8 al = op1;
	bool af, cf;

	if ((op1 & 0xf) > 0x9 || ACCESS_FLAG(F_AF)) {
		al -= 0x6;
		af = true;
	} else {
		af = false;
	}
	if (op1 > 0x99 || ACCESS_FLAG(F_CF)) {
		al -= 0x60;
		cf = true;
	} else {
		cf = false;
	}
	x86emu_set_flags(emu, cf, al, af,
			 ZF_SET(al), MSB_BYTE(al), OF_CLEAR, OF_BOOL);
	return al;
}

/* DEC - Decrement - 8-bit */
static u8 dec_byte(struct x86emu *emu, u8 op1)
{
	u8 bc, res = op1 - 1;
	bool cf = CF_STATUS;

	bc = (res & (~op1 | 1)) | (~op1 & 1); /* Borrow chain */
	x86emu_set_flags(emu, cf, res, MSB_NIBBLE(bc),
			 ZF_SET(res), MSB_BYTE(res), MSB2_BYTE(bc), OF_MSB2);
	return res;
}

/* DEC - Decrement - 16-bit */
static u16 dec_word(struct x86emu *emu, u16 op1)
{
	u16 bc, res = op1 - 1;
	bool cf = CF_STATUS;

	bc = (res & (~op1 | 1)) | (~op1 & 1);
	x86emu_set_flags(emu, cf, LOW_BYTE(res), MSB_NIBBLE(bc),
			 ZF_SET(res), MSB_WORD(res), MSB2_WORD(bc), OF_MSB2);
	return res;
}

/* DEC - Decrement - 32-bit */
static u32 dec_long(struct x86emu *emu, u32 op1)
{
	u32 bc, res = op1 - 1;
	bool cf = CF_STATUS;

	bc = (res & (~op1 | 1)) | (~op1 & 1);
	x86emu_set_flags(emu, cf, LOW_BYTE(res), MSB_NIBBLE(bc),
			 ZF_SET(res), MSB_LONG(res), MSB2_LONG(bc), OF_MSB2);
	return res;
}

/* DIV - Unsigned Divide - 8-bit */
static void div_byte(struct x86emu *emu, u8 op1)
{
	u16 dvd, mod, res16;

	X86EMU_DIV_ERR(op1 == 0);
	dvd = emu->x86.R_AX;
	res16 = dvd / op1;
	mod = dvd % op1;
	X86EMU_DIV_ERR(res16 > 0xff);
	emu->x86.R_AL = (u8)res16;
	emu->x86.R_AH = (u8)mod;
}

/* DIV - Unsigned Divide - 16-bit */
static void div_word(struct x86emu *emu, u16 op1)
{
	u32 dvd, mod, res32;

	X86EMU_DIV_ERR(op1 == 0);
	dvd = (((u32)emu->x86.R_DX) << 16) | emu->x86.R_AX;
	res32 = dvd / op1;
	mod = dvd % op1;
	X86EMU_DIV_ERR(res32 > 0xffff);
	emu->x86.R_AX = (u16)res32;
	emu->x86.R_DX = (u16)mod;
}

/* DIV - Unsigned Divide - 32-bit */
static void div_long(struct x86emu *emu, u32 op1)
{
	u64 dvd, res64;
	u32 mod;

	X86EMU_DIV_ERR(op1 == 0);
	dvd = (((u64)emu->x86.R_EDX) << 32) | emu->x86.R_EAX;
	res64 = div_u64_rem(dvd, op1, &mod);
	X86EMU_DIV_ERR(res64 > 0xffffffff);
	emu->x86.R_EAX = (u32)res64;
	emu->x86.R_EDX = mod;
}

/* IDIV - Signed Divide - 8-bit */
static void idiv_byte(struct x86emu *emu, u8 op1)
{
	s16 dvd, mod, res16;

	X86EMU_DIV_ERR(op1 == 0);
	dvd = (s16)emu->x86.R_AX;
	res16 = dvd / (s8)op1;
	mod = dvd % (s8)op1;
#if X86EMU_CPU < X86EMU_186
	X86EMU_DIV_ERR(res16 > 0x7f || res16 < -0x7f); /* 8086 */
#else
	X86EMU_DIV_ERR(res16 > 0x7f || res16 < (-0x7f - 1)); /* 80186+ */
#endif
	emu->x86.R_AL = (u8)res16;
	emu->x86.R_AH = (u8)mod;
}

/* IDIV - Signed Divide - 16-bit */
static void idiv_word(struct x86emu *emu, u16 op1)
{
	s32 dvd, mod, res32;

	X86EMU_DIV_ERR(op1 == 0);
	dvd = (((s32)emu->x86.R_DX) << 16) | emu->x86.R_AX;
	res32 = dvd / (s16)op1;
	mod = dvd % (s16)op1;
#if X86EMU_CPU < X86EMU_186
	X86EMU_DIV_ERR(res32 > 0x7fff || res32 < -0x7fff); /* 8086 */
#else
	X86EMU_DIV_ERR(res32 > 0x7fff || res32 < (-0x7fff - 1)); /* 80186+ */
#endif
	emu->x86.R_AX = (u16)res32;
	emu->x86.R_DX = (u16)mod;
}

/* IDIV - Signed Divide - 32-bit */
static void idiv_long(struct x86emu *emu, u32 op1)
{
	s64 dvd, res64;
	s32 mod;

	X86EMU_DIV_ERR(op1 == 0);
	dvd = (((s64)emu->x86.R_EDX) << 32) | emu->x86.R_EAX;
	res64 = div_s64_rem(dvd, (s32)op1, &mod);
#if X86EMU_CPU < X86EMU_186
	X86EMU_DIV_ERR(res64 > 0x7fffffff || res64 < -0x7fffffff); /* 8086 */
#else
	X86EMU_DIV_ERR(res64 > 0x7fffffff || res64 < (-0x7fffffff - 1)); /* 80186+ */
#endif
	emu->x86.R_EAX = (u32)res64;
	emu->x86.R_EDX = (u32)mod;
}

/* IMUL - Signed Multiplication - 8-bit */
static void imul_byte(struct x86emu *emu, u8 op1)
{
	s16 res16 = (s16)(s8)emu->x86.R_AL * (s8)op1;
	bool cf = true;
	u8 of = 1;

	emu->x86.R_AX = (u16)res16;
	if ((!MSB_BYTE(emu->x86.R_AL) && emu->x86.R_AH == 0x00) ||
	    (MSB_BYTE(emu->x86.R_AL) && emu->x86.R_AH == 0xff)) {
		cf = false;
		of = 0;
	}
	x86emu_set_flags(emu, cf, emu->x86.R_AL, AF_CLEAR,
			 ZF_SET(res16), MSB_BYTE(emu->x86.R_AL), of, OF_BOOL);
}

/* IMUL - Signed Multiplication - 16-bit */
static void imul_word(struct x86emu *emu, u16 op1)
{
	s32 res32 = (s32)(s16)emu->x86.R_AX * (s16)op1;
	bool cf = true;
	u8 of = 1;

	emu->x86.R_AX = (u16)((u32)res32 & 0xffff);
	emu->x86.R_DX = (u16)((u32)res32 >> 16);
	if ((!MSB_WORD(emu->x86.R_AX) && emu->x86.R_DX == 0x0000) ||
	    (MSB_WORD(emu->x86.R_AX) && emu->x86.R_DX == 0xffff)) {
		cf = false;
		of = 0;
	}
	x86emu_set_flags(emu, cf, LOW_BYTE(emu->x86.R_AX), AF_CLEAR,
			 ZF_SET(res32), MSB_WORD(emu->x86.R_AX), of, OF_BOOL);
}

/* IMUL - Signed Multiplication - 32-bit */
static void imul_long(struct x86emu *emu, u32 op1)
{
	s64 res64 = (s64)(s32)emu->x86.R_EAX * (s32)op1;
	bool cf = true;
	u8 of = 1;

	emu->x86.R_EAX = (u32)((u64)res64 & 0xffffffff);
	emu->x86.R_EDX = (u32)((u64)res64 >> 32);
	if ((!MSB_LONG(emu->x86.R_EAX) && emu->x86.R_EDX == 0x00000000) ||
	    (MSB_LONG(emu->x86.R_EAX) && emu->x86.R_EDX == 0xffffffff)) {
		cf = false;
		of = 0;
	}
	x86emu_set_flags(emu, cf, LOW_BYTE(emu->x86.R_EAX), AF_CLEAR,
			 ZF_SET(res64), MSB_LONG(emu->x86.R_EAX), of, OF_BOOL);
}

/* INC - Increment - 8-bit */
static u8 inc_byte(struct x86emu *emu, u8 op1)
{
	u8 cc, res = op1 + 1;
	bool cf = CF_STATUS;

	cc = ((1 & op1) | (~res)) & (1 | op1); /* Carry chain */
	x86emu_set_flags(emu, cf, res, MSB_NIBBLE(cc),
			 ZF_SET(res), MSB_BYTE(res), MSB2_BYTE(cc), OF_MSB2);
	return res;
}

/* INC - Increment - 16-bit */
static u16 inc_word(struct x86emu *emu, u16 op1)
{
	u16 cc, res = op1 + 1;
	bool cf = CF_STATUS;

	cc = ((1 & op1) | (~res)) & (1 | op1);
	x86emu_set_flags(emu, cf, LOW_BYTE(res), MSB_NIBBLE(cc),
			 ZF_SET(res), MSB_WORD(res), MSB2_WORD(cc), OF_MSB2);
	return res;
}

/* INC - Increment - 32-bit */
static u32 inc_long(struct x86emu *emu, u32 op1)
{
	u32 cc, res = op1 + 1;
	bool cf = CF_STATUS;

	cc = ((1 & op1) | (~res)) & (1 | op1);
	x86emu_set_flags(emu, cf, LOW_BYTE(res), MSB_NIBBLE(cc),
			 ZF_SET(res), MSB_LONG(res), MSB2_LONG(cc), OF_MSB2);
	return res;
}

/* INS - Input String */
static void ins(struct x86emu *emu, s32 size)
{
	s32 inc = (ACCESS_FLAG(F_DF)) ? -size : size;

	if (MODE_REP) {
		/* Don't care whether REPE or REPNE, in until CX is ZERO */
		u32 count = (MODE_DATA32) ? emu->x86.R_ECX : emu->x86.R_CX;

		switch (size) {
		case 1:
			while (count--) {
				store_byte(emu, emu->x86.R_ES, emu->x86.R_DI, (*emu->x86_inb)(emu->x86.R_DX));
				emu->x86.R_DI += inc;
			}
			break;
		case 2:
			while (count--) {
				store_word(emu, emu->x86.R_ES, emu->x86.R_DI, (*emu->x86_inw)(emu->x86.R_DX));
				emu->x86.R_DI += inc;
			}
			break;
		case 4:
			while (count--) {
				store_long(emu, emu->x86.R_ES, emu->x86.R_DI, (*emu->x86_inl)(emu->x86.R_DX));
				emu->x86.R_DI += inc;
			}
			break;
		}
		emu->x86.R_CX = 0;
		if (MODE_DATA32)
			emu->x86.R_ECX = 0;
		CLEAR_MODE_REP;
	} else {
		switch (size) {
		case 1:
			store_byte(emu, emu->x86.R_ES, emu->x86.R_DI, (*emu->x86_inb)(emu->x86.R_DX));
			break;
		case 2:
			store_word(emu, emu->x86.R_ES, emu->x86.R_DI, (*emu->x86_inw)(emu->x86.R_DX));
			break;
		case 4:
			store_long(emu, emu->x86.R_ES, emu->x86.R_DI, (*emu->x86_inl)(emu->x86.R_DX));
			break;
		}
		emu->x86.R_DI += inc;
	}
}

/* MUL - Unsigned Multiplication - 8-bit */
static void mul_byte(struct x86emu *emu, u8 op1)
{
	u16 res16 = (u16)emu->x86.R_AL * op1;

	emu->x86.R_AX = res16;
	x86emu_set_flags(emu, CF_SET(emu->x86.R_AH), emu->x86.R_AL, AF_CLEAR,
			 ZF_SET(res16), MSB_BYTE(emu->x86.R_AL), OF_SET(emu->x86.R_AH), OF_BOOL);
}

/* MUL - Unsigned Multiplication - 16-bit */
static void mul_word(struct x86emu *emu, u16 op1)
{
	u32 res32 = (u32)emu->x86.R_AX * op1;

	emu->x86.R_AX = LOW_WORD(res32);
	emu->x86.R_DX = HIGH_WORD(res32);
	x86emu_set_flags(emu, CF_SET(emu->x86.R_DX), LOW_BYTE(emu->x86.R_AX), AF_CLEAR,
			 ZF_SET(res32), MSB_WORD(emu->x86.R_AX), OF_SET(emu->x86.R_DX), OF_BOOL);
}

/* MUL - Unsigned Multiplication - 32-bit */
static void mul_long(struct x86emu *emu, u32 op1)
{
	u64 res64 = (u64)emu->x86.R_EAX * op1;

	emu->x86.R_EAX = LOW_LONG(res64);
	emu->x86.R_EDX = HIGH_LONG(res64);
	x86emu_set_flags(emu, CF_SET(emu->x86.R_EDX), LOW_BYTE(emu->x86.R_EAX), AF_CLEAR,
			 ZF_SET(res64), MSB_LONG(emu->x86.R_EAX), OF_SET(emu->x86.R_EDX), OF_BOOL);
}

/* NEG - Negate - 8-bit */
static u8 neg_byte(struct x86emu *emu, u8 op1)
{
	u8 bc, res = (u8)-op1;

	/*
	 * Calculate the borrow chain, modified such that op2 = op1, op1 = 0.
	 * substituting op1 = 0 into bc = res & (~op1 | op2) | (~op1 & op2);
	 * (the one used for sub) and simplifying, since ~op1 = 0xff...,
	 * ~op1 | op2 == 0xffff..., and res & 0xffff... == res.
	 * Similarly ~op1 & op2 == op2. So the simplified result is:
	 */
	bc = res | op1; /* Borrow chain */
	x86emu_set_flags(emu, CF_SET(op1), res, MSB_NIBBLE(bc),
			 ZF_SET(res), MSB_BYTE(res), MSB2_BYTE(bc), OF_MSB2);
	return res;
}

/* NEG - Negate - 16-bit */
static u16 neg_word(struct x86emu *emu, u16 op1)
{
	u16 bc, res = (u16)-op1;

	bc = res | op1;
	x86emu_set_flags(emu, CF_SET(op1), LOW_BYTE(res), MSB_NIBBLE(bc),
			 ZF_SET(res), MSB_WORD(res), MSB2_WORD(bc), OF_MSB2);
	return res;
}

/* NEG - Negate - 32-bit */
static u32 neg_long(struct x86emu *emu, u32 op1)
{
	u32 bc, res = (u32)-op1;

	bc = res | op1;
	x86emu_set_flags(emu, CF_SET(op1), LOW_BYTE(res), MSB_NIBBLE(bc),
			 ZF_SET(res), MSB_LONG(res), MSB2_LONG(bc), OF_MSB2);
	return res;
}

/* OR - Inclusive OR - 8-bit */
static u8 or_byte(struct x86emu *emu, u8 op1, u8 op2)
{
	u8 res = op1 | op2;

	x86emu_set_flags(emu, CF_CLEAR, res, AF_CLEAR,
			 ZF_SET(res), MSB_BYTE(res), OF_CLEAR, OF_BOOL);
	return res;
}

/* OR - Inclusive OR - 16-bit */
static u16 or_word(struct x86emu *emu, u16 op1, u16 op2)
{
	u16 res = op1 | op2;

	x86emu_set_flags(emu, CF_CLEAR, LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_WORD(res), OF_CLEAR, OF_BOOL);
	return res;
}

/* OR - Inclusive OR - 32-bit */
static u32 or_long(struct x86emu *emu, u32 op1, u32 op2)
{
	u32 res = op1 | op2;

	x86emu_set_flags(emu, CF_CLEAR, LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_LONG(res), OF_CLEAR, OF_BOOL);
	return res;
}

/* OUT - Output String */
static void outs(struct x86emu *emu, s32 size)
{
	s32 inc = (ACCESS_FLAG(F_DF)) ? -size : size;

	if (MODE_REP) {
		/* Don't care whether REPE or REPNE, out until CX is ZERO */
		u32 count = (MODE_DATA32) ? emu->x86.R_ECX : emu->x86.R_CX;

		switch (size) {
		case 1:
			while (count--) {
				(*emu->x86_outb)(emu->x86.R_DX, fetch_byte(emu, emu->x86.R_ES, emu->x86.R_SI));
				emu->x86.R_SI += inc;
			}
			break;
		case 2:
			while (count--) {
				(*emu->x86_outw)(emu->x86.R_DX, fetch_word(emu, emu->x86.R_ES, emu->x86.R_SI));
				emu->x86.R_SI += inc;
			}
			break;
		case 4:
			while (count--) {
				(*emu->x86_outl)(emu->x86.R_DX, fetch_long(emu, emu->x86.R_ES, emu->x86.R_SI));
				emu->x86.R_SI += inc;
			}
			break;
		}
		emu->x86.R_CX = 0;
		if (MODE_DATA32)
			emu->x86.R_ECX = 0;
		CLEAR_MODE_REP;
	} else {
		switch (size) {
		case 1:
			(*emu->x86_outb)(emu->x86.R_DX, fetch_byte(emu, emu->x86.R_ES, emu->x86.R_SI));
			break;
		case 2:
			(*emu->x86_outw)(emu->x86.R_DX, fetch_word(emu, emu->x86.R_ES, emu->x86.R_SI));
			break;
		case 4:
			(*emu->x86_outl)(emu->x86.R_DX, fetch_long(emu, emu->x86.R_ES, emu->x86.R_SI));
			break;
		}
		emu->x86.R_SI += inc;
	}
}

/* POP - Pop off Stack - 16-bit */
static u16 pop_word(struct x86emu *emu)
{
	u16 res = fetch_word(emu, emu->x86.R_SS, READ_ONCE(emu->x86.R_SP));

	WRITE_ONCE(emu->x86.R_SP, READ_ONCE(emu->x86.R_SP) + 2);
	return res;
}

/* POP - Pop off Stack - 32-bit */
static u32 pop_long(struct x86emu *emu)
{
	u32 res = fetch_long(emu, emu->x86.R_SS, READ_ONCE(emu->x86.R_SP));

	WRITE_ONCE(emu->x86.R_SP, READ_ONCE(emu->x86.R_SP) + 4);
	return res;
}

/* PUSH - Push onto Stack - 16-bit */
static void push_word(struct x86emu *emu, u16 val)
{
	u16 new_sp = READ_ONCE(emu->x86.R_SP) - 2;

#if X86EMU_CPU < X86EMU_286
	WRITE_ONCE(emu->x86.R_SP, new_sp);
	store_word(emu, emu->x86.R_SS, new_sp, val);
#else
	store_word(emu, emu->x86.R_SS, new_sp, val);
	WRITE_ONCE(emu->x86.R_SP, new_sp);
#endif
}

/* PUSH - Push onto Stack - 32-bit */
static void push_long(struct x86emu *emu, u32 val)
{
	u16 new_sp = READ_ONCE(emu->x86.R_SP) - 4;

#if X86EMU_CPU < X86EMU_286
	WRITE_ONCE(emu->x86.R_SP, new_sp);
	store_long(emu, emu->x86.R_SS, new_sp, val);
#else
	store_long(emu, emu->x86.R_SS, new_sp, val);
	WRITE_ONCE(emu->x86.R_SP, new_sp);
#endif
}

/* RCL - Rotate through Carry Left - 8-bit */
static u8 rcl_byte(struct x86emu *emu, u8 op1, u8 op2)
{
	u8 cnt, mask, cf, res = op1;

	/*
	 * op2 is the rotate distance. It varies from 0 - 8. op1 is the byte
	 * object rotated:
	 *
	 * B_7 ... B_0 CF
	 *
	 * Want to rotate through the carry by "op2" bits. We could loop, but
	 * that's inefficient. So the width is 9, and we split into three
	 * parts:
	 *
	 * The new carry flag (was B_n) the stuff in B_n-1 .. B_0 the stuff in
	 * B_7 .. B_n+1
	 *
	 * The new rotate is done mod 9, and given this, for a rotation of n bits
	 * (mod 9) the new carry flag is then located n bits from the MSB.
	 * The low part is then shifted up cnt bits, and the high part is or'd
	 * in. Using CAPS for new values, and lowercase for the original
	 * values, this can be expressed as:
	 *
	 * IF n > 0 1) CF <- b_(8-n) 2) B_(7) .. B_(n) <- b_(8-(n+1)) .. b_0
	 * 3) B_(n-1) <- cf 4) B_(n-2) .. B_0 <- b_7 .. b_(8-(n-1))
	 */

	cnt = X86EMU_SHIFT_MASK(op2) % 9;
	if (cnt) { /* Zero-bit rotate does nothing, that is affects no flags */

		/*
		 * Get the high stuff which rotated around into the positions
		 * B_cnt-2 .. B_0
		 * B_(n-2) .. B_0 <- b_7 .. b_(8-(n-1))
		 * shift it downward, 7-(n-2) = 9-n positions. and mask off
		 * the result before or'ing in.
		 */
		mask = (1 << (cnt - 1)) - 1;
		/*
		 * Get the low stuff which rotated into the range B_7 .. B_cnt
		 * B_(7) .. B_(n) <- b_(8-(n+1)) .. b_0
		 * note that the right hand side done by the mask
		 */
		res = ((u16)op1 << cnt) & 0xff;
		res |= ((u16)op1 >> (9 - cnt)) & mask;
		if (ACCESS_FLAG(F_CF)) /* Carry flag is set */
			res |= 1 << (cnt - 1); /* B_(n-1) <- cf */
		/* Extract the new Carry flag */
		cf = (op1 >> (8 - cnt)) & 1; /* CF <- b_(8-n) */
		/* Overflow is the XOR of cf and the MSB of the result */
		x86emu_set_flags(emu, CF_SET(cf), PF_STATUS, AF_STATUS,
				 ZF_STATUS, SF_STATUS, CF_SET(cf) ^ MSB_BYTE(res), OF_BOOL);
	}
	return res;
}

/* RCL - Rotate through Carry Left - 16-bit */
static u16 rcl_word(struct x86emu *emu, u16 op1, u8 op2)
{
	u16 cnt, mask, cf, res = op1;

	cnt = X86EMU_SHIFT_MASK(op2) % 17;
	if (cnt) {
		mask = (1 << (cnt - 1)) - 1;
		res = ((u32)op1 << cnt) & 0xffff;
		res |= ((u32)op1 >> (17 - cnt)) & mask;
		if (ACCESS_FLAG(F_CF))
			res |= 1 << (cnt - 1);
		cf = (op1 >> (16 - cnt)) & 1;
		x86emu_set_flags(emu, CF_SET(cf), PF_STATUS, AF_STATUS,
				 ZF_STATUS, SF_STATUS, CF_SET(cf) ^ MSB_WORD(res), OF_BOOL);
	}
	return res;
}

/* RCL - Rotate through Carry Left - 32-bit */
static u32 rcl_long(struct x86emu *emu, u32 op1, u8 op2)
{
	u32 cnt, mask, cf, res = op1;

	cnt = X86EMU_SHIFT_MASK(op2) % 33;
	if (cnt) {
		mask = (1 << (cnt - 1)) - 1;
		res = ((u64)op1 << cnt) & 0xffffffff;
		res |= ((u64)op1 >> (33 - cnt)) & mask;
		if (ACCESS_FLAG(F_CF))
			res |= 1 << (cnt - 1);
		cf = (op1 >> (32 - cnt)) & 1;
		x86emu_set_flags(emu, CF_SET(cf), PF_STATUS, AF_STATUS,
				 ZF_STATUS, SF_STATUS, CF_SET(cf) ^ MSB_LONG(res), OF_BOOL);
	}
	return res;
}

/* RCR - Rotate through Carry Right - 8-bit */
static u8 rcr_byte(struct x86emu *emu, u8 op1, u8 op2)
{
	u8 cnt, mask, cf, res = op1;
	bool old_cf = CF_STATUS;

	/*
	 * op2 is the rotate distance. It varies from 0 - 8. op1 is the byte
	 * object rotated:
	 *
	 * CF B_7 ... B_0
	 *
	 * The rotate is done mod 9, and given this, for a rotation of n bits
	 * (mod 9) the new carry flag is then located n bits from the LSB.
	 * The low part is then shifted up cnt bits, and the high part is or'd
	 * in. Using CAPS for new values, and lowercase for the original
	 * values, this can be expressed as:
	 *
	 * IF n > 0 1) CF <- b_(n-1) 2) B_(8-(n+1)) .. B_(0) <- b_(7) .. b_(n)
	 * 3) B_(8-n) <- cf 4) B_(7) .. B_(8-(n-1)) <- b_(n-2) .. b_(0)
	 */

	cnt = X86EMU_SHIFT_MASK(op2) % 9;
	if (cnt) { /* Zero-bit rotate does nothing, that is affects no flags */
		/*
		 * B_(8-(n+1)) .. B_(0) <- b_(7) .. b_n
		 * note that the right hand side done by the mask This is
		 * effectively done by shifting the object to the right. The
		 * result must be masked, in case the object came in and was
		 * treated as a negative number. Needed???
		 */
		mask = (1 << (8 - cnt)) - 1;
		res = ((u16)op1 >> cnt) & mask;

		/*
		 * Now the high stuff which rotated around into the positions
		 * B_cnt-2 .. B_0
		 * B_(7) .. B_(8-(n-1)) <- b_(n-2) .. b_(0)
		 * shift it downward, 7-(n-2) = 9-n positions. and mask off
		 * the result before or'ing in.
		 */
		res |= ((u16)op1 << (9 - cnt)) & 0xff;
		if (ACCESS_FLAG(F_CF)) /* Carry flag is set */
			res |= 1 << (8 - cnt); /* B_(8-n) <- cf */
		/* Extract the new Carry flag */
		cf = (op1 >> (cnt - 1)) & 1; /* CF <- b_(n-1) */
		/* Overflow is the XOR of the cf and the MSB of op1 */
		x86emu_set_flags(emu, CF_SET(cf), PF_STATUS, AF_STATUS,
				 ZF_STATUS, SF_STATUS, old_cf ^ MSB_BYTE(op1), OF_BOOL);
	}
	return res;
}

/* RCR - Rotate through Carry Right - 16-bit */
static u16 rcr_word(struct x86emu *emu, u16 op1, u8 op2)
{
	u16 cnt, mask, cf, res = op1;
	bool old_cf = CF_STATUS;

	cnt = X86EMU_SHIFT_MASK(op2) % 17;
	if (cnt) {
		mask = (1 << (16 - cnt)) - 1;
		res = ((u32)op1 >> cnt) & mask;
		res |= ((u32)op1 << (17 - cnt)) & 0xffff;
		if (ACCESS_FLAG(F_CF))
			res |= 1 << (16 - cnt);
		cf = (op1 >> (cnt - 1)) & 1;
		x86emu_set_flags(emu, CF_SET(cf), PF_STATUS, AF_STATUS,
				 ZF_STATUS, SF_STATUS, old_cf ^ MSB_WORD(op1), OF_BOOL);
	}
	return res;
}

/* RCR - Rotate through Carry Right - 32-bit */
static u32 rcr_long(struct x86emu *emu, u32 op1, u8 op2)
{
	u32 cnt, mask, cf, res = op1;
	bool old_cf = CF_STATUS;

	cnt = X86EMU_SHIFT_MASK(op2) % 33;
	if (cnt) {
		mask = (1 << (32 - cnt)) - 1;
		res = ((u64)op1 >> cnt) & mask;
		res |= ((u64)op1 << (33 - cnt)) & 0xffffffff;
		if (ACCESS_FLAG(F_CF))
			res |= 1 << (32 - cnt);
		cf = (op1 >> (cnt - 1)) & 1;
		x86emu_set_flags(emu, CF_SET(cf), PF_STATUS, AF_STATUS,
				 ZF_STATUS, SF_STATUS, old_cf ^ MSB_LONG(op1), OF_BOOL);
	}
	return res;
}

/* ROL - Rotate left - 8-bit */
static u8 rol_byte(struct x86emu *emu, u8 op1, u8 op2)
{
	u8 cnt, mask, res = op1;

	/*
	 * op2 is the rotate distance. It varies from 0 - 7. op1 is the byte
	 * object rotated. Have:
	 *
	 * B_7 ... B_0 CF
	 *
	 * The rotate is done mod 8.
	 * operations.
	 *
	 * IF n > 0 1) B_(7) .. B_(n) <- b_(8-(n+1)) .. b_(0) 2) B_(n-1) ..
	 * B_(0) <- b_(7) .. b_(8-n)
	 */

	cnt = X86EMU_SHIFT_MASK(op2) % 8;
	if (cnt) {
		mask = (1 << cnt) - 1; /* B_(n-1) .. B_(0) <- b_(7) .. b_(8-n) */
		res = (op1 << cnt); /* B_(7) .. B_(n) <- b_(8-(n+1)) .. b_(0) */
		res |= (op1 >> (8 - cnt)) & mask;
		/* Overflow is the XOR of the LSB and the MSB of the result */
		x86emu_set_flags(emu, LSB_BIT(res), PF_STATUS, AF_STATUS,
				 ZF_STATUS, SF_STATUS, LSB_BIT(res) ^ MSB_BYTE(res), OF_BOOL);
	}
	return res;
}

/* ROL - Rotate left - 16-bit */
static u16 rol_word(struct x86emu *emu, u16 op1, u8 op2)
{
	u16 cnt, mask, res = op1;

	cnt = X86EMU_SHIFT_MASK(op2) % 16;
	if (cnt) {
		mask = (1 << cnt) - 1;
		res = (op1 << cnt);
		res |= (op1 >> (16 - cnt)) & mask;
		x86emu_set_flags(emu, LSB_BIT(res), PF_STATUS, AF_STATUS,
				 ZF_STATUS, SF_STATUS, LSB_BIT(res) ^ MSB_WORD(res), OF_BOOL);
	}
	return res;
}

/* ROL - Rotate left - 32-bit */
static u32 rol_long(struct x86emu *emu, u32 op1, u8 op2)
{
	u32 cnt, mask, res = op1;

	cnt = X86EMU_SHIFT_MASK(op2) % 32;
	if (cnt) {
		mask = (1 << cnt) - 1;
		res = (op1 << cnt);
		res |= (op1 >> (32 - cnt)) & mask;
		x86emu_set_flags(emu, LSB_BIT(res), PF_STATUS, AF_STATUS,
				 ZF_STATUS, SF_STATUS, LSB_BIT(res) ^ MSB_LONG(res), OF_BOOL);
	}
	return res;
}

/* ROR - Rotate Right - 8-bit */
static u8 ror_byte(struct x86emu *emu, u8 op1, u8 op2)
{
	u8 cnt, mask, res = op1;

	/*
	 * op2 is the rotate distance. It varies from 0 - 7. op1 is the byte
	 * object rotated. Have:
	 *
	 * CF B_7 ... B_0
	 *
	 * The rotate is done mod 8.
	 *
	 * IF n > 0 1) B_(8-(n+1)) .. B_(0) <- b_(7) .. b_(n) 2) B_(7) ..
	 * B_(8-n) <- b_(n-1) .. b_(0)
	 */

	cnt = X86EMU_SHIFT_MASK(op2) % 8;
	if (cnt) {
		mask = (1 << (8 - cnt)) - 1; /* B_(8-(n+1)) .. B_(0) <- b_(7) .. b_(n) */
		res = (op1 << (8 - cnt)); /* B_(7) .. B_(8-n) <- b_(n-1) .. b_(0) */
		res |= (op1 >> (cnt)) & mask;
		/* Overflow is the XOR of the two MSB of the result */
		x86emu_set_flags(emu, MSB_BYTE(res), PF_STATUS, AF_STATUS,
				 ZF_STATUS, SF_STATUS, MSB2_BYTE(res), OF_MSB2);
	}
	return res;
}

/* ROR - Rotate Right - 16-bit */
static u16 ror_word(struct x86emu *emu, u16 op1, u8 op2)
{
	u16 cnt, mask, res = op1;

	cnt = X86EMU_SHIFT_MASK(op2) % 16;
	if (cnt) {
		mask = (1 << (16 - cnt)) - 1;
		res = (op1 << (16 - cnt));
		res |= (op1 >> (cnt)) & mask;
		x86emu_set_flags(emu, MSB_WORD(res), PF_STATUS, AF_STATUS,
				 ZF_STATUS, SF_STATUS, MSB2_WORD(res), OF_MSB2);
	}
	return res;
}

/* ROR - Rotate Right - 32-bit */
static u32 ror_long(struct x86emu *emu, u32 op1, u8 op2)
{
	u32 cnt, mask, res = op1;

	cnt = X86EMU_SHIFT_MASK(op2) % 32;
	if (cnt) {
		mask = (1 << (32 - cnt)) - 1;
		res = (op1 << (32 - cnt));
		res |= (op1 >> (cnt)) & mask;
		x86emu_set_flags(emu, MSB_LONG(res), PF_STATUS, AF_STATUS,
				 ZF_STATUS, SF_STATUS, MSB2_LONG(res), OF_MSB2);
	}
	return res;
}

/* SAR - Shift Arithmetic Right - 8-bit */
static u8 sar_byte(struct x86emu *emu, u8 op1, u8 op2)
{
	u8 cnt, mask, cf, res;
	bool sf = MSB_BYTE(op1);

	cnt = X86EMU_SHIFT_MASK(op2);
	if (cnt < 8) {
		if (cnt == 0)
			return op1;
		mask = (1 << (8 - cnt)) - 1;
		res = (op1 >> cnt) & mask;
		if (sf)
			res |= ~mask;
		cf = op1 & (1 << (cnt - 1));
	} else {
		res = (sf) ? 0xff : 0x00;
		cf = (sf) ? 1 : 0;
	}
	x86emu_set_flags(emu, CF_SET(cf), res, AF_CLEAR,
			 ZF_SET(res), MSB_BYTE(res), OF_CLEAR, OF_BOOL);
	return res;
}

/* SAR - Shift Arithmetic Right - 16-bit */
static u16 sar_word(struct x86emu *emu, u16 op1, u8 op2)
{
	u16 cnt, mask, cf, res;
	bool sf = MSB_WORD(op1);

	cnt = X86EMU_SHIFT_MASK(op2);
	if (cnt < 16) {
		if (cnt == 0)
			return op1;
		mask = (1 << (16 - cnt)) - 1;
		res = (op1 >> cnt) & mask;
		if (sf)
			res |= ~mask;
		cf = op1 & (1 << (cnt - 1));
	} else {
		res = (sf) ? 0xffff : 0x0000;
		cf = (sf) ? 1 : 0;
	}
	x86emu_set_flags(emu, CF_SET(cf), LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_BYTE(res), OF_CLEAR, OF_BOOL);
	return res;
}

/* SAR - Shift Arithmetic Right - 32-bit */
static u32 sar_long(struct x86emu *emu, u32 op1, u8 op2)
{
	u32 cnt, mask, cf, res;
	bool sf = MSB_LONG(op1);

	cnt = X86EMU_SHIFT_MASK(op2);
	if (cnt < 32) {
		if (cnt == 0)
			return op1;
		mask = (1 << (32 - cnt)) - 1;
		res = (op1 >> cnt) & mask;
		if (sf)
			res |= ~mask;
		cf = op1 & (1 << (cnt - 1));
	} else { /* Unused on 80186+ */
		res = (sf) ? 0xffffffff : 0x00000000;
		cf = (sf) ? 1 : 0;
	}
	x86emu_set_flags(emu, CF_SET(cf), LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_BYTE(res), OF_CLEAR, OF_BOOL);
	return res;
}

/* SBB - Subtract with Borrow - 8-bit */
static u8 sbb_byte(struct x86emu *emu, u8 op1, u8 op2)
{
	u8 bc, res = op1 - op2 - CF_STATUS;

	bc = (res & (~op1 | op2)) | (~op1 & op2); /* Borrow chain */
	x86emu_set_flags(emu, MSB_BYTE(bc), res, MSB_NIBBLE(bc),
			 ZF_SET(res), MSB_BYTE(res), MSB2_BYTE(bc), OF_MSB2);
	return res;
}

/* SBB - Subtract with Borrow - 16-bit */
static u16 sbb_word(struct x86emu *emu, u16 op1, u16 op2)
{
	u16 bc, res = op1 - op2 - CF_STATUS;

	bc = (res & (~op1 | op2)) | (~op1 & op2);
	x86emu_set_flags(emu, MSB_WORD(bc), LOW_BYTE(res), MSB_NIBBLE(bc),
			 ZF_SET(res), MSB_WORD(res), MSB2_WORD(bc), OF_MSB2);
	return res;
}

/* SBB - Subtract with Borrow - 32-bit */
static u32 sbb_long(struct x86emu *emu, u32 op1, u32 op2)
{
	u32 bc, res = op1 - op2 - CF_STATUS;

	bc = (res & (~op1 | op2)) | (~op1 & op2);
	x86emu_set_flags(emu, MSB_LONG(bc), LOW_BYTE(res), MSB_NIBBLE(bc),
			 ZF_SET(res), MSB_LONG(res), MSB2_LONG(bc), OF_MSB2);
	return res;
}

/* SAL/SHL - Shift Arithmetic/Logical Left - 8-bit */
static u8 shl_byte(struct x86emu *emu, u8 op1, u8 op2)
{
	u8 cnt, cf, res;

	cnt = X86EMU_SHIFT_MASK(op2);
	if (cnt < 8) {
		if (cnt == 0)
			return op1;
		res = op1 << cnt;
		cf = op1 & (1 << (8 - cnt));
	} else {
		res = 0x00;
		cf = 0;
	}
	x86emu_set_flags(emu, CF_SET(cf), res, AF_CLEAR,
			 ZF_SET(res), MSB_BYTE(res), CF_SET(cf) ^ MSB_BYTE(res), OF_BOOL);
	return res;
}

/* SAL/SHL - Shift Arithmetic/Logical Left - 16-bit */
static u16 shl_word(struct x86emu *emu, u16 op1, u8 op2)
{
	u16 cnt, cf, res;

	cnt = X86EMU_SHIFT_MASK(op2);
	if (cnt < 16) {
		if (cnt == 0)
			return op1;
		res = op1 << cnt;
		cf = op1 & (1 << (16 - cnt));
	} else {
		res = 0x0000;
		cf = 0;
	}
	x86emu_set_flags(emu, CF_SET(cf), LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_WORD(res), CF_SET(cf) ^ MSB_WORD(res), OF_BOOL);
	return res;
}

/* SAL/SHL - Shift Arithmetic/Logical Left - 32-bit */
static u32 shl_long(struct x86emu *emu, u32 op1, u8 op2)
{
	u32 cnt, cf, res;

	cnt = X86EMU_SHIFT_MASK(op2);
	if (cnt < 32) {
		if (cnt == 0)
			return op1;
		res = op1 << cnt;
		cf = op1 & (1 << (32 - cnt));
	} else { /* Unused on 80186+ */
		res = 0x00000000;
		cf = 0;
	}
	x86emu_set_flags(emu, CF_SET(cf), LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_LONG(res), CF_SET(cf) ^ MSB_LONG(res), OF_BOOL);
	return res;
}

/* SHLD - Shift Left Double - 16-bit - 386+ */
static u16 shld_word(struct x86emu *emu, u16 op1, u16 op2, u8 op3)
{
	u16 cnt, cf, res;

	cnt = op3 & 0x1f;
	if (cnt < 16) {
		if (cnt == 0)
			return op1;
		res = (op1 << cnt) | (op2 >> (16 - cnt));
		cf = op1 & (1 << (16 - cnt));
	} else {
		res = 0x0000;
		cf = 0;
	}
	x86emu_set_flags(emu, CF_SET(cf), LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_WORD(res), CF_SET(cf) ^ MSB_WORD(res), OF_BOOL);
	return res;
}

/* SHLD - Shift Left Double - 32-bit - 386+ */
static u32 shld_long(struct x86emu *emu, u32 op1, u32 op2, u8 op3)
{
	u32 cnt, cf, res;

	cnt = op3 & 0x1f;
	if (cnt < 32) {
		if (cnt == 0)
			return op1;
		res = (op1 << cnt) | (op2 >> (32 - cnt));
		cf = op1 & (1 << (32 - cnt));
	} else { /* Unused */
		res = 0x00000000;
		cf = 0;
	}
	x86emu_set_flags(emu, CF_SET(cf), LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_LONG(res), CF_SET(cf) ^ MSB_LONG(res), OF_BOOL);
	return res;
}

/* SHR - Shift Logical Right - 8-bit */
static u8 shr_byte(struct x86emu *emu, u8 op1, u8 op2)
{
	u8 cnt, cf, res;

	cnt = X86EMU_SHIFT_MASK(op2);
	if (cnt < 8) {
		if (cnt == 0)
			return op1;
		res = op1 >> cnt;
		cf = op1 & (1 << (cnt - 1));
	} else {
		res = 0x00;
		cf = 0;
	}
	x86emu_set_flags(emu, CF_SET(cf), res, AF_CLEAR,
			 ZF_SET(res), MSB_BYTE(res), MSB2_BYTE(res), OF_MSB2);
	return res;
}

/* SHR - Shift Logical Right - 16-bit */
static u16 shr_word(struct x86emu *emu, u16 op1, u8 op2)
{
	u16 cnt, cf, res;

	cnt = X86EMU_SHIFT_MASK(op2);
	if (cnt < 16) {
		if (cnt == 0)
			return op1;
		res = op1 >> cnt;
		cf = op1 & (1 << (cnt - 1));
	} else {
		res = 0x0000;
		cf = 0;
	}
	x86emu_set_flags(emu, CF_SET(cf), LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_WORD(res), MSB2_WORD(res), OF_MSB2);
	return res;
}

/* SHR - Shift Logical Right - 32-bit */
static u32 shr_long(struct x86emu *emu, u32 op1, u8 op2)
{
	u32 cnt, cf, res;

	cnt = X86EMU_SHIFT_MASK(op2);
	if (cnt < 32) {
		if (cnt == 0)
			return op1;
		res = op1 >> cnt;
		cf = op1 & (1 << (cnt - 1));
	} else { /* Unused on 80186+ */
		res = 0x00000000;
		cf = 0;
	}
	x86emu_set_flags(emu, CF_SET(cf), LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_LONG(res), MSB2_LONG(res), OF_MSB2);
	return res;
}

/* SHRD - Shift Right Double - 16-bit - 386+ */
static u16 shrd_word(struct x86emu *emu, u16 op1, u16 op2, u8 op3)
{
	u16 cnt, cf, res;

	cnt = op3 & 0x1f;
	if (cnt < 16) {
		if (cnt == 0)
			return op1;
		res = (op1 >> cnt) | (op2 << (16 - cnt));
		cf = op1 & (1 << (cnt - 1));
	} else {
		res = 0x0000;
		cf = 0;
	}
	x86emu_set_flags(emu, CF_SET(cf), LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_WORD(res), MSB2_WORD(res), OF_MSB2);
	return res;
}

/* SHRD - Shift Right Double - 32-bit - 386+ */
static u32 shrd_long(struct x86emu *emu, u32 op1, u32 op2, u8 op3)
{
	u32 cnt, cf, res;

	cnt = op3 & 0x1f;
	if (cnt < 32) {
		if (cnt == 0)
			return op1;
		res = (op1 >> cnt) | (op2 << (32 - cnt));
		cf = op1 & (1 << (cnt - 1));
	} else { /* Unused */
		res = 0x00000000;
		cf = 0;
	}
	x86emu_set_flags(emu, CF_SET(cf), LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_LONG(res), MSB2_LONG(res), OF_MSB2);
	return res;
}

/* SUB - Subtract - 8-bit */
static u8 sub_byte(struct x86emu *emu, u8 op1, u8 op2)
{
	u8 bc, res = op1 - op2;

	bc = (res & (~op1 | op2)) | (~op1 & op2); /* Borrow chain */
	x86emu_set_flags(emu, MSB_BYTE(bc), res, MSB_NIBBLE(bc),
			 ZF_SET(res), MSB_BYTE(res), MSB2_BYTE(bc), OF_MSB2);
	return res;
}

/* SUB - Subtract - 16-bit */
static u16 sub_word(struct x86emu *emu, u16 op1, u16 op2)
{
	u16 bc, res = op1 - op2;

	bc = (res & (~op1 | op2)) | (~op1 & op2);
	x86emu_set_flags(emu, MSB_WORD(bc), LOW_BYTE(res), MSB_NIBBLE(bc),
			 ZF_SET(res), MSB_WORD(res), MSB2_WORD(bc), OF_MSB2);
	return res;
}

/* SUB - Subtract - 32-bit */
static u32 sub_long(struct x86emu *emu, u32 op1, u32 op2)
{
	u32 bc, res = op1 - op2;

	bc = (res & (~op1 | op2)) | (~op1 & op2);
	x86emu_set_flags(emu, MSB_LONG(bc), LOW_BYTE(res), MSB_NIBBLE(bc),
			 ZF_SET(res), MSB_LONG(res), MSB2_LONG(bc), OF_MSB2);
	return res;
}

/* TEST - Logical Compare - 8-bit */
static void test_byte(struct x86emu *emu, u8 op1, u8 op2)
{
	u8 res = op1 & op2;

	x86emu_set_flags(emu, CF_CLEAR, res, AF_CLEAR,
			 ZF_SET(res), MSB_BYTE(res), OF_CLEAR, OF_BOOL);
}

/* TEST - Logical Compare - 16-bit */
static void test_word(struct x86emu *emu, u16 op1, u16 op2)
{
	u16 res = op1 & op2;

	x86emu_set_flags(emu, CF_CLEAR, LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_WORD(res), OF_CLEAR, OF_BOOL);
}

/* TEST - Logical Compare - 32-bit */
static void test_long(struct x86emu *emu, u32 op1, u32 op2)
{
	u32 res = op1 & op2;

	x86emu_set_flags(emu, CF_CLEAR, LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_LONG(res), OF_CLEAR, OF_BOOL);
}

/* XOR - Exclusive OR - 8-bit */
static u8 xor_byte(struct x86emu *emu, u8 op1, u8 op2)
{
	u8 res = op1 ^ op2;

	x86emu_set_flags(emu, CF_CLEAR, res, AF_CLEAR,
			 ZF_SET(res), MSB_BYTE(res), OF_CLEAR, OF_BOOL);
	return res;
}

/* XOR - Exclusive OR - 16-bit */
static u16 xor_word(struct x86emu *emu, u16 op1, u16 op2)
{
	u16 res = op1 ^ op2;

	x86emu_set_flags(emu, CF_CLEAR, LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_WORD(res), OF_CLEAR, OF_BOOL);
	return res;
}

/* XOR - Exclusive OR - 32-bit */
static u32 xor_long(struct x86emu *emu, u32 op1, u32 op2)
{
	u32 res = op1 ^ op2;

	x86emu_set_flags(emu, CF_CLEAR, LOW_BYTE(res), AF_CLEAR,
			 ZF_SET(res), MSB_LONG(res), OF_CLEAR, OF_BOOL);
	return res;
}

/* Three-Byte Opcode Instructions */
static void x86emu_exec_three_byte(struct x86emu *emu, u8 op2)
{
	u8 op3 = fetch_byte_imm(emu);

	pr_err("UD: Undefined Opcode 0f%02x%02x\n", op2, op3);
	x86emu_intr_dispatch(emu, INT6_UD_FAULT);
}
