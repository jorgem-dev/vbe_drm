// SPDX-License-Identifier: MIT

/*
 * Copyright (C) 1998 Josh Vanderhoof
 * Copyright (C) 2005-2006 Matthew Garrett
 * Copyright (C) 2005-2006 Jonathan McDowell
 * Copyright (C) 2024 Jorge Maidana
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL JOSH VANDERHOOF BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/mutex.h>
#include <linux/sizes.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/types.h>

#include "x86emu.h"
#include "lrmi.h"

#define LRMI_LIB_NAME			"lrmi"

#define lrmi_pr_info(fmt, ...)		pr_info(LRMI_LIB_NAME ": " fmt, ##__VA_ARGS__)
#define lrmi_pr_warn(fmt, ...)		pr_warn(LRMI_LIB_NAME ": " fmt, ##__VA_ARGS__)
#define lrmi_pr_err(fmt, ...)		pr_err(LRMI_LIB_NAME ": " fmt, ##__VA_ARGS__)

#define LRMI_STACK_SIZE			SZ_64K
#define LRMI_MEM_BLOCKS			15

struct lrmi_mem_block {
	u32 size:24;
	u8  free;
} __packed;

struct lrmi_mem_blocks {
	u16 ready, count;
	struct lrmi_mem_block blocks[LRMI_MEM_BLOCKS];
} __packed;

/* In order of usage */
enum {
	LRMI_VBIOS = 0,
	LRMI_RAM,
	LRMI_IVT_BDA,
	LRMI_VRAM,
	LRMI_SBIOS,
	LRMI_EBDA_HI,
	LRMI_EBDA_LO,
	LRMI_MEM_LO,
	LRMI_MEM_HI,
};

struct lrmi_mem_seg {
	u8 *virt;
	u32 phys;
	u32 size;
	u32 offset;
	u32 pos;
	const char *name;
	bool iomem;
};

static struct lrmi_mem_seg lrmi_mem[] = {
	[LRMI_VBIOS]   = { NULL, 0x0c0000, 0x00000, 0, 6, "V-BIOS", true  },
	[LRMI_RAM]     = { NULL, 0x010000, 0x70000, 0, 2, "S-RAM ", false },
	[LRMI_IVT_BDA] = { NULL, 0x000000, 0x00600, 0, 0, "IVTBDA", true  },
	[LRMI_VRAM]    = { NULL, 0x0a0000, 0x20000, 0, 5, "V-RAM ", true  },
	[LRMI_SBIOS]   = { NULL, 0x0e0000, 0x20000, 0, 7, "S-BIOS", true  },
	[LRMI_EBDA_HI] = { NULL, 0x000000, 0x00000, 0, 4, "EBDA-H", true  },
	[LRMI_EBDA_LO] = { NULL, 0x080000, 0x00000, 0, 3, "EBDA-L", false },
	[LRMI_MEM_LO]  = { NULL, 0x000600, 0x0fa00, 0, 1, "MEM-LO", false },
	[LRMI_MEM_HI]  = { NULL, 0x100000, 0x10000, 0, 8, "MEM-HI", false },
};

static const u32 lrmi_mem_segs = ARRAY_SIZE(lrmi_mem);
static struct x86emu *lrmi_x86emu;
static struct lrmi_mem_blocks lrmi_real_mem;
static u32 lrmi_stack;

static DEFINE_MUTEX(lrmi_x86emu_mlock);
static DEFINE_MUTEX(lrmi_real_mem_mlock);

static void *lrmi_phys_to_virt_align(u32 phys, size_t align)
{
	int i;
#if X86EMU_CPU < X86EMU_286
	u32 addr = phys & 0xfffff; /* Wrap around */
#else
	u32 addr = phys;
#endif

	for (i = 0; i < lrmi_mem_segs; i++) {
		if (lrmi_mem[i].virt &&
		    addr >= lrmi_mem[i].phys &&
		    addr <= lrmi_mem[i].phys + lrmi_mem[i].size - align)
			return (lrmi_mem[i].virt + addr -
				lrmi_mem[i].phys + lrmi_mem[i].offset);
	}

	pr_err_once("Invalid address 0x%08x\n", addr);
	return NULL;
}

void *lrmi_phys_to_virt(u32 addr)
{
	return lrmi_phys_to_virt_align(addr, SZ_1);
}

u8 lrmi_readb(u32 addr)
{
	void __iomem *vptr = lrmi_phys_to_virt_align(addr, SZ_1);
	u8 val = 0xff;

	if (vptr)
		val = readb(vptr);
#ifdef LRMI_DEBUG
	lrmi_pr_info("readb 0x%02x <- 0x%08x\n", val, addr);
#endif
	return val;
}

u16 lrmi_readw(u32 addr)
{
	void __iomem *vptr = lrmi_phys_to_virt_align(addr, SZ_2);
	u16 val = 0xffff;

	if (vptr)
		val = readw(vptr);
#ifdef LRMI_DEBUG
	lrmi_pr_info("readw 0x%04x <- 0x%08x\n", val, addr);
#endif
	return val;
}

u32 lrmi_readl(u32 addr)
{
	void __iomem *vptr = lrmi_phys_to_virt_align(addr, SZ_4);
	u32 val = 0xffffffff;

	if (vptr)
		val = readl(vptr);
#ifdef LRMI_DEBUG
	lrmi_pr_info("readl 0x%08x <- 0x%08x\n", val, addr);
#endif
	return val;
}

void lrmi_writeb(u32 addr, u8 val)
{
	void __iomem *vptr = lrmi_phys_to_virt_align(addr, SZ_1);

	if (vptr)
		writeb(val, vptr);
#ifdef LRMI_DEBUG
	lrmi_pr_info("writeb 0x%02x -> 0x%08x\n", val, addr);
#endif
}

void lrmi_writew(u32 addr, u16 val)
{
	void __iomem *vptr = lrmi_phys_to_virt_align(addr, SZ_2);

	if (vptr)
		writew(val, vptr);
#ifdef LRMI_DEBUG
	lrmi_pr_info("writew 0x%04x -> 0x%08x\n", val, addr);
#endif
}

void lrmi_writel(u32 addr, u32 val)
{
	void __iomem *vptr = lrmi_phys_to_virt_align(addr, SZ_4);

	if (vptr)
		writel(val, vptr);
#ifdef LRMI_DEBUG
	lrmi_pr_info("writel 0x%08x -> 0x%08x\n", val, addr);
#endif
}

static void lrmi_real_mem_unmap(void)
{
	int i;

	mutex_lock(&lrmi_real_mem_mlock);
	lrmi_real_mem.ready = 0;

	for (i = 0; i < lrmi_mem_segs; i++) {
		if (lrmi_mem[i].iomem || !lrmi_mem[i].virt)
			continue;

		memset(lrmi_mem[i].virt, 0, lrmi_mem[i].size);
		kfree(lrmi_mem[i].virt);
		lrmi_mem[i].virt = NULL;
	}
	mutex_unlock(&lrmi_real_mem_mlock);
}

static int lrmi_real_mem_map(void)
{
	int i;

	mutex_lock(&lrmi_real_mem_mlock);
	if (lrmi_real_mem.ready) {
		mutex_unlock(&lrmi_real_mem_mlock);
		return 0;
	}

	for (i = 0; i < lrmi_mem_segs; i++) {
		if (lrmi_mem[i].iomem || !lrmi_mem[i].size)
			continue;

		lrmi_mem[i].virt = kzalloc(lrmi_mem[i].size, GFP_KERNEL);
		if (!lrmi_mem[i].virt) {
			lrmi_pr_err("Failed to alloc %s\n", lrmi_mem[i].name);
			mutex_unlock(&lrmi_real_mem_mlock);
			lrmi_real_mem_unmap();
			return -ENOMEM;
		}
	}

	lrmi_real_mem.ready = 1;
	lrmi_real_mem.count = 1;
	lrmi_real_mem.blocks[0].size = lrmi_mem[LRMI_RAM].size;
	lrmi_real_mem.blocks[0].free = 1;
	mutex_unlock(&lrmi_real_mem_mlock);
	return 0;
}

static __always_inline void lrmi_real_mem_insert_block(int i)
{
	memmove(lrmi_real_mem.blocks + i + 1, lrmi_real_mem.blocks + i,
	       (lrmi_real_mem.count - i) * sizeof(struct lrmi_mem_block));
	lrmi_real_mem.count++;
}

static __always_inline void lrmi_real_mem_delete_block(int i)
{
	lrmi_real_mem.count--;
	memmove(lrmi_real_mem.blocks + i, lrmi_real_mem.blocks + i + 1,
	       (lrmi_real_mem.count - i) * sizeof(struct lrmi_mem_block));
}

u32 lrmi_alloc(size_t size)
{
	int i;
	u32 phys;

	mutex_lock(&lrmi_real_mem_mlock);
	if (!lrmi_real_mem.ready) {
		mutex_unlock(&lrmi_real_mem_mlock);
		return 0;
	}

	if (lrmi_real_mem.count >= LRMI_MEM_BLOCKS) {
		mutex_unlock(&lrmi_real_mem_mlock);
		return 0;
	}

	size = round_up(size, SZ_16);
	phys = lrmi_mem[LRMI_RAM].phys;

	for (i = 0; i < lrmi_real_mem.count; i++) {
		if (lrmi_real_mem.blocks[i].free && size < lrmi_real_mem.blocks[i].size) {
			lrmi_real_mem_insert_block(i);
			lrmi_real_mem.blocks[i].size = size;
			lrmi_real_mem.blocks[i].free = 0;
			lrmi_real_mem.blocks[i + 1].size -= size;
			mutex_unlock(&lrmi_real_mem_mlock);
			return phys;
		}
		phys += lrmi_real_mem.blocks[i].size;
	}
	mutex_unlock(&lrmi_real_mem_mlock);
	return 0;
}

void lrmi_free(u32 addr)
{
	int i;
	u32 phys;

	mutex_lock(&lrmi_real_mem_mlock);
	if (!lrmi_real_mem.ready) {
		mutex_unlock(&lrmi_real_mem_mlock);
		return;
	}

	i = 0;
	phys = lrmi_mem[LRMI_RAM].phys;

	while (addr != phys) {
		phys += lrmi_real_mem.blocks[i].size;
		i++;
		if (i == lrmi_real_mem.count) {
			mutex_unlock(&lrmi_real_mem_mlock);
			return;
		}
	}

	lrmi_real_mem.blocks[i].free = 1;

	if (i + 1 < lrmi_real_mem.count && lrmi_real_mem.blocks[i + 1].free) {
		lrmi_real_mem.blocks[i].size += lrmi_real_mem.blocks[i + 1].size;
		lrmi_real_mem_delete_block(i + 1);
	}
	if (i - 1 >= 0 && lrmi_real_mem.blocks[i - 1].free) {
		lrmi_real_mem.blocks[i - 1].size += lrmi_real_mem.blocks[i].size;
		lrmi_real_mem_delete_block(i);
	}
	mutex_unlock(&lrmi_real_mem_mlock);
}

static int lrmi_mem_map(void)
{
	u32 base_mem_size, upper_mem_size;
	int i, ret;
#ifdef LRMI_DEBUG
	u32 pos;
#endif

	/* Base memory before EBDA */
	base_mem_size = (u32)(*(u16 *)phys_to_virt(0x413)) * SZ_1K;

	/* Setup EBDA */
	lrmi_mem[LRMI_EBDA_HI].phys = (u32)(*(u16 *)phys_to_virt(0x40e)) << 4;
	if (lrmi_mem[LRMI_EBDA_HI].phys < 0x80000 || lrmi_mem[LRMI_EBDA_HI].phys > 0x9fc00) {
		lrmi_pr_warn("Wrong EBDA address 0x%05x, using default address\n",
			     lrmi_mem[LRMI_EBDA_HI].phys);
		lrmi_mem[LRMI_EBDA_HI].phys = 0x9f000;
		lrmi_mem[LRMI_EBDA_HI].size = SZ_4K;
	} else {
		/* The first byte in the EBDA is its size in KiB */
		lrmi_mem[LRMI_EBDA_HI].size =
			(u32)(*(u8 *)phys_to_virt(lrmi_mem[LRMI_EBDA_HI].phys)) * SZ_1K;
	}

	if ((base_mem_size >= 0x80000) && (base_mem_size < lrmi_mem[LRMI_EBDA_HI].phys)) {
		u32 ebda_reloc = lrmi_mem[LRMI_EBDA_HI].phys - base_mem_size;

		lrmi_mem[LRMI_EBDA_HI].phys -= ebda_reloc;
		lrmi_mem[LRMI_EBDA_HI].size += ebda_reloc;
	}

	if (!lrmi_mem[LRMI_EBDA_HI].size || (lrmi_mem[LRMI_EBDA_HI].phys +
	     lrmi_mem[LRMI_EBDA_HI].size > lrmi_mem[LRMI_VRAM].phys)) {
		lrmi_pr_warn("Wrong EBDA size %u, fixing\n", lrmi_mem[LRMI_EBDA_HI].size);
		lrmi_mem[LRMI_EBDA_HI].size = lrmi_mem[LRMI_VRAM].phys - lrmi_mem[LRMI_EBDA_HI].phys;
	}

	lrmi_mem[LRMI_EBDA_HI].offset = lrmi_mem[LRMI_EBDA_HI].phys -
					(lrmi_mem[LRMI_EBDA_HI].phys & PAGE_MASK);

	lrmi_mem[LRMI_EBDA_LO].size = lrmi_mem[LRMI_EBDA_HI].phys - lrmi_mem[LRMI_EBDA_HI].offset -
				      lrmi_mem[LRMI_EBDA_LO].phys;

#if X86EMU_CPU < X86EMU_286
	lrmi_mem[LRMI_MEM_HI].size = 0;
#endif

	ret = lrmi_real_mem_map();
	if (ret)
		return ret;

	/* Find the Video BIOS size */
	if ((*(u16 *)phys_to_virt(lrmi_mem[LRMI_VBIOS].phys)) != 0xaa55) {
		lrmi_pr_err("VBIOS ROM not found\n");
		lrmi_exit();
		return -ENODEV;
	}

	lrmi_mem[LRMI_VBIOS].size =
		(u32)(*(u8 *)phys_to_virt(lrmi_mem[LRMI_VBIOS].phys + 2)) * SZ_512;
	if (lrmi_mem[LRMI_VBIOS].size < SZ_4K) {
		lrmi_pr_err("Wrong VBIOS ROM size %u\n", lrmi_mem[LRMI_VBIOS].size);
		lrmi_exit();
		return -ENXIO;
	}

	lrmi_mem[LRMI_VBIOS].size = round_up(lrmi_mem[LRMI_VBIOS].size, SZ_32K);
	upper_mem_size = lrmi_mem[LRMI_SBIOS].phys -
			(lrmi_mem[LRMI_VBIOS].phys + lrmi_mem[LRMI_VBIOS].size);
	lrmi_mem[LRMI_SBIOS].phys -= upper_mem_size;
	lrmi_mem[LRMI_SBIOS].size += upper_mem_size;

	for (i = 0; i < lrmi_mem_segs; i++) {
		if (!lrmi_mem[i].iomem || !lrmi_mem[i].size)
			continue;

		lrmi_mem[i].virt = ioremap(lrmi_mem[i].phys - lrmi_mem[i].offset,
					   lrmi_mem[i].size + lrmi_mem[i].offset);
		if (!lrmi_mem[i].virt) {
			lrmi_pr_err("Failed to map %s\n", lrmi_mem[i].name);
			if (i == LRMI_EBDA_HI)
				continue;
			lrmi_exit();
			return -ENOMEM;
		}
	}

#ifdef LRMI_DEBUG
	lrmi_pr_info("LRMI memory:\n");
	for (pos = 0; pos < lrmi_mem_segs; pos++) {
		for (i = 0; i < lrmi_mem_segs; i++) {
			if (lrmi_mem[i].pos == pos && lrmi_mem[i].virt)
				lrmi_pr_info(" %s:  [%06x-%06x] -> [%px-%px] %3u %s\n",
					lrmi_mem[i].name, lrmi_mem[i].phys,
					lrmi_mem[i].phys + lrmi_mem[i].size - 1,
					lrmi_phys_to_virt(lrmi_mem[i].phys),
					lrmi_phys_to_virt(lrmi_mem[i].phys + lrmi_mem[i].size - 1),
					(lrmi_mem[i].size < SZ_8K) ? lrmi_mem[i].size : (lrmi_mem[i].size / SZ_1K),
					(lrmi_mem[i].size < SZ_8K) ? "B" : "KiB");
		}
	}
#endif
	return 0;
}

u16 lrmi_int_seg(u8 intno)
{
	return lrmi_readw((u32)intno * 4 + 2);
}

u16 lrmi_int_off(u8 intno)
{
	return lrmi_readw((u32)intno * 4);
}

u8 lrmi_inb(u16 port)
{
	u8 val = inb(port);

#ifdef LRMI_DEBUG
	lrmi_pr_info("inb 0x%02x <- 0x%04x\n", val, port);
#endif
	return val;
}

u16 lrmi_inw(u16 port)
{
	u16 val = inw(port);

#ifdef LRMI_DEBUG
	lrmi_pr_info("inw 0x%04x <- 0x%04x\n", val, port);
#endif
	return val;
}

u32 lrmi_inl(u16 port)
{
	u32 val = inl(port);

#ifdef LRMI_DEBUG
	lrmi_pr_info("inl 0x%08x <- 0x%04x\n", val, port);
#endif
	return val;
}

void lrmi_outb(u16 port, u8 val)
{
#ifdef LRMI_DEBUG
	lrmi_pr_info("outb 0x%02x -> 0x%04x\n", val, port);
#endif
	outb(val, port);
}

void lrmi_outw(u16 port, u16 val)
{
#ifdef LRMI_DEBUG
	lrmi_pr_info("outw 0x%04x -> 0x%04x\n", val, port);
#endif
	outw(val, port);
}

void lrmi_outl(u16 port, u32 val)
{
#ifdef LRMI_DEBUG
	lrmi_pr_info("outl 0x%08x -> 0x%04x\n", val, port);
#endif
	outl(val, port);
}

int lrmi_init(void)
{
	int ret;

	lrmi_x86emu = kzalloc(sizeof(*lrmi_x86emu), GFP_KERNEL);
	if (!lrmi_x86emu)
		return -ENOMEM;

	/* x86 memory R/W not emulated */
	lrmi_x86emu->x86_readb  = lrmi_readb;
	lrmi_x86emu->x86_readw  = lrmi_readw;
	lrmi_x86emu->x86_readl  = lrmi_readl;
	lrmi_x86emu->x86_writeb = lrmi_writeb;
	lrmi_x86emu->x86_writew = lrmi_writew;
	lrmi_x86emu->x86_writel = lrmi_writel;

	/* x86 port I/O not emulated */
	lrmi_x86emu->x86_inb  = lrmi_inb;
	lrmi_x86emu->x86_inw  = lrmi_inw;
	lrmi_x86emu->x86_inl  = lrmi_inl;
	lrmi_x86emu->x86_outb = lrmi_outb;
	lrmi_x86emu->x86_outw = lrmi_outw;
	lrmi_x86emu->x86_outl = lrmi_outl;

	ret = lrmi_mem_map();
	if (ret)
		return ret;

	/* Allocate stack */
	lrmi_stack = lrmi_alloc(LRMI_STACK_SIZE);
	if (!lrmi_stack) {
		lrmi_exit();
		return -ENOMEM;
	}
	lrmi_x86emu->x86.R_SS = lrmi_phys_to_seg(lrmi_stack);
	memset(lrmi_phys_to_virt(lrmi_stack), 0, LRMI_STACK_SIZE);
	return 0;
}

static void lrmi_x86emu_push_word(struct x86emu *emu, u16 val)
{
	u16 new_sp = READ_ONCE(emu->x86.R_SP) - 2;

#if X86EMU_CPU < X86EMU_286
	WRITE_ONCE(emu->x86.R_SP, new_sp);
	(*emu->x86_writew)(((u32)emu->x86.R_SS << 4) + new_sp, val);
#else
	(*emu->x86_writew)(((u32)emu->x86.R_SS << 4) + new_sp, val);
	WRITE_ONCE(emu->x86.R_SP, new_sp);
#endif
}

static void lrmi_x86emu_call(struct lrmi_regs *regs)
{
	lrmi_x86emu->x86.R_EAX = regs->eax;
	lrmi_x86emu->x86.R_EBX = regs->ebx;
	lrmi_x86emu->x86.R_ECX = regs->ecx;
	lrmi_x86emu->x86.R_EDX = regs->edx;
	lrmi_x86emu->x86.R_ESI = regs->esi;
	lrmi_x86emu->x86.R_EDI = regs->edi;
	lrmi_x86emu->x86.R_ES = regs->es;
	lrmi_x86emu->x86.R_FS = regs->fs;
	lrmi_x86emu->x86.R_GS = regs->gs;
	lrmi_x86emu->x86.R_CS = regs->cs;
	lrmi_x86emu->x86.R_SS = regs->ss ? regs->ss : lrmi_phys_to_seg(lrmi_stack);
	lrmi_x86emu->x86.R_DS = regs->ds ? regs->ds : lrmi_x86emu->x86.R_DS;
	lrmi_x86emu->x86.R_EIP = regs->ip;
	WRITE_ONCE(lrmi_x86emu->x86.R_ESP, regs->sp);
	WRITE_ONCE(lrmi_x86emu->x86.R_EBP, regs->ebp);

	memset(lrmi_phys_to_virt(lrmi_stack), 0, LRMI_STACK_SIZE);

	x86emu_exec(lrmi_x86emu);

	regs->eax = lrmi_x86emu->x86.R_EAX;
	regs->ebx = lrmi_x86emu->x86.R_EBX;
	regs->ecx = lrmi_x86emu->x86.R_ECX;
	regs->edx = lrmi_x86emu->x86.R_EDX;
	regs->esi = lrmi_x86emu->x86.R_ESI;
	regs->edi = lrmi_x86emu->x86.R_EDI;
	regs->es = lrmi_x86emu->x86.R_ES;
	regs->flags = lrmi_x86emu->x86.R_FLG;
}

void lrmi_int(struct lrmi_regs *regs, u8 intno)
{
	mutex_lock(&lrmi_x86emu_mlock);
	lrmi_x86emu->x86.R_EFLG = GET_PPF_FLAGS(regs->flags);
	lrmi_x86emu_push_word(lrmi_x86emu, lrmi_x86emu->x86.R_FLG);
	lrmi_x86emu->x86.R_EFLG &= ~(F_TF | F_IF);
	lrmi_x86emu_push_word(lrmi_x86emu, 0);
	lrmi_x86emu_push_word(lrmi_x86emu, 0);
	regs->cs = lrmi_int_seg(intno);
	regs->ip = lrmi_int_off(intno);
	lrmi_x86emu->x86.intr = 0;
	lrmi_x86emu_call(regs);
	mutex_unlock(&lrmi_x86emu_mlock);
}

void lrmi_call(struct lrmi_regs *regs)
{
	mutex_lock(&lrmi_x86emu_mlock);
	lrmi_x86emu->x86.R_EFLG = GET_PPF_FLAGS(regs->flags);
	lrmi_x86emu_push_word(lrmi_x86emu, 0);
	lrmi_x86emu_push_word(lrmi_x86emu, 0);
	lrmi_x86emu_call(regs);
	mutex_unlock(&lrmi_x86emu_mlock);
}

void lrmi_exit(void)
{
	int i;

	for (i = 0; i < lrmi_mem_segs; i++) {
		if (!lrmi_mem[i].iomem || !lrmi_mem[i].virt)
			continue;

		iounmap(lrmi_mem[i].virt);
		lrmi_mem[i].virt = NULL;
	}
	lrmi_real_mem_unmap();
	lrmi_stack = 0;
	kfree(lrmi_x86emu);
	lrmi_x86emu = NULL;
}
