/* SPDX-License-Identifier: HPND-sell-variant */

/*
 * Realmode X86 Emulator Library
 *
 * Copyright (C) 1996-1999 SciTech Software, Inc.
 * Copyright (C) David Mosberger-Tang
 * Copyright (C) 1999 Egbert Eich
 * Copyright (C) 2007 Joerg Sonnenberger
 * Copyright (C) 2024 Jorge Maidana
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies and that
 * both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of the authors not be used
 * in advertising or publicity pertaining to distribution of the software
 * without specific, written prior permission. The authors make no
 * representations about the suitability of this software for any purpose.
 * It is provided "as is" without express or implied warranty.
 *
 * THE AUTHORS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL THE AUTHORS BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef X86EMU_H
#define X86EMU_H

/*
 * General EAX, EBX, ECX, EDX type registers.  Note that for
 * portability, and speed, the issue of byte swapping is not addressed
 * in the registers.  All registers are stored in the default format
 * available on the host machine.  The only critical issue is that the
 * registers should line up EXACTLY in the same manner as they do in
 * the 386.  That is:
 *
 * EAX & 0x00ff == AL
 * EAX & 0xff00 == AH
 * EAX & 0xffff == AX
 */

#ifdef	__BIG_ENDIAN__

struct x86emu_register32 {
	u32 e_reg;
};

struct x86emu_register16 {
	u16 __pad16;
	u16 x_reg;
};

struct x86emu_register8 {
	u8 __pad8[2];
	u8 h_reg, l_reg;
};

#else /* !__BIG_ENDIAN__ */

struct x86emu_register32 {
	u32 e_reg;
};

struct x86emu_register16 {
	u16 x_reg;
};

struct x86emu_register8 {
	u8 l_reg, h_reg;
};

#endif /* BIG_ENDIAN */

union x86emu_register {
	struct x86emu_register32 I32_reg;
	struct x86emu_register16 I16_reg;
	struct x86emu_register8 I8_reg;
};

struct x86emu_regs {
	u16 register_cs;
	u16 register_ds;
	u16 register_es;
	u16 register_fs;
	u16 register_gs;
	u16 register_ss;
	union x86emu_register register_flags;
	union x86emu_register register_a;
	union x86emu_register register_b;
	union x86emu_register register_c;
	union x86emu_register register_d;
	union x86emu_register register_sp;
	union x86emu_register register_bp;
	union x86emu_register register_si;
	union x86emu_register register_di;
	union x86emu_register register_ip;
	u32 mode;
	u32 intr; /* mask of pending interrupts */
	u8 intno;
	u8 __pad[3];
};
static_assert(sizeof(struct x86emu_regs) == 64, "Size of struct x86emu_regs must be 64");

struct x86emu {
	struct x86emu_regs x86;
	u64 tsc;
	u32 cur_offset;
	u32 cur_mod:2;
	u32 cur_rl:3;
	u32 cur_rh:3;
	u32 x86_addr:24;
	u8 (*x86_readb)(u32 addr);
	u16 (*x86_readw)(u32 addr);
	u32 (*x86_readl)(u32 addr);
	void (*x86_writeb)(u32 addr, u8 val);
	void (*x86_writew)(u32 addr, u16 val);
	void (*x86_writel)(u32 addr, u32 val);
	u8 (*x86_inb)(u16 port);
	u16 (*x86_inw)(u16 port);
	u32 (*x86_inl)(u16 port);
	void (*x86_outb)(u16 port, u8 val);
	void (*x86_outw)(u16 port, u16 val);
	void (*x86_outl)(u16 port, u32 val);
	void (*x86_intr[256])(struct x86emu *emu, u8 intno);
};

void x86emu_exec(struct x86emu *emu);
void x86emu_halt_sys(struct x86emu *emu);

/* Emulated CPU Type */
#define X86EMU_086			0
#define X86EMU_186			1
#define X86EMU_286			2 /* Real Mode */
#define X86EMU_386			3 /* Real Mode */
#define X86EMU_CPU			X86EMU_386

/* 8-bit registers */
#define R_AH				register_a.I8_reg.h_reg
#define R_AL				register_a.I8_reg.l_reg
#define R_BH				register_b.I8_reg.h_reg
#define R_BL				register_b.I8_reg.l_reg
#define R_CH				register_c.I8_reg.h_reg
#define R_CL				register_c.I8_reg.l_reg
#define R_DH				register_d.I8_reg.h_reg
#define R_DL				register_d.I8_reg.l_reg

/* Artificial 8-bit registers */
#define R_PSBH				register_flags.I8_reg.h_reg
#define R_PSBL				register_flags.I8_reg.l_reg

/* 16-bit registers */
#define R_AX				register_a.I16_reg.x_reg
#define R_BX				register_b.I16_reg.x_reg
#define R_CX				register_c.I16_reg.x_reg
#define R_DX				register_d.I16_reg.x_reg

/* Special 16-bit registers */
#define R_SP				register_sp.I16_reg.x_reg
#define R_BP				register_bp.I16_reg.x_reg
#define R_SI				register_si.I16_reg.x_reg
#define R_DI				register_di.I16_reg.x_reg
#define R_IP				register_ip.I16_reg.x_reg
#define R_FLG				register_flags.I16_reg.x_reg

/* 32-bit registers */
#define R_EAX				register_a.I32_reg.e_reg
#define R_EBX				register_b.I32_reg.e_reg
#define R_ECX				register_c.I32_reg.e_reg
#define R_EDX				register_d.I32_reg.e_reg

/* Special 32-bit registers */
#define R_ESP				register_sp.I32_reg.e_reg
#define R_EBP				register_bp.I32_reg.e_reg
#define R_ESI				register_si.I32_reg.e_reg
#define R_EDI				register_di.I32_reg.e_reg
#define R_EIP				register_ip.I32_reg.e_reg
#define R_EFLG				register_flags.I32_reg.e_reg

/* Segment registers */
#define R_CS				register_cs
#define R_DS				register_ds
#define R_SS				register_ss
#define R_ES				register_es
#define R_FS				register_fs
#define R_GS				register_gs

/* Following bits masked in to a 16-bit quantity */
#define F_CF				BIT(0)  /* Carry flag */
#define F_B1				BIT(1)  /* Reserved - always 1 */
#define F_PF				BIT(2)  /* Parity flag */
#define F_B3				BIT(3)  /* Reserved - always 0 */
#define F_AF				BIT(4)  /* Auxiliary Carry flag */
#define F_B5				BIT(5)  /* Reserved 0 */
#define F_ZF				BIT(6)  /* Zero flag */
#define F_SF				BIT(7)  /* Sign flag */
#define F_TF				BIT(8)  /* Trap flag */
#define F_IF				BIT(9)  /* Interrupt Enable flag */
#define F_DF				BIT(10) /* Direction flag */
#define F_OF				BIT(11) /* Overflow flag */
#define F_IOPL				(BIT(12) | BIT(13)) /* I/O PL flag 286-PM+, always 3 on < 286, always 0 on 286-RM */
#define F_NT				BIT(14) /* Nested Task flag 286-PM+, always 1 on < 286, always 0 on 286-RM */
#define F_B15				BIT(15) /* Reserved - always 1 on < 286, always 0 on 286+ */

/* lahf/sahf flags */
#define LSF_FLAGS			(F_CF | F_PF | F_AF | F_ZF | F_SF)
#define GET_LSF_FLAGS(reg)		(((reg) & LSF_FLAGS) | F_B1) /* Bit 1 always set */

/* pushf/popf flags */
#if X86EMU_CPU < X86EMU_386
#define PPF_FLAGS			(F_CF | F_PF | F_AF | F_ZF | F_SF | \
					 F_TF | F_IF | F_DF | F_OF)
#else
#define PPF_FLAGS			(F_CF | F_PF | F_AF | F_ZF | F_SF | \
					 F_TF | F_IF | F_DF | F_OF | F_IOPL | F_NT)
#endif

#if X86EMU_CPU < X86EMU_286
#define GET_PPF_FLAGS(reg)		(((reg) & PPF_FLAGS) | F_B1 | \
					 BIT(12) | BIT(13) | BIT(14) | F_B15) /* Bits 1, 12-15 always set */
#else
#define GET_PPF_FLAGS(reg)		(((reg) & PPF_FLAGS) | F_B1) /* Bit 1 always set */
#endif

#define SET_FLAG(flag)			(emu->x86.R_EFLG |= (flag))
#define CLEAR_FLAG(flag)		(emu->x86.R_EFLG &= ~(flag))
#define ACCESS_FLAG(flag)		(emu->x86.R_EFLG & (flag))

#define X86EMU_SET_FLAG_COND(cond, flags) \
	do { if (cond) SET_FLAG(flags); else CLEAR_FLAG(flags); } while (0)

/* Bool */
#define CF_SET(val)			((val) != 0)
#define ZF_SET(val)			((val) == 0)
#define OF_SET(val)			((val) != 0)
#define CF_CLEAR			(false)
#define AF_CLEAR			(false)
#define SF_CLEAR			(false)
#define OF_CLEAR			(0)
#define OF_BOOL				(true)
#define OF_MSB2				(false)

#define CF_STATUS			(ACCESS_FLAG(F_CF) ? 1 : 0)
#define PF_STATUS			(ACCESS_FLAG(F_PF) ? 1 : 0)
#define AF_STATUS			(ACCESS_FLAG(F_AF) ? 1 : 0)
#define ZF_STATUS			(ACCESS_FLAG(F_ZF) ? 1 : 0)
#define SF_STATUS			(ACCESS_FLAG(F_SF) ? 1 : 0)
#define OF_STATUS			(ACCESS_FLAG(F_OF) ? 1 : 0)

#define LSB_BIT(val)			((val) & 1)

#define MSB_NIBBLE(val)			(((val) & 0x8) != 0)
#define MSB_BYTE(val)			(((val) & 0x80) != 0)
#define MSB_WORD(val)			(((val) & 0x8000) != 0)
#define MSB_LONG(val)			(((val) & 0x80000000) != 0)

/* Upper 2-bit */
#define MSB2_BYTE(val)			((val) >> 6)
#define MSB2_WORD(val)			((val) >> 14)
#define MSB2_LONG(val)			((val) >> 30)

/* add/adc carry */
#define CFA_BYTE(val)			(((val) & 0x100) != 0)
#define CFA_WORD(val)			(((val) & 0x10000) != 0)
#define CFA_LONG(val)			(((val) & 0x100000000) != 0)

/* Low/Hi masks */
#define LOW_BYTE(val)			((u8)((val) & 0xff))
#define LOW_WORD(val)			((u16)((val) & 0xffff))
#define LOW_LONG(val)			((u32)((val) & 0xffffffff))
#define HIGH_BYTE(val)			((u8)(((val) >> 8) & 0xff))
#define HIGH_WORD(val)			((u16)(((val) >> 16) & 0xffff))
#define HIGH_LONG(val)			((u32)(((val) >> 32) & 0xffffffff))

/* Emulator machine state, segment usage control */
#define SYSMODE_SEG_DS_SS		BIT(0)  /* No segment override */
#define SYSMODE_SEGOVR_CS		BIT(1)  /* Segment override */
#define SYSMODE_SEGOVR_DS		BIT(2)  /* Segment override */
#define SYSMODE_SEGOVR_ES		BIT(3)  /* Segment override */
#define SYSMODE_SEGOVR_FS		BIT(4)  /* Segment override */
#define SYSMODE_SEGOVR_GS		BIT(5)  /* Segment override */
#define SYSMODE_SEGOVR_SS		BIT(6)  /* Segment override */
#define SYSMODE_PREFIX_REPE		BIT(7)  /* REPE */
#define SYSMODE_PREFIX_REPNE		BIT(8)  /* REPNE */
#define SYSMODE_PREFIX_DATA32		BIT(9)  /* 32-bit data */
#define SYSMODE_PREFIX_ADDR32		BIT(10) /* 32-bit addressing */

#define SYSMODE_SEG_MODE		(SYSMODE_SEG_DS_SS | SYSMODE_SEGOVR_CS | \
					 SYSMODE_SEGOVR_DS | SYSMODE_SEGOVR_ES | \
					 SYSMODE_SEGOVR_FS | SYSMODE_SEGOVR_GS | \
					 SYSMODE_SEGOVR_SS)
#define SYSMODE_32BIT_MODE		(SYSMODE_PREFIX_DATA32 | SYSMODE_PREFIX_ADDR32)
#define SYSMODE_CLEAR_MODE		(SYSMODE_SEG_MODE | SYSMODE_32BIT_MODE)

#define MODE_ADDR32			(emu->x86.mode & SYSMODE_PREFIX_ADDR32)
#define MODE_DATA32			(emu->x86.mode & SYSMODE_PREFIX_DATA32)
#define MODE_REPE			(emu->x86.mode & SYSMODE_PREFIX_REPE)
#define MODE_REPNE			(emu->x86.mode & SYSMODE_PREFIX_REPNE)
#define MODE_REP			(emu->x86.mode & (SYSMODE_PREFIX_REPE | SYSMODE_PREFIX_REPNE))
#define MODE_SEG			(emu->x86.mode & SYSMODE_SEG_MODE)
#define CLEAR_MODE_REPE			(emu->x86.mode &= ~SYSMODE_PREFIX_REPE)
#define CLEAR_MODE_REPNE		(emu->x86.mode &= ~SYSMODE_PREFIX_REPNE)
#define CLEAR_MODE_REP			(emu->x86.mode &= ~(SYSMODE_PREFIX_REPE | SYSMODE_PREFIX_REPNE))
#define CLEAR_MODE			(emu->x86.mode &= ~SYSMODE_CLEAR_MODE)
#define SET_MODE_REPE			(emu->x86.mode |= SYSMODE_PREFIX_REPE)
#define SET_MODE_REPNE			(emu->x86.mode |= SYSMODE_PREFIX_REPNE)

/* Interrupts */
#define INTR_SYNCH			BIT(0)
#define INTR_HALT			BIT(2)
#define INTR_MODES			(INTR_SYNCH | INTR_HALT)

#define INTR_MODE_SET			(emu->x86.intr & INTR_MODES)
#define INTR_MODE_CLEAR			(emu->x86.intr &= ~INTR_MODES)
#define INTR_MODE_SYNCH			(emu->x86.intr & INTR_SYNCH)
#define INTR_MODE_HALT			(emu->x86.intr & INTR_HALT)
#define INTR_SET_SYNCH			(emu->x86.intr |= INTR_SYNCH)
#define INTR_CLEAR_SYNCH		(emu->x86.intr &= ~INTR_SYNCH)
#define INTR_SET_HALT			(emu->x86.intr |= INTR_HALT)
#define INTR_CLEAR_HALT			(emu->x86.intr &= ~INTR_HALT)

#define INT0_DE_FAULT			0 /* #DE Divide Error */
#define INT1_DB_TRAP			1 /* #DB Debug */
#define INT2_NMI			2 /* Non-Maskable External Interrupt */
#define INT3_BP_TRAP			3 /* #BP Breakpoint */
#define INT4_OF_TRAP			4 /* #OF Overflow */
#define INT5_BR_FAULT			5 /* #BR Bound Range Exceeded */
#define INT6_UD_FAULT			6 /* #UD Undefined Opcode */
#define INT7_NM_FAULT			7 /* #NM No Math Coprocessor Available */

#define X86EMU_DIV_ERR(cond)		do { if (cond) { x86emu_intr_raise(emu, INT0_DE_FAULT); return; } } while (0)

#endif /* X86EMU_H */
