// SPDX-License-Identifier: GPL-2.0-or-later

#include <linux/delay.h>
#include <linux/firmware.h>
#include <linux/math64.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <linux/version.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(6, 0, 0)
#include <linux/aperture.h>
#endif
#include <linux/vgaarb.h>

#include <drm/drm_atomic_helper.h>
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(6, 13, 0) &&		\
	LINUX_VERSION_CODE < KERNEL_VERSION(6, 14, 0))
#include <drm/drm_client_setup.h>
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(6, 14, 0)
#include <drm/clients/drm_client_setup.h>
#endif
#include <drm/drm_drv.h>
#include <drm/drm_edid.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(6, 2, 0)
#include <drm/drm_fb_helper.h>
#elif (LINUX_VERSION_CODE >= KERNEL_VERSION(6, 2, 0) &&		\
	LINUX_VERSION_CODE < KERNEL_VERSION(6, 11, 0))
#include <drm/drm_fbdev_generic.h>
#else
#include <drm/drm_fbdev_ttm.h>
#endif
#include <drm/drm_fourcc.h>
#include <drm/drm_framebuffer.h>
#include <drm/drm_gem_framebuffer_helper.h>
#include <drm/drm_gem_vram_helper.h>
#include <drm/drm_managed.h>
#include <drm/drm_probe_helper.h>
#include <drm/drm_simple_kms_helper.h>

#include "lrmi.h"

#define VBEDRM_DRIVER_DESC		"VBE Core 2.0+ DRM Driver"
#define VBEDRM_DRIVER_NAME		"vbe-drm"
#define VBEDRM_DRIVER_MAJOR		4
#define VBEDRM_DRIVER_MINOR		16
#define VBEDRM_DRIVER_PATCHLEVEL	0
#if LINUX_VERSION_CODE < KERNEL_VERSION(6, 11, 0)
#define VBEDRM_DRIVER_DATE		"20241210"
#endif

MODULE_AUTHOR("Jorge Maidana <jorgem.linux@gmail.com>");
MODULE_DESCRIPTION(VBEDRM_DRIVER_DESC);
MODULE_VERSION(__stringify(VBEDRM_DRIVER_MAJOR) "."
	       __stringify(VBEDRM_DRIVER_MINOR) "."
	       __stringify(VBEDRM_DRIVER_PATCHLEVEL));
MODULE_LICENSE("GPL");

static int vbe_drm_modeset = -1;
static int xres = 1024;
static int yres = 768;
static int bpp = 32;
static int boot;
static uint base;
static uint vram_mb;
static bool setfps;
static bool novsync;
static bool noedid;
static bool nodpms;
static bool nosave;
static bool nowc;

module_param_named(modeset, vbe_drm_modeset, int, 0444);
module_param(xres, int, 0444);
module_param(yres, int, 0444);
module_param(bpp, int, 0444);
module_param(boot, int, 0444);
module_param(base, uint, 0444);
module_param(vram_mb, uint, 0444);
module_param(setfps, bool, 0644);
module_param(novsync, bool, 0644);
module_param(noedid, bool, 0444);
module_param(nodpms, bool, 0444);
module_param(nosave, bool, 0444);
module_param(nowc, bool, 0444);

MODULE_PARM_DESC(modeset, "enable/disable kernel modesetting");
MODULE_PARM_DESC(xres, "default x resolution");
MODULE_PARM_DESC(yres, "default y resolution");
MODULE_PARM_DESC(bpp, "default bpp");
MODULE_PARM_DESC(boot, "0 = no boot, 1 = boot main vbios, 2 = boot vbios_VENDOR_DEVICE.bin");
MODULE_PARM_DESC(base, "linear framebuffer address");
MODULE_PARM_DESC(vram_mb, "video memory in MiB");
MODULE_PARM_DESC(setfps, "enable refresh rate control");
MODULE_PARM_DESC(novsync, "disable vsync");
MODULE_PARM_DESC(noedid, "disable edid read");
MODULE_PARM_DESC(nodpms, "disable dpms commands");
MODULE_PARM_DESC(nosave, "disable vbe state save");
MODULE_PARM_DESC(nowc, "disable linear framebuffer write combining");

/* ------------------------------------------------------------------ */
/* VBE Definitions                                                    */

#define vbe_pr_debug(fmt, ...)		pr_debug(VBEDRM_DRIVER_NAME ": " fmt, ##__VA_ARGS__)
#define vbe_pr_info(fmt, ...)		pr_info(VBEDRM_DRIVER_NAME ": " fmt, ##__VA_ARGS__)
#define vbe_pr_warn(fmt, ...)		pr_warn(VBEDRM_DRIVER_NAME ": " fmt, ##__VA_ARGS__)
#define vbe_pr_err(fmt, ...)		pr_err(VBEDRM_DRIVER_NAME ": " fmt, ##__VA_ARGS__)

#define vbe_task_failed(r)		((((r)->eax) & 0xffff) != 0x004f)

#define VBE_INT10H			0x10
#define VBE_INT42H			0x42
#define VBE_ROM_C0000			0xc0000

#define VBE_BUF_SAVE			BIT(0)
#define VBE_BUF_INFO			BIT(1)
#define VBE_BUF_ESBX			BIT(2)
#define VBE_BUF_ESDI			BIT(3)

#define VBE_CRTC_FLAG_DBLSCAN		BIT(0)
#define VBE_CRTC_FLAG_INTERLACE		BIT(1)
#define VBE_CRTC_FLAG_NHSYNC		BIT(2)
#define VBE_CRTC_FLAG_NVSYNC		BIT(3)

#define VBE_MODE_ATTR_SUPPORTEDHW	BIT(0)
#define VBE_MODE_ATTR_COLOR		BIT(3)
#define VBE_MODE_ATTR_GRAPHICS		BIT(4)
#define VBE_MODE_ATTR_LFB		BIT(7)
#define VBE_MODE_ATTR_DBLSCAN		BIT(8)
#define VBE_MODE_ATTR_INTERLACE		BIT(9)
#define VBE_MODE_ATTR_MASK		(VBE_MODE_ATTR_SUPPORTEDHW | VBE_MODE_ATTR_COLOR | \
					 VBE_MODE_ATTR_GRAPHICS | VBE_MODE_ATTR_LFB)

#define VBE_MODE_SET_CRTC		BIT(11)
#define VBE_MODE_SET_LFB		BIT(14)
#define VBE_MODE_SET_FB_NOCLEAR		BIT(15)

#define VBE_MODESET_INIT		BIT(0)
#define VBE_MODESET_SET			BIT(1)
#define VBE_MODESET_PM_OFF		BIT(2)

#define VBE_DPMS_MODE_ON		0x0000
#define VBE_DPMS_MODE_OFF		0x0400
#define VBE_DPMS_MODE_MASK		0x0f00

#define VBE_VGA_STATUS			0x3da
#define VBE_VGA_VTRACE			BIT(3)
#define VBE_VGA_VTRACE_ON		1
#define VBE_VGA_VTRACE_OFF		0

#define VBE_MIN_HEIGHT			480
#define VBE_MIN_WIDTH			640
#define VBE_MAX_HEIGHT			4096
#define VBE_MAX_WIDTH			8192
#define VBE_MAX_MODES			4096

#define VBE_VRAM_GRNL			SZ_64K
#define VBE_VRAM_MAX			SZ_256M

/* VBE Info Block */
struct vbe_ib {
	char vbe_signature[4];
	u8  vbe_ver_min;
	u8  vbe_ver_maj;
	u32 oem_string_ptr;
	u32 capabilities;
	u32 mode_list_ptr;
	u16 total_memory;
	u8  oem_sw_rev_min;
	u8  oem_sw_rev_maj;
	u32 oem_vendor_name_ptr;
	u32 oem_product_name_ptr;
	u32 oem_product_rev_ptr;
	u8  reserved[222];
	char oem_data[256];
	char drv_data[512];
} __packed;

/* VBE Mode Info Block */
struct vbe_mode_ib {
	u16 mode_attr;
	u8  winA_attr;
	u8  winB_attr;
	u16 win_granularity;
	u16 win_size;
	u16 winA_seg;
	u16 winB_seg;
	u32 win_func_ptr;
	u16 bytes_per_scan_line;

	/* VBE 1.2+ */
	u16 x_res;
	u16 y_res;
	u8  x_char_size;
	u8  y_char_size;
	u8  planes;
	u8  bits_per_pixel;
	u8  banks;
	u8  memory_model;
	u8  bank_size;
	u8  image_pages;
	u8  page_func;

	/* Direct color fields for direct/6 and YUV/7 memory models. */
	/* Offsets are bit positions of LSB in the mask. */
	u8  red_len;
	u8  red_off;
	u8  green_len;
	u8  green_off;
	u8  blue_len;
	u8  blue_off;
	u8  rsvd_len;
	u8  rsvd_off;
	u8  direct_color_info;	/* Direct color mode attributes */

	/* VBE 2.0+ */
	u32 phys_base_ptr;
	u32 off_screen_mem_off;
	u16 off_screen_mem_len;

	/* VBE 3.0+ */
	u16 lin_bytes_per_scan_line;
	u8  bnk_image_pages;
	u8  lin_image_pages;
	u8  lin_red_len;
	u8  lin_red_off;
	u8  lin_green_len;
	u8  lin_green_off;
	u8  lin_blue_len;
	u8  lin_blue_off;
	u8  lin_rsvd_len;
	u8  lin_rsvd_off;
	u32 max_pixel_clock;
	u16 mode_id;
	u8  reserved[188];
} __packed;

/* VBE CRTC Info Block */
struct vbe_crtc_ib {
	u16 horiz_total;
	u16 horiz_start;
	u16 horiz_end;
	u16 vert_total;
	u16 vert_start;
	u16 vert_end;
	u8  flags;
	u32 pixel_clock;
	u16 refresh_rate;
	u8  reserved[45];
} __packed;

struct vbe_device {
	/* HW */
	void __iomem *lfb_map;
	unsigned long lfb_base;
	unsigned long lfb_size;

	/* Display */
	u16 xres;
	u16 yres;
	u16 bpp;
	u16 modeset;

	/* DRM */
	struct drm_device *dev;
	struct drm_simple_display_pipe pipe;
	struct drm_connector connector;
#if LINUX_VERSION_CODE < KERNEL_VERSION(6, 3, 0)
	struct edid *edid;
#else
	const struct drm_edid *drm_edid;
#endif

	/* VBE */
	struct vbe_ib vbe_info;		/* VBE Info Block */
	struct vbe_mode_ib *vbe_modes;	/* List of supported VBE modes */
	u8 *st_buf;
	u32 st_size;
	int modes;
};

static DEFINE_MUTEX(vbe_task_mlock);
static u32 vbe_vbios_rom = VBE_ROM_C0000;

/* ------------------------------------------------------------------ */
/* VBIOS ROM                                                          */

static bool vbe_vbios_rom_load(struct pci_dev *pdev)
{
	const struct firmware *fw = NULL;
	char vbios_rom_file[32] = { 0 };
	bool rom_load = false;

	if (vbe_vbios_rom != VBE_ROM_C0000)
		return rom_load;

	snprintf(vbios_rom_file, sizeof(vbios_rom_file), "vbios_%04x_%04x.bin",
		 pdev->vendor, pdev->device);
	if (!firmware_request_nowarn(&fw, vbios_rom_file, &pdev->dev)) {
		u32 size = round_up(fw->size, SZ_64K);

		if (size <= SZ_128K) {
			u32 lbuf = lrmi_alloc(size);

			if (lbuf) {
				vbe_vbios_rom = lbuf;
				memcpy(lrmi_phys_to_virt(vbe_vbios_rom), fw->data, fw->size);
				rom_load = true;
				vbe_pr_info("VBIOS ROM loaded at [0x%x-0x%x]\n",
					    vbe_vbios_rom, vbe_vbios_rom + size - 1);
			} else {
				vbe_pr_err("VBIOS ROM load failed!\n");
			}
		}
	}
	release_firmware(fw);
	return rom_load;
}

static bool vbe_vbios_ready(void)
{
	if ((lrmi_int_seg(VBE_INT10H) < lrmi_phys_to_seg(vbe_vbios_rom)) ||
	    (lrmi_int_seg(VBE_INT42H) < lrmi_phys_to_seg(vbe_vbios_rom))) {
		vbe_pr_warn("VBIOS not ready!\n");
		return false;
	}
	return true;
}

static int vbe_vbios_boot(u16 pdev_id)
{
	struct lrmi_regs regs = { 0 };

	if (lrmi_readw(vbe_vbios_rom) != 0xaa55) {
		vbe_pr_err("Invalid VBIOS ROM!\n");
		return -ENODEV;
	}

	regs.cs = lrmi_phys_to_seg(vbe_vbios_rom);
	regs.ip = 0x0003;

	regs.eax = pdev_id ? pdev_id : 0x00ff;
	regs.edx = 0x0080;
	regs.ds = 0x0040;

	lrmi_call(&regs);
	msleep(50);

	if (!vbe_vbios_ready())
		return -ENODEV;
	return 0;
}

/* Restore main VBIOS */
static bool vbe_vbios_rom_unload(void)
{
	if (vbe_vbios_rom == VBE_ROM_C0000)
		return false;
	lrmi_free(vbe_vbios_rom);
	vbe_vbios_rom = VBE_ROM_C0000;
	return true;
}

/* ------------------------------------------------------------------ */
/* VBE Task                                                           */

static int vbe_task_exec(struct lrmi_regs *regs, void *vbe_buf, u32 vbe_buf_size, u32 vbe_buf_flags)
{
	u32 lbuf;

	mutex_lock(&vbe_task_mlock);

	if (vbe_buf_size > SZ_16K) {
		mutex_unlock(&vbe_task_mlock);
		return -ENOMEM;
	}

	if (!vbe_buf_size || !vbe_buf) {
		lrmi_int(regs, VBE_INT10H);
		if (vbe_task_failed(regs)) {
			mutex_unlock(&vbe_task_mlock);
			return -EINVAL;
		}
		mutex_unlock(&vbe_task_mlock);
		return 0;
	}

	lbuf = lrmi_alloc(round_up(vbe_buf_size, SZ_4K));
	if (!lbuf) {
		vbe_pr_err("Memory allocation for a VBE task buffer failed\n");
		mutex_unlock(&vbe_task_mlock);
		return -ENOMEM;
	}

	/* Get the VBE Info Block */
	if (vbe_buf_flags & VBE_BUF_INFO) {
		struct vbe_ib *ib;
		u32 oem_strings[4];
		u32 lphys, bufend;
		u16 *mode;
		u8 *buf = vbe_buf;
		u8 *cbuf;
		int i, fsize;

		regs->es = lrmi_phys_to_seg(lbuf);
		regs->edi = 0x0000;
		memcpy(lrmi_phys_to_virt(lbuf), buf, vbe_buf_size);

		lrmi_int(regs, VBE_INT10H);
		if (vbe_task_failed(regs))
			goto err_free_task_buf;
		memcpy(buf, lrmi_phys_to_virt(lbuf), vbe_buf_size);

		/* The original VBE Info Block is 512 bytes long */
		ib = (struct vbe_ib *)buf;
		bufend = lbuf + sizeof(*ib);
		cbuf = buf + 512;
		fsize = vbe_buf_size - 512;
		lphys = lrmi_far_to_phys(ib->mode_list_ptr);

		/* Mode list is in the buffer, we're good */
		if (lphys >= lbuf && lphys < bufend) {
			vbe_pr_debug("The mode list is in the buffer at 0x%x\n", lphys);
			ib->mode_list_ptr = lphys - lbuf;
			mode = (u16 *)(buf + ib->mode_list_ptr);

			while (fsize > 2 && *mode != 0xffff) {
				mode++;
				cbuf += 2;
				fsize -= 2;
				lphys += 2;
			}
			*mode = 0xffff;
			cbuf += 2;
			fsize -= 2;

		/* Mode list is in the ROM. We copy as much of it as we can to the task buffer */
		} else if (lphys >= vbe_vbios_rom + SZ_4 && lrmi_phys_to_virt(lphys)) {
			u16 tmp;

			vbe_pr_debug("Mode list found at 0x%x\n", lphys);
			mode = (u16 *)cbuf;

			while (fsize > 2 && (tmp = lrmi_readw(lphys)) != 0xffff) {
				*mode = tmp;
				mode++;
				cbuf += 2;
				fsize -= 2;
				lphys += 2;
			}
			ib->mode_list_ptr = 512;
			*mode = 0xffff;
			cbuf += 2;
			fsize -= 2;

		/* Mode list is somewhere else. We're seriously screwed */
		} else {
			vbe_pr_err("Can't retrieve mode list from 0x%x\n", lphys);
			ib->mode_list_ptr = 0;
		}

		oem_strings[0] = ib->oem_string_ptr;
		oem_strings[1] = ib->oem_vendor_name_ptr;
		oem_strings[2] = ib->oem_product_name_ptr;
		oem_strings[3] = ib->oem_product_rev_ptr;

		for (i = 0; i < ARRAY_SIZE(oem_strings); i++) {
			int lsize;

			if (!oem_strings[i])
				continue;

			lphys = lrmi_far_to_phys(oem_strings[i]);
			if (lphys >= lbuf && lphys < bufend) {
				u32 maxlen = bufend - lphys;

				oem_strings[i] = lphys - lbuf;
				lsize = strnlen((char *)buf + oem_strings[i], maxlen);
				if (!lsize || lsize == maxlen) {
					oem_strings[i] = 0;
					continue;
				}
				fsize -= lsize;
				cbuf += lsize;
			} else if (lphys >= vbe_vbios_rom + SZ_4 &&
				   lrmi_phys_to_virt(lphys) && fsize > 0) {
				lsize = strscpy_pad((char *)cbuf,
						    (char *)lrmi_phys_to_virt(lphys), fsize);
				if (lsize == -E2BIG) {
					oem_strings[i] = 0;
					continue;
				}
				oem_strings[i] = vbe_buf_size - fsize;
				lsize++;
				fsize -= lsize;
				cbuf += lsize;
			} else {
				oem_strings[i] = 0;
			}
			if (fsize < 0)
				fsize = 0;
		}
		ib->oem_string_ptr = oem_strings[0];
		ib->oem_vendor_name_ptr = oem_strings[1];
		ib->oem_product_name_ptr = oem_strings[2];
		ib->oem_product_rev_ptr = oem_strings[3];
	} else {
		if (vbe_buf_flags & VBE_BUF_ESDI) {
			regs->es = lrmi_phys_to_seg(lbuf);
			regs->edi = 0x0000;
		} else if (vbe_buf_flags & VBE_BUF_ESBX) {
			regs->es = lrmi_phys_to_seg(lbuf);
			regs->ebx = 0x0000;
		} else {
			vbe_pr_err("Unknown VBE buffer flag\n");
			goto err_free_task_buf;
		}
		memcpy(lrmi_phys_to_virt(lbuf), vbe_buf, vbe_buf_size);

		lrmi_int(regs, VBE_INT10H);
		if (vbe_task_failed(regs))
			goto err_free_task_buf;

		if (vbe_buf_flags & VBE_BUF_SAVE)
			memcpy(vbe_buf, lrmi_phys_to_virt(lbuf), vbe_buf_size);
	}
	lrmi_free(lbuf);
	mutex_unlock(&vbe_task_mlock);
	return 0;

err_free_task_buf:
	lrmi_free(lbuf);
	mutex_unlock(&vbe_task_mlock);
	return -EINVAL;
}

/* ------------------------------------------------------------------ */
/* VBE Functions                                                      */

static int vbe_fb_find_mode(struct vbe_device *vbe, int x_res, int y_res, int bits_per_pixel);

static void vbe_fb_set_text_mode(void)
{
	struct lrmi_regs regs = { 0 };

	regs.eax = 0x0003;
	(void)vbe_task_exec(&regs, NULL, 0, 0);
}

/* Function 00h - Return VBE Controller Information */
static int vbe_f00h_get(struct vbe_device *vbe)
{
	struct lrmi_regs regs = { 0 };
	char vbeinfo[256];
	int ret;

	regs.eax = 0x4f00;

	memcpy(vbe->vbe_info.vbe_signature, "VBE2", 4);

	ret = vbe_task_exec(&regs, &vbe->vbe_info, sizeof(struct vbe_ib), VBE_BUF_INFO);
	if (ret) {
		vbe_pr_err("Get VBE info block error %d\n", ret);
		return ret;
	}

	if (!vbe->vbe_info.mode_list_ptr) {
		vbe_pr_err("Missing mode list!\n");
		return -EINVAL;
	}

	if (vbe->vbe_info.oem_string_ptr)
		snprintf(vbeinfo, sizeof(vbeinfo), "%s, VBE %u.%u",
			 ((char *)&vbe->vbe_info) + vbe->vbe_info.oem_string_ptr,
			 vbe->vbe_info.vbe_ver_maj, vbe->vbe_info.vbe_ver_min);
	else
		snprintf(vbeinfo, sizeof(vbeinfo), "VBE %u.%u",
			 vbe->vbe_info.vbe_ver_maj, vbe->vbe_info.vbe_ver_min);

	/* Print informational messages about the video adapter and the VBE version */
	vbe_pr_info("%s\n", vbeinfo);

	if (vbe->vbe_info.vbe_ver_maj < 2) {
		vbe_pr_err("VBE %u.%u is not supported\n",
			   vbe->vbe_info.vbe_ver_maj, vbe->vbe_info.vbe_ver_min);
		return -EINVAL;
	}
	return 0;
}

/* Function 01h - Return VBE Mode Information */
static int vbe_f01h_get(struct vbe_device *vbe)
{
	struct lrmi_regs regs = { 0 };
	int off = 0, ret;
	u16 *mode;

	/* Count available modes */
	vbe->modes = 0;
	mode = (u16 *)(((u8 *)&vbe->vbe_info) + vbe->vbe_info.mode_list_ptr);
	while (*mode != 0xffff) {
		mode++;
		vbe->modes++;
		if (vbe->modes >= VBE_MAX_MODES)
			break;
	}
	vbe->vbe_modes = kcalloc(vbe->modes, sizeof(struct vbe_mode_ib), GFP_KERNEL);
	if (!vbe->vbe_modes)
		return -ENOMEM;

	/* Get info about all available modes */
	mode = (u16 *)(((u8 *)&vbe->vbe_info) + vbe->vbe_info.mode_list_ptr);
	while (*mode != 0xffff) {
		struct vbe_mode_ib *mib;

		memset(&regs, 0, sizeof(regs));
		regs.eax = 0x4f01;
		regs.ecx = (u32)*mode;

		ret = vbe_task_exec(&regs, vbe->vbe_modes + off, sizeof(struct vbe_mode_ib),
				    VBE_BUF_ESDI | VBE_BUF_SAVE);
		if (ret) {
			vbe_pr_warn("Mode 0x%x info block error %d\n", *mode, ret);
			mode++;
			vbe->modes--;
			continue;
		}

		mib = vbe->vbe_modes + off;
		mib->mode_id = *mode;

		/*
		 * We only want modes that are supported with the current
		 * hardware configuration, color, graphics and that have
		 * support for the LFB.
		 */
		if (((mib->mode_attr & VBE_MODE_ATTR_MASK) == VBE_MODE_ATTR_MASK) &&
		    !(mib->x_res & 7) && !(mib->bits_per_pixel & 7) &&
		     (mib->bits_per_pixel >= 16) && (mib->bits_per_pixel <= 32) &&
		     (mib->mode_id >= 0x100) && (mib->mode_id != 0x81ff))
			off++;
		else
			vbe->modes--;

		mode++;
	}
	if (vbe->modes < 1) {
		vbe_pr_err("VBE compatible mode not found\n");
		return -ENXIO;
	}
	return 0;
}

/* Function 02h - Set VBE Mode */
static int vbe_f02h_set(struct vbe_device *vbe, struct drm_display_mode *mode)
{
	struct lrmi_regs regs = { 0 };
	struct vbe_mode_ib *mib = NULL;
	u32 clock, mode_id;
	int fb_mode, ret;

	fb_mode = vbe_fb_find_mode(vbe, mode->hdisplay, mode->vdisplay, vbe->bpp);
	if (fb_mode < 0)
		return fb_mode;

	mib = &vbe->vbe_modes[fb_mode];
	clock = mode->crtc_clock * 1000;

setmode:
	mode_id = mib->mode_id | VBE_MODE_SET_LFB | VBE_MODE_SET_FB_NOCLEAR;
	regs.eax = 0x4f02;

	if (vbe->vbe_info.vbe_ver_maj >= 3 && setfps && clock && clock <= mib->max_pixel_clock) {
		struct vbe_crtc_ib *crtc = kzalloc(sizeof(struct vbe_crtc_ib), GFP_KERNEL);

		if (!crtc)
			return -ENOMEM;

		crtc->horiz_total = mode->crtc_htotal;
		crtc->horiz_start = mode->crtc_hsync_start;
		crtc->horiz_end = mode->crtc_hsync_end;
		crtc->vert_total = mode->crtc_vtotal;
		crtc->vert_start = mode->crtc_vsync_start;
		crtc->vert_end = mode->crtc_vsync_end;
		crtc->pixel_clock = clock;
		crtc->refresh_rate = (u16)div_u64((u64)clock * 100ULL,
				     mode->crtc_vtotal * mode->crtc_htotal);

		if ((mib->mode_attr & VBE_MODE_ATTR_DBLSCAN) &&
		    (mode->flags & DRM_MODE_FLAG_DBLSCAN))
			crtc->flags |= VBE_CRTC_FLAG_DBLSCAN;
		if ((mib->mode_attr & VBE_MODE_ATTR_INTERLACE) &&
		    (mode->flags & DRM_MODE_FLAG_INTERLACE))
			crtc->flags |= VBE_CRTC_FLAG_INTERLACE;
		if ((mode->flags & DRM_MODE_FLAG_NHSYNC) ||
		   !(mode->flags & DRM_MODE_FLAG_PHSYNC))
			crtc->flags |= VBE_CRTC_FLAG_NHSYNC;
		if ((mode->flags & DRM_MODE_FLAG_NVSYNC) ||
		   !(mode->flags & DRM_MODE_FLAG_PVSYNC))
			crtc->flags |= VBE_CRTC_FLAG_NVSYNC;

		mode_id |= VBE_MODE_SET_CRTC;
		regs.ebx = mode_id;

		ret = vbe_task_exec(&regs, crtc, sizeof(struct vbe_crtc_ib), VBE_BUF_ESDI);
		kfree(crtc);

		if (ret) {
			DRM_WARN("Refresh rate set error %d\n", ret);
			clock = 0;
			memset(&regs, 0, sizeof(regs));
			goto setmode;
		}
	} else {
		mode_id &= ~(VBE_MODE_SET_CRTC);
		regs.ebx = mode_id;

		ret = vbe_task_exec(&regs, NULL, 0, 0);
		if (ret)
			return ret;
	}

	if (!(vbe->modeset & VBE_MODESET_PM_OFF) &&
	    ((vbe->xres != mib->x_res) || (vbe->yres != mib->y_res)))
		memset_io(vbe->lfb_map, 0, vbe->lfb_size);

	vbe->xres = mib->x_res;
	vbe->yres = mib->y_res;
	return 0;
}

/* Function 04h - Save State */
static void vbe_f04h_save(struct vbe_device *vbe)
{
	struct lrmi_regs regs = { 0 };
	int ret;

	if (nosave) {
		vbe->st_size = 0;
		return;
	}

	if (vbe->st_buf)
		return;

	regs.eax = 0x4f04;
	regs.ecx = 0x000f;
	regs.edx = 0x0000;

	ret = vbe_task_exec(&regs, NULL, 0, 0);
	if (ret) {
		vbe->st_size = 0;
		return;
	}

	vbe->st_size = (u32)(regs.ebx & 0xff) * SZ_64;
	if (!vbe->st_size)
		return;

	vbe->st_buf = kzalloc(vbe->st_size, GFP_KERNEL);
	if (!vbe->st_buf) {
		vbe->st_size = 0;
		return;
	}

	memset(&regs, 0, sizeof(regs));
	regs.eax = 0x4f04;
	regs.ecx = 0x000f;
	regs.edx = 0x0001;

	ret = vbe_task_exec(&regs, vbe->st_buf, vbe->st_size, VBE_BUF_ESBX | VBE_BUF_SAVE);
	if (ret) {
		kfree(vbe->st_buf);
		vbe->st_buf = NULL;
		vbe->st_size = 0;
	}
}

/* Function 04h - Restore State */
static void vbe_f04h_restore(struct vbe_device *vbe, bool exit)
{
	struct lrmi_regs regs = { 0 };
	int ret;

	/* First, try to set the standard 80x25 text mode */
	vbe_fb_set_text_mode();

	if (!vbe->st_buf) {
		vbe->st_size = 0;
		return;
	}

	if (!vbe->st_size) {
		kfree(vbe->st_buf);
		vbe->st_buf = NULL;
		return;
	}

	/*
	 * Now try to restore whatever hardware state we might have
	 * saved when the fb device was first opened.
	 */
	regs.eax = 0x4f04;
	regs.ecx = 0x000f;
	regs.edx = 0x0002;

	ret = vbe_task_exec(&regs, vbe->st_buf, vbe->st_size, VBE_BUF_ESBX);
	if (ret)
		vbe_pr_warn("VBE state restore error %d\n", ret);

	if (exit) {
		kfree(vbe->st_buf);
		vbe->st_buf = NULL;
		vbe->st_size = 0;
	}
}

/* Function 06h - Set Scan Line Length in Pixels */
static int vbe_f06h_set(u32 width, bool fast)
{
	struct lrmi_regs regs = { 0 };
	int ret;

	if (fast) {
		regs.eax = 0x4f06;
		regs.ebx = 0x0001;

		/* Avoid unnecessary scan line commands */
		ret = vbe_task_exec(&regs, NULL, 0, 0);
		if (!ret) {
			if ((regs.ecx & 0xffff) == (width & 0xffff))
				return 0;
		}
	}

	memset(&regs, 0, sizeof(regs));
	regs.eax = 0x4f06;
	regs.ebx = 0x0003;

	/* Maximum scan line length */
	ret = vbe_task_exec(&regs, NULL, 0, 0);
	if (!ret) {
		if ((regs.ecx & 0xffff) < (width & 0xffff))
			return -EINVAL;
	}

	memset(&regs, 0, sizeof(regs));
	regs.eax = 0x4f06;
	regs.ebx = 0x0000;
	regs.ecx = width & 0xffff;

	ret = vbe_task_exec(&regs, NULL, 0, 0);
	if (ret)
		return ret;

	if ((regs.ecx & 0xffff) != (width & 0xffff))
		return -ENXIO;
	return 0;
}

static __always_inline void vbe_vga_vtrace_sync(u8 mode)
{
	u8 status;

	do {
		status = lrmi_inb(VBE_VGA_STATUS);
		cpu_relax();
	} while (((status & VBE_VGA_VTRACE) >> 3) != mode);
}

/* Function 07h - Set Display Start in Pixels */
static int vbe_f07h_set(u32 x, u32 y, bool vtrace_sync)
{
	struct lrmi_regs regs = { 0 };
	int ret;

	regs.eax = 0x4f07;
	regs.ebx = 0x0000;
	regs.ecx = x & 0xffff;
	regs.edx = y & 0xffff;

	/* Wait for the end of the current vertical retrace */
	if (!novsync && vtrace_sync)
		vbe_vga_vtrace_sync(VBE_VGA_VTRACE_OFF);

	ret = vbe_task_exec(&regs, NULL, 0, 0);
	if (ret)
		return ret;

	/* Wait for the start of the next vertical retrace */
	if (!novsync && vtrace_sync)
		vbe_vga_vtrace_sync(VBE_VGA_VTRACE_ON);

	memset(&regs, 0, sizeof(regs));
	regs.eax = 0x4f07;
	regs.ebx = 0x0001;

	ret = vbe_task_exec(&regs, NULL, 0, 0);
	if (ret)
		return ret;

	if (((regs.ecx & 0xffff) != (x & 0xffff)) || ((regs.edx & 0xffff) != (y & 0xffff)))
		return -ENXIO;
	return 0;
}

/* Supplemental Function 10h - Power Management Extensions (PM) */
static void vbe_f10h_set(u32 mode, bool fast)
{
	struct lrmi_regs regs = { 0 };
	int ret;

	if (nodpms)
		return;

	if (fast) {
		regs.eax = 0x4f10;
		regs.ebx = 0x0002;

		/* Avoid unnecessary DPMS commands */
		ret = vbe_task_exec(&regs, NULL, 0, 0);
		if (!ret) {
			if ((regs.ebx & VBE_DPMS_MODE_MASK) == (mode & VBE_DPMS_MODE_MASK))
				return;
		}
	}

	memset(&regs, 0, sizeof(regs));
	regs.eax = 0x4f10;
	regs.ebx = (mode & VBE_DPMS_MODE_MASK) | 0x0001;

	ret = vbe_task_exec(&regs, NULL, 0, 0);
	if (ret)
		vbe_pr_debug("DPMS mode %u set error %d\n", (mode & VBE_DPMS_MODE_MASK) >> 8, ret);
}

/* Inverted colors quirk */
static void vbe_f10h_reset(void)
{
	vbe_f10h_set(VBE_DPMS_MODE_OFF, false);
	vbe_f10h_set(VBE_DPMS_MODE_ON, false);
}

/* Supplemental Function 15h - Display Data Channel (DDC) */
static int vbe_f15h_get(void __always_unused *context, u8 *buf,
			u32 __always_unused block, size_t len)
{
	struct lrmi_regs regs = { 0 };
	void *vbe_edid;
	int ret;

	if (noedid)
		return -ENODEV;

	regs.eax = 0x4f15;
	regs.ebx = 0x0000;
	regs.ecx = 0x0000;

	ret = vbe_task_exec(&regs, NULL, 0, 0);
	if (ret)
		return ret;

	if (!(regs.ebx & 0x3))
		return -EINVAL;

	memset(&regs, 0, sizeof(regs));
	regs.eax = 0x4f15;
	regs.ebx = 0x0001;
	regs.ecx = 0x0000;
	regs.edx = 0x0000;

	vbe_edid = kzalloc(EDID_LENGTH, GFP_KERNEL);
	if (!vbe_edid)
		return -ENOMEM;

	ret = vbe_task_exec(&regs, vbe_edid, EDID_LENGTH, VBE_BUF_ESDI | VBE_BUF_SAVE);
	if (ret) {
		DRM_ERROR("Read EDID error %d\n", ret);
		goto err_free_task_buf;
	}

	memcpy(buf, vbe_edid, len);

err_free_task_buf:
	kfree(vbe_edid);
	return ret;
}

/* ------------------------------------------------------------------ */
/* FB                                                                 */

static void vbe_remove_conflicting_devices(struct vbe_device *vbe)
{
#if LINUX_VERSION_CODE < KERNEL_VERSION(6, 0, 0)
	struct apertures_struct *ap;

	ap = alloc_apertures(1);
	if (!ap)
		return;
	ap->ranges[0].base = vbe->lfb_base;
	ap->ranges[0].size = vbe->lfb_size;
	remove_conflicting_framebuffers(ap, VBEDRM_DRIVER_NAME, false);
	kfree(ap);
#elif (LINUX_VERSION_CODE >= KERNEL_VERSION(6, 0, 0) &&		\
	LINUX_VERSION_CODE < KERNEL_VERSION(6, 5, 0))
	aperture_remove_conflicting_devices(vbe->lfb_base, vbe->lfb_size, false, VBEDRM_DRIVER_NAME);
#else
	aperture_remove_conflicting_devices(vbe->lfb_base, vbe->lfb_size, VBEDRM_DRIVER_NAME);
#endif
}

static int vbe_fb_getmem_mode(struct vbe_device *vbe, struct vbe_mode_ib *mib)
{
	unsigned long size_vmode, size_remap, size_total;
	int i;

	/*
	 * Find out how much video memory is required for the mode with
	 * the highest resolution.
	 */
	size_remap = 0;
	for (i = 0; i < vbe->modes; i++) {
		unsigned long size_mode = ((vbe->vbe_info.vbe_ver_maj < 3) ?
					    vbe->vbe_modes[i].bytes_per_scan_line :
					    vbe->vbe_modes[i].lin_bytes_per_scan_line) *
					    vbe->vbe_modes[i].y_res;

		if (size_mode > size_remap)
			size_remap = size_mode;
	}
	size_remap = round_up(size_remap * 4, VBE_VRAM_GRNL);

	/* Minimum amount of video memory needed for the selected mode */
	size_vmode = round_up(((vbe->vbe_info.vbe_ver_maj < 3) ?
				mib->bytes_per_scan_line : mib->lin_bytes_per_scan_line) *
				mib->y_res * 2, VBE_VRAM_GRNL);

	/* Total available video memory */
	size_total = vbe->vbe_info.total_memory * SZ_64K;
	if (size_total < size_vmode) {
		DRM_ERROR("Not enough video memory for selected mode %ux%u-%u\n",
			  mib->x_res, mib->y_res, mib->bits_per_pixel);
		return -ENOMEM;
	}

	if ((vram_mb * SZ_1M >= size_vmode) && (vram_mb * SZ_1M <= size_total))
		size_remap = vram_mb * SZ_1M;

	if (size_remap < size_vmode)
		size_remap = size_vmode;
	if (size_remap > size_total)
		size_remap = size_total;
	if (size_remap > VBE_VRAM_MAX)
		size_remap = VBE_VRAM_MAX;

	vbe->lfb_base = base ? base : mib->phys_base_ptr;
	vbe->lfb_size = size_remap;
	return 0;
}

static int vbe_fb_request_mem(struct vbe_device *vbe)
{
	if (vbe->lfb_map || !vbe->lfb_base ||
	   (vbe->lfb_size < (VBE_MIN_WIDTH * VBE_MIN_HEIGHT * (vbe->bpp / 8) * 2)) ||
	   (((u64)vbe->lfb_base + (u64)vbe->lfb_size - 1) >> 32)) {
		DRM_ERROR("LFB setup error\n");
		return -ENXIO;
	}

	vbe_remove_conflicting_devices(vbe);

	if (!request_mem_region(vbe->lfb_base, vbe->lfb_size, VBEDRM_DRIVER_NAME)) {
		DRM_ERROR("Request region [0x%lx-0x%lx] failed\n", vbe->lfb_base,
			  vbe->lfb_base + vbe->lfb_size - 1);
		return -ENOMEM;
	}

	if (!nowc)
		vbe->lfb_map = ioremap_wc(vbe->lfb_base, vbe->lfb_size);
	if (!vbe->lfb_map) {
		vbe->lfb_map = ioremap(vbe->lfb_base, vbe->lfb_size);
		if (!vbe->lfb_map) {
			DRM_ERROR("Cannot map the LFB\n");
			release_mem_region(vbe->lfb_base, vbe->lfb_size);
			return -ENOMEM;
		}
	}
	DRM_INFO("LFB size %lu KiB at [0x%lx-0x%lx]\n", vbe->lfb_size / SZ_1K,
		 vbe->lfb_base, vbe->lfb_base + vbe->lfb_size - 1);
	return 0;
}

static int vbe_fb_find_mode(struct vbe_device *vbe, int x_res, int y_res, int bits_per_pixel)
{
	int i;

	for (i = 0; i < vbe->modes; i++) {
		if ((vbe->vbe_modes[i].x_res == x_res) && (vbe->vbe_modes[i].y_res == y_res) &&
		    (vbe->vbe_modes[i].bits_per_pixel == bits_per_pixel)) {
			DRM_DEBUG_DRIVER("Select mode %d = %ux%u-%u\n", i, vbe->vbe_modes[i].x_res,
					 vbe->vbe_modes[i].y_res, vbe->vbe_modes[i].bits_per_pixel);
			return i;
		}
	}
	return -EINVAL;
}

static void vbe_edid_load(struct vbe_device *vbe)
{
	u8 header[8] = { 0 };

	/* Check header to detect valid edid */
	vbe_f15h_get(vbe, header, 0, ARRAY_SIZE(header));
	if (drm_edid_header_is_valid(header) != 8)
		return;

#if LINUX_VERSION_CODE < KERNEL_VERSION(6, 3, 0)
	kfree(vbe->edid);
	vbe->edid = drm_do_get_edid(&vbe->connector, vbe_f15h_get, vbe);
	if (!vbe->edid)
		return;
#else
	drm_edid_free(vbe->drm_edid);
	vbe->drm_edid = drm_edid_read_custom(&vbe->connector, vbe_f15h_get, vbe);
	if (!vbe->drm_edid)
		return;
#endif
	DRM_DEBUG_DRIVER("EDID Found\n");
}

static int vbe_hw_init(struct pci_dev *pdev, struct drm_device *dev)
{
	struct vbe_device *vbe = dev->dev_private;
	struct vbe_mode_ib *mib = NULL;
	int mode, ret;
	bool ready, rom_load = false;

	ready = vbe_vbios_ready();
	if (ready)
		vbe_f04h_save(vbe);

	/* Load external VBIOS ROM */
	if (boot == 2)
		rom_load = vbe_vbios_rom_load(pdev);

vbios_boot:
	/* Boot the VBIOS */
	if ((boot == 1) || rom_load || !ready) {
		ret = vbe_vbios_boot(pci_dev_id(pdev));
		if (ret) {
			rom_load = vbe_vbios_rom_unload();
			if (rom_load)
				goto vbios_boot;
			return ret;
		}
		vbe_f04h_restore(vbe, false);
		vbe_f10h_reset();
	}

	ret = vbe_f00h_get(vbe);
	if (ret) {
		rom_load = vbe_vbios_rom_unload();
		if (rom_load)
			goto vbios_boot;
		return ret;
	}

	ret = vbe_f01h_get(vbe);
	if (ret)
		return ret;

	if (bpp >= 32)
		vbe->bpp = 32;
	else if (bpp >= 24)
		vbe->bpp = 24;
	else
		vbe->bpp = 16;

set_mode:
	vbe->xres = clamp(xres, VBE_MIN_WIDTH, VBE_MAX_WIDTH);
	vbe->yres = clamp(yres, VBE_MIN_HEIGHT, VBE_MAX_HEIGHT);
try_mode:
	/* Try order: custom resolution -> 1024x768 -> 800x600 -> 640x480 -> fail */
	mode = vbe_fb_find_mode(vbe, vbe->xres, vbe->yres, vbe->bpp);
	if (mode < 0) {
		DRM_DEBUG_DRIVER("Mode %ux%u-%u not supported\n", vbe->xres, vbe->yres, vbe->bpp);
new_mode:
		if (vbe->xres > 1024 || vbe->yres > 768) {
			vbe->xres = 1024;
			vbe->yres = 768;
			goto try_mode;
		} else if (vbe->xres > 800 || vbe->yres > 600) {
			vbe->xres = 800;
			vbe->yres = 600;
			goto try_mode;
		} else if (vbe->xres > 640 || vbe->yres > 480) {
			vbe->xres = 640;
			vbe->yres = 480;
			goto try_mode;
		}
		if (vbe->bpp > 16) {
			vbe->bpp = 16;
			goto set_mode;
		}
		return -EINVAL;
	}

	mib = &vbe->vbe_modes[mode];
	if (!mib)
		goto new_mode;

	ret = vbe_fb_getmem_mode(vbe, mib);
	if (ret)
		goto new_mode;

	ret = vbe_fb_request_mem(vbe);
	if (ret)
		return ret;
	return 0;
}

static void vbe_hw_fini(struct pci_dev *pdev, struct drm_device *dev)
{
	struct vbe_device *vbe = dev->dev_private;
	int ret;

	memset_io(vbe->lfb_map, 0, vbe->lfb_size);
	iounmap(vbe->lfb_map);
	release_mem_region(vbe->lfb_base, vbe->lfb_size);
#if LINUX_VERSION_CODE < KERNEL_VERSION(6, 3, 0)
	kfree(vbe->edid);
#else
	drm_edid_free(vbe->drm_edid);
#endif
	kfree(vbe->vbe_modes);

	if (vbe_vbios_rom_unload()) {
		ret = vbe_vbios_boot(pci_dev_id(pdev));
		if (ret)
			return;
	}
	vbe_f04h_restore(vbe, true);
}

static void vbe_hw_setmode(struct vbe_device *vbe, struct drm_display_mode *mode)
{
	int idx, ret;

	if (!drm_dev_enter(vbe->dev, &idx))
		return;

	DRM_DEBUG_DRIVER("Switching to %ux%u-%u\n", mode->hdisplay, mode->vdisplay, vbe->bpp);

	ret = vbe_f02h_set(vbe, mode);
	if (ret) {
		DRM_ERROR("Mode set %ux%u-%u error %d\n",
			  mode->hdisplay, mode->vdisplay, vbe->bpp, ret);
		drm_dev_exit(idx);
		return;
	}

	ret = vbe_f06h_set(vbe->xres, false);
	if (ret)
		DRM_ERROR("Set display scan line=%u error %d\n", vbe->xres, ret);

	ret = vbe_f07h_set(0, 0, false);
	if (ret)
		DRM_ERROR("Reset display offset error %d\n", ret);

	vbe->modeset |= VBE_MODESET_SET | VBE_MODESET_INIT;
	drm_dev_exit(idx);
}

static void vbe_hw_setbase(struct vbe_device *vbe, int x, int y, u32 stride, u64 addr)
{
	unsigned long offset;
	u32 vx, vy, vwidth;
	int idx, ret;

	if (!drm_dev_enter(vbe->dev, &idx))
		return;

	offset = (unsigned long)addr + y * stride + x * (vbe->bpp / 8);
	vy = offset / stride;
	vx = (offset % stride) * 8 / vbe->bpp;
	vwidth = stride * 8 / vbe->bpp;

	DRM_DEBUG_DRIVER("VBE: x=%d y=%d addr=0x%llx -> offset=0x%lx vx=%u vy=%u stride=%u vwidth=%u\n",
			 x, y, addr, offset, vx, vy, stride, vwidth);

	ret = vbe_f06h_set(vwidth, true);
	if (ret)
		DRM_DEBUG_DRIVER("Set display scan line=%u error %d\n", vwidth, ret);

	ret = vbe_f07h_set(vx, vy, true);
	if (ret)
		DRM_DEBUG_DRIVER("Set display offset vx=%u vy=%u error %d\n", vx, vy, ret);
	drm_dev_exit(idx);
}

/* ------------------------------------------------------------------ */
/* GEM                                                                */

static const uint32_t vbe_formats[] = {
	DRM_FORMAT_XRGB8888,
	DRM_FORMAT_RGB888,
	DRM_FORMAT_RGB565,
};

static void vbe_plane_update(struct vbe_device *vbe, struct drm_plane_state *state)
{
	struct drm_gem_vram_object *gbo;
	s64 gpu_addr;

	if (!state->fb || !(vbe->modeset & VBE_MODESET_INIT) || !(vbe->modeset & VBE_MODESET_SET))
		return;

	gbo = drm_gem_vram_of_gem(state->fb->obj[0]);
	gpu_addr = drm_gem_vram_offset(gbo);
	if (WARN_ON_ONCE(gpu_addr < 0))
		return; /* Bug: we didn't pin the BO to VRAM in prepare_fb */

	vbe_hw_setbase(vbe, state->crtc_x, state->crtc_y, state->fb->pitches[0],
		       state->fb->offsets[0] + gpu_addr);
}

static void vbe_pipe_enable(struct drm_simple_display_pipe *pipe, struct drm_crtc_state *crtc_state,
			    struct drm_plane_state *plane_state)
{
	struct vbe_device *vbe = pipe->crtc.dev->dev_private;

	vbe_f10h_set(VBE_DPMS_MODE_ON, true);
	vbe_hw_setmode(vbe, &crtc_state->mode);
	vbe_plane_update(vbe, plane_state);
}

static void vbe_pipe_disable(struct drm_simple_display_pipe *pipe)
{
	struct vbe_device *vbe = pipe->crtc.dev->dev_private;

	vbe->modeset &= ~VBE_MODESET_SET;
	vbe_f10h_set(VBE_DPMS_MODE_OFF, true);
}

static void vbe_pipe_update(struct drm_simple_display_pipe *pipe,
			    struct drm_plane_state __always_unused *old_state)
{
	struct vbe_device *vbe = pipe->crtc.dev->dev_private;

	vbe_plane_update(vbe, pipe->plane.state);
}

static int vbe_pipe_prepare_fb(struct drm_simple_display_pipe *pipe,
			       struct drm_plane_state *new_state)
{
	return drm_gem_vram_plane_helper_prepare_fb(&pipe->plane, new_state);
}

static void vbe_pipe_cleanup_fb(struct drm_simple_display_pipe *pipe,
				struct drm_plane_state *old_state)
{
	drm_gem_vram_plane_helper_cleanup_fb(&pipe->plane, old_state);
}

static const struct drm_simple_display_pipe_funcs vbe_pipe_funcs = {
	.enable	    = vbe_pipe_enable,
	.disable    = vbe_pipe_disable,
	.update	    = vbe_pipe_update,
	.prepare_fb = vbe_pipe_prepare_fb,
	.cleanup_fb = vbe_pipe_cleanup_fb,
};

static bool vbe_connector_preferred_mode_valid(struct vbe_device *vbe, int modes,
					       struct drm_connector *connector)
{
	struct drm_display_mode *mode, *preferred_mode, *t;
	int fb_mode;

	if (list_empty(&connector->probed_modes))
		return false;

	preferred_mode = list_first_entry(&connector->probed_modes, struct drm_display_mode, head);
	fb_mode = vbe_fb_find_mode(vbe, preferred_mode->hdisplay, preferred_mode->vdisplay, vbe->bpp);
	if ((fb_mode < 0) || (modes < 1)) {
		list_for_each_entry_safe(mode, t, &connector->probed_modes, head) {
			list_del(&mode->head);
			drm_mode_destroy(connector->dev, mode);
		}
		return false;
	}
	return true;
}

static int vbe_connector_get_modes(struct drm_connector *connector)
{
	struct vbe_device *vbe = container_of(connector, struct vbe_device, connector);
	int modes = 0;

#if LINUX_VERSION_CODE < KERNEL_VERSION(6, 3, 0)
	if (vbe->edid) {
		drm_connector_update_edid_property(connector, vbe->edid);
		modes = drm_add_edid_modes(connector, vbe->edid);
		if (vbe_connector_preferred_mode_valid(vbe, modes, connector))
			return modes;
		drm_connector_update_edid_property(connector, NULL);
		kfree(vbe->edid);
		vbe->edid = NULL;
	}
#else
	if (vbe->drm_edid) {
		drm_edid_connector_update(&vbe->connector, vbe->drm_edid);
		modes = drm_edid_connector_add_modes(connector);
		if (vbe_connector_preferred_mode_valid(vbe, modes, connector))
			return modes;
		drm_edid_connector_update(&vbe->connector, NULL);
		drm_edid_free(vbe->drm_edid);
		vbe->drm_edid = NULL;
	}
#endif
	modes = drm_add_modes_noedid(connector, VBE_MAX_WIDTH, VBE_MAX_HEIGHT);
	drm_set_preferred_mode(connector, vbe->xres, vbe->yres);
	return modes;
}

static const struct drm_connector_helper_funcs vbe_connector_helper_funcs = {
	.get_modes = vbe_connector_get_modes,
};

static const struct drm_connector_funcs vbe_connector_funcs = {
	.fill_modes = drm_helper_probe_single_connector_modes,
	.destroy = drm_connector_cleanup,
	.reset = drm_atomic_helper_connector_reset,
	.atomic_duplicate_state = drm_atomic_helper_connector_duplicate_state,
	.atomic_destroy_state = drm_atomic_helper_connector_destroy_state,
};

static void vbe_connector_init(struct drm_device *dev)
{
	struct vbe_device *vbe = dev->dev_private;
	struct drm_connector *connector = &vbe->connector;

	vbe_edid_load(vbe);
	drm_connector_init(dev, connector, &vbe_connector_funcs, DRM_MODE_CONNECTOR_VGA);
	drm_connector_helper_add(connector, &vbe_connector_helper_funcs);
}

static struct drm_framebuffer *vbe_mode_fb_create(struct drm_device *dev, struct drm_file *file,
						  const struct drm_mode_fb_cmd2 *mode_cmd)
{
	struct vbe_device *vbe = dev->dev_private;

	if ((vbe->bpp == 32 && mode_cmd->pixel_format == DRM_FORMAT_XRGB8888) ||
	    (vbe->bpp == 24 && mode_cmd->pixel_format == DRM_FORMAT_RGB888) ||
	    (vbe->bpp == 16 && mode_cmd->pixel_format == DRM_FORMAT_RGB565))
		return drm_gem_fb_create(dev, file, mode_cmd);

	return ERR_PTR(-EINVAL);
}

static enum drm_mode_status vbe_mode_valid(struct drm_device *dev,
					   const struct drm_display_mode *mode)
{
	struct vbe_device *vbe = dev->dev_private;
	int fb_mode = vbe_fb_find_mode(vbe, mode->hdisplay, mode->vdisplay, vbe->bpp);

	if (fb_mode < 0)
		return MODE_BAD;

	return drm_vram_helper_mode_valid(dev, mode);
}

static const struct drm_mode_config_funcs vbe_mode_funcs = {
	.fb_create = vbe_mode_fb_create,
	.mode_valid = vbe_mode_valid,
	.atomic_check = drm_atomic_helper_check,
	.atomic_commit = drm_atomic_helper_commit,
};

static int vbe_kms_init(struct vbe_device *vbe)
{
	int ret;

	ret = drmm_mode_config_init(vbe->dev);
	if (ret)
		return ret;

#if LINUX_VERSION_CODE < KERNEL_VERSION(6, 4, 0)
	vbe->dev->mode_config.prefer_shadow_fbdev = 1;
#if LINUX_VERSION_CODE < KERNEL_VERSION(6, 2, 0)
	vbe->dev->mode_config.fb_base = vbe->lfb_base;
#endif
#endif
	vbe->dev->mode_config.max_width = VBE_MAX_WIDTH;
	vbe->dev->mode_config.max_height = VBE_MAX_HEIGHT;
	vbe->dev->mode_config.preferred_depth = (vbe->bpp < 24) ? 16 : 24;
	vbe->dev->mode_config.prefer_shadow = 1;
	vbe->dev->mode_config.funcs = &vbe_mode_funcs;

	vbe_connector_init(vbe->dev);
	drm_simple_display_pipe_init(vbe->dev, &vbe->pipe, &vbe_pipe_funcs,
				     vbe_formats, ARRAY_SIZE(vbe_formats), NULL,
				     &vbe->connector);

	drm_mode_config_reset(vbe->dev);
	return 0;
}

/* ------------------------------------------------------------------ */
/* DRM                                                                */

static struct vbe_device *vbe_load(struct pci_dev *pdev, struct drm_device *dev)
{
	struct vbe_device *vbe;
	int ret;

	vbe = drmm_kzalloc(dev, sizeof(*vbe), GFP_KERNEL);
	if (!vbe)
		return NULL;

	dev->dev_private = vbe;
	vbe->dev = dev;

	ret = vbe_hw_init(pdev, dev);
	if (ret)
		return NULL;

	ret = drmm_vram_helper_init(dev, vbe->lfb_base, vbe->lfb_size);
	if (ret)
		goto err_hw_fini;

	ret = vbe_kms_init(vbe);
	if (ret)
		goto err_hw_fini;

	return vbe;

err_hw_fini:
	vbe_hw_fini(pdev, dev);
	return NULL;
}

DEFINE_DRM_GEM_FOPS(vbe_fops);

#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 11, 0)
static struct drm_driver vbe_drm_driver = {
#else
static const struct drm_driver vbe_drm_driver = {
#endif
	.driver_features	= DRIVER_GEM | DRIVER_MODESET | DRIVER_ATOMIC,
	.fops			= &vbe_fops,
	.name			= VBEDRM_DRIVER_NAME,
	.desc			= VBEDRM_DRIVER_DESC,
#if LINUX_VERSION_CODE < KERNEL_VERSION(6, 11, 0)
	.date			= VBEDRM_DRIVER_DATE,
#endif
	.major			= VBEDRM_DRIVER_MAJOR,
	.minor			= VBEDRM_DRIVER_MINOR,
	.patchlevel		= VBEDRM_DRIVER_PATCHLEVEL,
	DRM_GEM_VRAM_DRIVER,
#if LINUX_VERSION_CODE >= KERNEL_VERSION(6, 13, 0)
	DRM_FBDEV_TTM_DRIVER_OPS,
#endif
};

/* ------------------------------------------------------------------ */
/* PM                                                                 */

#ifdef CONFIG_PM_SLEEP
static int vbe_pm_suspend(struct device *dev)
{
	struct drm_device *drm_dev = dev_get_drvdata(dev);
	struct vbe_device *vbe = drm_dev->dev_private;

	vbe->modeset |= VBE_MODESET_PM_OFF;
	vbe->modeset &= ~VBE_MODESET_INIT;
	return drm_mode_config_helper_suspend(drm_dev);
}

static int vbe_pm_resume(struct device *dev)
{
	struct drm_device *drm_dev = dev_get_drvdata(dev);
	struct vbe_device *vbe = drm_dev->dev_private;
	struct pci_dev *pdev = to_pci_dev(dev);
	int ret;

	ret = vbe_vbios_boot(pci_dev_id(pdev));
	if (ret) {
		if (vbe_vbios_rom_unload()) {
			ret = vbe_vbios_boot(pci_dev_id(pdev));
			if (ret)
				return ret;
		} else {
			return ret;
		}
	}

	vbe_f04h_restore(vbe, false);
	vbe_f10h_reset();
	ret = drm_mode_config_helper_resume(drm_dev);
	if (ret)
		return ret;
	vbe->modeset &= ~VBE_MODESET_PM_OFF;
	return 0;
}
#endif

static const struct dev_pm_ops vbe_pm_ops = {
	SET_SYSTEM_SLEEP_PM_OPS(vbe_pm_suspend, vbe_pm_resume)
};

/* ------------------------------------------------------------------ */
/* PCI                                                                */

static int vbe_pci_probe(struct pci_dev *pdev, const struct pci_device_id __always_unused *ent)
{
	struct drm_device *dev;
	struct vbe_device *vbe;
	int ret;

	if (pdev != vga_default_device())
		return -ENODEV;

	dev = drm_dev_alloc(&vbe_drm_driver, &pdev->dev);
	if (IS_ERR(dev))
		return PTR_ERR(dev);

	ret = pcim_enable_device(pdev);
	if (ret)
		goto err_free_dev;

#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 14, 0)
	dev->pdev = pdev;
#endif

	pci_set_drvdata(pdev, dev);

	vbe = vbe_load(pdev, dev);
	if (!vbe) {
		ret = -ENOMEM;
		goto err_free_dev;
	}

	ret = drm_dev_register(dev, 0);
	if (ret)
		goto err_hw_fini;

#if LINUX_VERSION_CODE < KERNEL_VERSION(6, 11, 0)
	drm_fbdev_generic_setup(dev, vbe->bpp);
#elif (LINUX_VERSION_CODE >= KERNEL_VERSION(6, 11, 0) &&	\
	LINUX_VERSION_CODE < KERNEL_VERSION(6, 13, 0))
	drm_fbdev_ttm_setup(dev, vbe->bpp);
#else
	drm_client_setup_with_color_mode(dev, vbe->bpp);
#endif
	return 0;

err_hw_fini:
	vbe_hw_fini(pdev, dev);
err_free_dev:
	drm_dev_put(dev);
	return ret;
}

static void vbe_pci_remove(struct pci_dev *pdev)
{
	struct drm_device *dev = pci_get_drvdata(pdev);

	drm_dev_unplug(dev);
	drm_atomic_helper_shutdown(dev);
	vbe_hw_fini(pdev, dev);
	drm_dev_put(dev);
	vbe_f10h_reset();
}

static void vbe_pci_shutdown(struct pci_dev *pdev)
{
	struct drm_device *dev = pci_get_drvdata(pdev);

#if LINUX_VERSION_CODE < KERNEL_VERSION(6, 7, 0)
	if (!dev)
		return;
#endif
	drm_atomic_helper_shutdown(dev);
}

static const struct pci_device_id vbe_pci_tbl[] = {
	{ PCI_DEVICE_CLASS(PCI_CLASS_DISPLAY_VGA << 8, ~0) },
	{ }
};
MODULE_DEVICE_TABLE(pci, vbe_pci_tbl);

static struct pci_driver vbe_pci_driver = {
	.name		= VBEDRM_DRIVER_NAME,
	.id_table	= vbe_pci_tbl,
	.probe		= vbe_pci_probe,
	.remove		= vbe_pci_remove,
	.shutdown	= vbe_pci_shutdown,
	.driver.pm	= &vbe_pm_ops,
};

static int vbe_pci_init(void)
{
	int ret;

	if (!vbe_drm_modeset)
		return -ENODEV;

	ret = lrmi_init();
	if (ret)
		return ret;

	return pci_register_driver(&vbe_pci_driver);
}

static void vbe_pci_exit(void)
{
	pci_unregister_driver(&vbe_pci_driver);
	lrmi_exit();
}

module_init(vbe_pci_init);
module_exit(vbe_pci_exit);
