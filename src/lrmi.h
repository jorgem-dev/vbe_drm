/* SPDX-License-Identifier: MIT */

/*
 * Copyright (C) 1998 Josh Vanderhoof
 * Copyright (C) 2005-2006 Matthew Garrett
 * Copyright (C) 2005-2006 Jonathan McDowell
 * Copyright (C) 2024 Jorge Maidana
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL JOSH VANDERHOOF BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef LRMI_H
#define LRMI_H

struct lrmi_regs {
	u32 edi, esi, ebp;
	u32 reg32;
	u32 ebx, edx, ecx, eax;
	u16 flags;
	u16 es, ds, fs, gs;
	u16 ip, cs, sp, ss;
	u16 reg16[7];
};
static_assert(sizeof(struct lrmi_regs) == 64, "Size of struct lrmi_regs must be 64");

int lrmi_init(void); /* Initialize, return 0 if successful */
void lrmi_exit(void); /* Unmap all memory maps */
void lrmi_call(struct lrmi_regs *regs); /* Simulate a 16 bit far call */
void lrmi_int(struct lrmi_regs *regs, u8 intno); /* Simulate a 16 bit interrupt */
u32 lrmi_alloc(size_t size); /* Allocate real mode memory, 16 byte aligned */
void lrmi_free(u32 addr); /* Free real mode memory */

/* I/O functions */
u8 lrmi_inb(u16 port);
u16 lrmi_inw(u16 port);
u32 lrmi_inl(u16 port);
void lrmi_outb(u16 port, u8 val);
void lrmi_outw(u16 port, u16 val);
void lrmi_outl(u16 port, u32 val);

/* Memory access functions */
static inline u16 lrmi_phys_to_seg(u32 addr)
{
	return (u16)((addr >> 4) & 0xffff);
}

static inline u32 lrmi_far_to_phys(u32 addr)
{
	return (u32)((((addr) & 0xffff0000) >> 12) + ((addr) & 0xffff));
}

void *lrmi_phys_to_virt(u32 addr);
u8 lrmi_readb(u32 addr);
u16 lrmi_readw(u32 addr);
u32 lrmi_readl(u32 addr);
void lrmi_writeb(u32 addr, u8 val);
void lrmi_writew(u32 addr, u16 val);
void lrmi_writel(u32 addr, u32 val);
u16 lrmi_int_seg(u8 intno);
u16 lrmi_int_off(u8 intno);

#endif
