## VBE Core 2.0+ DRM Driver for Linux 5.10+

vbe-drm is a VBE/Core 2.0+ DRM driver based on bochs-drm, replacing all the direct VGA and DISPI I/O control functions with VBE function calls using lrmi and x86emu.
